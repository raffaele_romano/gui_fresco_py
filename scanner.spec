# -*- mode: python ; coding: utf-8 -*-

from PyInstaller.utils.hooks import collect_data_files
from glob import glob

block_cipher = None
extra_data = collect_data_files('skrf')
# extra_data += glob('/GUI_fresco_py/data/**')

datas = [
  (os.path.join(SPECPATH, "resources_rc.py"), "./"),
  (os.path.join(SPECPATH, "data"), "data"),

]

extra_data +=datas

a = Analysis(
    ['scanner.py'],
    pathex=[],
    binaries=[],
    datas=extra_data,
    hiddenimports=['skrf','sklearn','resources_rc','pyvisa_py','sklearn.cross_decomposition','pyrcc5'],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False,
)


pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)

exe = EXE(
    pyz,
    a.scripts,
    [],
    exclude_binaries=True,
    name='scanner',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    console=True,
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
    icon='data\System\Design\icon.ico',
)
coll = COLLECT(
    exe,
    a.binaries,
    a.zipfiles,
    a.datas,
    strip=False,
    upx=True,
    upx_exclude=[],
    name='scanner',
)
