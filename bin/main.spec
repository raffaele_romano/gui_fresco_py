# -*- mode: python ; coding: utf-8 -*-

import os
import skrf as rf

datas = [
  (os.path.join(os.path.dirname(rf.__file__), "data/"), "skrf/data/"),
  (os.path.join(SPECPATH, "resources_rc.py"), "./"),
  (os.path.join(SPECPATH, "System/Calkit/*"), "System/Calkit/"),
  (os.path.join(SPECPATH, "System/Design/*"), "System/Design/"),
  (os.path.join(SPECPATH, "System/Settings/*"), "System/Settings/")
]

block_cipher = None

a = Analysis(['main.py'],
             pathex=[SPECPATH],
             binaries=[],
             datas=datas,
             hiddenimports=["pyvisa_py"],
             hookspath=[],
             hooksconfig={},
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,  
          [],
          name='main',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=False,
          disable_windowed_traceback=False,
          target_arch=None,
          codesign_identity=None,
          entitlements_file=None , icon='System/Design/icon.ico')
