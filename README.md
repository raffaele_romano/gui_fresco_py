# ReadMe

gui_fresco_py is a Python-based framework for non-invasive testing of fruits.

## Platform Compatibility
Python 3.8+ on Windows 10 (some dependencies might not be compatible with Python 3.10.x)

## Installation

1. Download and install the [RVNA Software](https://coppermountaintech.com/r-vna-soft/) from Copper Mountain Technologies.

2. Create a virtual environment for the project using `venv` or similar and activate it.
```bat
python -m venv venv
.\venv\Scripts\activate
```

3. Use the package manager [pip](https://pip.pypa.io/en/stable/) to install the dependencies.
```bat
pip install -r requirements.txt
```

## Usage

* Launch measurement UI.
```bat
python main.py
```

* Launch file-transfer UI.
```bat
python transfer.py
```

* Compile measurement and file-trasfer UIs to executables.
```bat
.\build.bat
```

* Generate requirements file (requires _pigar_ package from PyPI)
```bat
.\gen_reqs.bat
```

* Update documentation sources and generate HTML docs.
```bat
cd docs
.\generate_docs.bat
```

* Generate HTML docs from existing source files.
```bat
.\make.bat html
```

## License
Proprietary