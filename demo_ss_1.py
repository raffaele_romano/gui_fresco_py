import sys
from PyQt5.QtWidgets import QApplication, QWidget, QProgressBar, QVBoxLayout, QLabel, QFrame
from PyQt5.QtCore import Qt, QTimer, pyqtSignal
from PyQt5.QtGui import QPixmap

class SplashScreen(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('Splash Screen Example')
        self.setFixedSize(400, 400)
        self.setWindowFlag(Qt.FramelessWindowHint)
        self.setAttribute(Qt.WA_TranslucentBackground)

        self.initUI()

        self.progressBar.setValue(0)  # Initialize the progress bar to 0%

    # def initUI(self):
    #     layout = QVBoxLayout()
    #     self.setLayout(layout)
    #
    #     self.progressBar = QProgressBar(self)
    #     self.progressBar.setAlignment(Qt.AlignCenter)
    #     self.progressBar.setFormat('%p%')
    #     self.progressBar.setTextVisible(True)
    #     self.progressBar.setRange(0, 100)  # We use a 0-100 range for this example

    def initUI(self):
        # Create a QFrame as a container for the background image and progress bar
        frame = QFrame(self)
        frame.setGeometry(0, 0, 300, 300)  # Set the size and position of the frame
        frame.setStyleSheet("background-image: url('data/System/Design/splash_1.png'); background-position: center;")

        layout = QVBoxLayout()
        frame.setLayout(layout)

        # Create the progress bar
        self.progressBar = QProgressBar()
        self.progressBar.setAlignment(Qt.AlignCenter)
        self.progressBar.setFormat('%p%')
        self.progressBar.setTextVisible(True)
        self.progressBar.setRange(0, 100)  # We use a 0-100 range for this example

        layout.addWidget(self.progressBar)

class MyApp(QWidget):

    updateProgressSignal = pyqtSignal(int)

    def __init__(self, splash_screen):
        super().__init__()
        self.window_width, self.window_height = 1200, 800
        self.setMinimumSize(self.window_width, self.window_height)

        layout = QVBoxLayout()
        self.setLayout(layout)

        self.splash_screen = splash_screen  # Set a reference to the SplashScreen instance

        # Connect the updateProgressSignal to the updateProgressBar slot in SplashScreen
        self.updateProgressSignal.connect(self.splash_screen.progressBar.setValue)

        # Simulate the completion of method_one, method_two, and method_three
        self.callMethodWithDelay(self.method_one, 2000)  # Delay of 1000 milliseconds (1 second)
        self.callMethodWithDelay(self.method_two, 4000)  # Delay of 2000 milliseconds (2 seconds)
        self.callMethodWithDelay(self.method_three, 5000)  # Delay of 3000 milliseconds (3 seconds)

    def callMethodWithDelay(self, method, delay_ms):
        # Call a method with a specified delay using QTimer
        timer = QTimer(self)
        timer.timeout.connect(method)
        timer.setSingleShot(True)  # Set to run once
        timer.start(delay_ms)

    def method_one(self):
        # Simulate the completion of method_one
        # You can add your actual logic here
        print('Method one running')
        self.updateProgressSignal.emit(33)  # Emit signal to update progress to 33%

    def method_two(self):
        # Simulate the completion of method_two
        # You can add your actual logic here
        print('Method two running')
        self.updateProgressSignal.emit(66)  # Emit signal to update progress to 66%

    def method_three(self):
        # Simulate the completion of method_three
        # You can add your actual logic here
        print('Method 3 running')
        self.updateProgressSignal.emit(100)  # Emit signal to update progress to 100%
        # Close the splash screen and show MyApp
        self.splash_screen.close()
        self.show()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setStyleSheet('''
        QProgressBar {
            background-color: #DA7B93;
            color: rgb(200, 200, 200);
            border-style: none;
            border-radius: 10px;
            text-align: center;
            font-size: 30px;
        }

        QProgressBar::chunk {
            border-radius: 10px;
            background-color: qlineargradient(spread:pad x1:0, x2:1, y1:0.511364, y2:0.523, stop:0 #1C3334, stop:1 #376E6F);
        }
    ''')

    splash = SplashScreen()
    splash.show()

    myApp = MyApp(splash)
    # myApp.show()

    try:
        sys.exit(app.exec_())
    except SystemExit:
        print('Closing Window...')
