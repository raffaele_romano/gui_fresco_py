# -*- coding: utf-8 -*-
"""
Created on Sun Sep 19 17:47:10 2021.

@author: Nishad Mandlik
"""

from pathlib import Path
import pickle
import select
import socket
from sys import platform

_SRV_PORT = 2019
_CLT_PORT = 2020
_SCRIPT_DIR = Path(__file__).parent
_MEAS_DIR = Path("data/User/Measurements")
_BUF_SIZE = 4096
_DATA_SZ_SZ = 4  # Number of bytes for representing size of data
_ENDIANNESS = "little"
_REQ_FILES_LIST = "files_list"
_REQ_FILES = "files"
_REQ_END = "end"
_FILE_SEND_INTERVAL_S = 0


class Base:
    """
    Base class for TCP-based file-transfer.

    Parameters
    ----------
    port : int
        Port to be used for the connection.

    Returns
    -------
    None.

    """

    def __init__(self, port):
        self.init_sock(port)
        self.conn_sock = None

    def send_raw(self, byt):
        """
        Send bytes data on the connected socket.

        Parameters
        ----------
        byt : bytes
            Data to be sent on the connected socket.

        Returns
        -------
        None.

        """
        if self.conn_sock is None:
            return
        self.conn_sock.sendall(len(byt).to_bytes(_DATA_SZ_SZ, _ENDIANNESS))
        self.conn_sock.sendall(byt)

    def recv_raw(self):
        """
        Receive bytes data on the connected socket.

        Returns
        -------
        bytes
            Data received on the socket.

        """
        if self.conn_sock is None:
            return None
        pending = _DATA_SZ_SZ
        data = b''
        while pending > 0:
            tmp = self.conn_sock.recv(pending)
            data += tmp
            pending -= len(tmp)
        pending = int.from_bytes(data, _ENDIANNESS)
        data = b''
        while pending > 0:
            tmp = self.conn_sock.recv(min(pending, _BUF_SIZE))
            data += tmp
            pending -= len(tmp)
        return data

    def send_obj(self, obj):
        """
        Pickle a python-object and send it on the connected socket.

        Parameters
        ----------
        obj : object
            Object to be sent on the socket.

        Returns
        -------
        None.

        """
        self.send_raw(pickle.dumps(obj))

    def recv_obj(self):
        """
        Receive data on the connected socket and unpickle it.

        Returns
        -------
        object
            Data received on the socket..

        """
        return pickle.loads(self.recv_raw())

    def send_str(self, string):
        """
        Send a string on the connected socket.

        Parameters
        ----------
        string : str
            String to be sent on the socket.

        Returns
        -------
        None.

        """
        self.send_raw(string.encode("utf-8"))

    def recv_str(self):
        """
        Receive a string on the connected socket.

        Returns
        -------
        str
            String received on the socket.

        """
        return self.recv_raw().decode("utf-8")

    def init_sock(self, port):
        """
        Initialize a socket for connection.

        Parameters
        ----------
        port : port
            Port to be used for the connection.

        Returns
        -------
        None.

        """
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        if (platform == "linux") or (platform == "linux2"):
            self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
        if platform == "win32":
            self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind(("", port))

    def del_sock(self):
        """
        Shutdown and close the socket.

        Returns
        -------
        None.

        """
        try:
            self.sock.shutdown(socket.SHUT_RDWR)
        except:
            pass
        self.sock.close()

    def __del__(self):
        """
        Delete the socket while destroying the object.

        Returns
        -------
        None.

        """
        self.del_sock()


class Server(Base):
    """
    Server for TCP-based file-transfer.

    Parameters
    ----------
    cb_connected : function, optional
        User-defined callback for connection-established event. The function
        should accept a single argument which is a tuple containing the IP
        address and port of the connected client. The default is None.
    cb_disconnected : function, optional
        User-defined callback for disconnection event. The function should not
        accept any arguments. The default is None.
    cb_file_sending : function, optional
        User-defined callback for the event when sending of a file has started.
        The function should accept 3 arguments, namely file name, number of
        transfers started (including the current one), and the total number
        of files requested by the client. The default is None.
    cb_file_sent : function, optional
        User-defined callback for the event when a file is sent.
        The function should accept 3 arguments, namely file name, number of
        transfers completed (including the current one), and the total number
        of files requested by the client. The default is None.

    Returns
    -------
    None.

    """

    def __init__(self, cb_connected=None, cb_disconnected=None,
                 cb_file_sending=None, cb_file_sent=None):
        super().__init__(_SRV_PORT)
        self.sock.listen()
        self.conn_addr = None

        if cb_connected is None:
            self.cb_connected = lambda x: None
        else:
            self.cb_connected = cb_connected

        if cb_disconnected is None:
            self.cb_disconnected = lambda: None
        else:
            self.cb_disconnected = cb_disconnected

        if cb_file_sending is None:
            self.cb_file_sending = lambda x, y, z: None
        else:
            self.cb_file_sending = cb_file_sending

        if cb_file_sent is None:
            self.cb_file_sent = lambda x, y, z: None
        else:
            self.cb_file_sent = cb_file_sent

        self.enabled = True

    def accept_conn(self):
        """
        Listen for incoming connection request from the client.

        Returns
        -------
        tuple
            IP address and port of the conncected client.

        """
        while self.enabled:
            r, w, e = select.select([self.sock], [], [], 1)
            for s in r:
                if s == self.sock:
                    self.conn_sock, self.conn_addr = self.sock.accept()
                    self.cb_connected(self.conn_addr)
                    return self.conn_addr

    def break_conn(self):
        """
        Disconnect the client and close the socket.

        Note
        ----
        This only closes the socket on which the client is connected. For a TCP
        server such as this one, this is different from the socket which is
        used for listening to connection requests.

        Returns
        -------
        None.

        """
        if self.conn_sock is not None:
            self.conn_sock.shutdown(socket.SHUT_RDWR)
            self.conn_sock.close()
            self.conn_sock = None
            self.conn_addr = None
        self.cb_disconnected()

    def start_serving(self):
        """
        Start listening to requests from the client, and process them.

        Returns
        -------
        None.

        """
        while (1):
            try:
                req = self.recv_str().split("/")
                if req[0] == _REQ_FILES_LIST:
                    self.send_files_list()
                elif req[0] == _REQ_FILES:
                    self.send_files(req[1:])
                elif req[0] == _REQ_END:
                    self.break_conn()
                    break
            except:
                self.break_conn()
                break

    def send_files_list(self):
        """
        Send a list of available measurement files to the client.

        Returns
        -------
        None.

        """
        it = _MEAS_DIR.glob("*")
        files_list = [x.name for x in it if x.is_file()]
        self.send_obj(files_list)

    def send_files(self, file_names):
        """
        Read the specified measurement files and send their data to the client.

        Parameters
        ----------
        file_names : list
            List of file names requested by the client.

        Returns
        -------
        None.

        """
        for i in range(0, len(file_names)):
            file_name = file_names[i]
            self.cb_file_sending(str(file_name), i + 1, len(file_names))
            try:
                with (_MEAS_DIR / file_name).open("rb") as f:
                    file_bytes = f.read()
            except FileNotFoundError:
                file_bytes = b""
            self.send_raw(file_bytes)
            self.cb_file_sent(str(file_name), i + 1, len(file_names))


class Client(Base):
    """
    Client for TCP-based file transfer.

    Parameters
    ----------
    cb_connected : function, optional
        User-defined callback for connection-established event.
        The function should not accept any arguments. The default is None.
    cb_disconnected : function, optional
        User-defined callback for disconnection event.
        The function should not accept any arguments. The default is None.
    cb_files_list_recd : function, optional
        User-defined callback for reception of files-list from the server.
        The function should not accept a single arguments which is a list
        of received file names. The default is None.
    cb_file_receiving : function, optional
        User-defined callback for the event when reception of a file has
        started. The function should accept 3 arguments, namely file name,
        number of transfers started (including the current one), and the total
        number of files requested. The default is None.
    cb_file_received : function, optional
        User-defined callback for the event when a file is received.
        The function should accept 3 arguments, namely file name, number of
        transfers completed (including the current one), and the total number
        of files requested by the client. The default is None.

    Returns
    -------
    None.

    """

    def __init__(self, cb_connected=None, cb_disconnected=None,
                 cb_files_list_recd=None, cb_file_receiving=None,
                 cb_file_received=None):
        super().__init__(_CLT_PORT)

        if cb_connected is None:
            self.cb_connected = lambda: None
        else:
            self.cb_connected = cb_connected

        if cb_disconnected is None:
            self.cb_disconnected = lambda: None
        else:
            self.cb_disconnected = cb_disconnected

        if cb_files_list_recd is None:
            self.cb_files_list_recd = lambda x: None
        else:
            self.cb_files_list_recd = cb_files_list_recd

        if cb_file_receiving is None:
            self.cb_file_receiving = lambda x, y, z: None
        else:
            self.cb_file_receiving = cb_file_receiving

        if cb_file_received is None:
            self.cb_file_received = lambda x, y, z: None
        else:
            self.cb_file_received = cb_file_received

    def request_conn(self, serv_addr):
        """
        Initiate a connection to the server at the specified address.

        Parameters
        ----------
        serv_addr : str
            IP address of the server.

        Returns
        -------
        None.

        """
        self.sock.connect((serv_addr, _SRV_PORT))
        self.conn_sock = self.sock
        self.cb_connected()

    def break_conn(self):
        """
        Disconnect from the server and reinitialize the socket.

        Returns
        -------
        None.

        """
        self.del_sock()
        self.init_sock(_CLT_PORT)
        self.cb_disconnected()

    def request_files_list(self):
        """
        Request the list of measurement files available on the server.

        Returns
        -------
        files_list : list
            List of file names.

        """
        self.send_str(_REQ_FILES_LIST)
        files_list = self.recv_obj()
        self.cb_files_list_recd(files_list)
        return files_list

    def request_files(self, file_names, dest_dir):
        """
        Request the specified files from the server.

        Parameters
        ----------
        file_names : list
            List of file names to be requested.
        dest_dir : pathlib.Path
            Local directory for storing the receiving files..

        Returns
        -------
        None.

        """
        self.send_str(_REQ_FILES + "/" + "/".join(file_names))
        for i in range(0, len(file_names)):
            file_name = file_names[i]
            self.cb_file_receiving(file_name, i + 1, len(file_names))
            file_bytes = self.recv_raw()
            with (dest_dir / file_name).open("wb") as f:
                f.write(file_bytes)
            self.cb_file_received(file_name, i + 1, len(file_names))

    def request_end(self):
        """
        Send a request to the server to end serving, and break the connection.

        Returns
        -------
        None.

        """
        self.send_str(_REQ_END)
        self.break_conn()
