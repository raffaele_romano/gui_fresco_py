# -*- coding: utf-8 -*-
"""
Created on Mon Aug 16 10:09:04 2021.

@author: Nishad Mandlik
"""

from datetime import datetime as dt
import numpy as np
from pathlib import Path
import pickle
import skrf as rf
from warnings import warn

_SCRIPT_DIR = Path(__file__).parent.parent
_CALIB_DIR = _SCRIPT_DIR / "data/User/Calibration"
_CALKIT_DIR = _SCRIPT_DIR / "data/System/Calkit"


class Cal:
    """
    Class for the VNA calibration.

    Parameters
    ----------
    vna_name : str
        VNA model name.
    vna_sn : str
        VNA serial number
    calkit_name : str
        Name of the calkit.
    reinit_incomplete : bool, optional
        Specifies whether incomplete calibration should be loaded as is, or
        should be reinitialized to a blank one. The default is False.

    Returns
    -------
    None.

    """

    def __init__(self, vna_name, vna_sn, calkit_name, reinit_incomplete=False):
        cal_clear = True
        _CALIB_DIR.mkdir(parents=True, exist_ok=True)
        self.calib_file = _CALIB_DIR / (vna_name + "_" + vna_sn + "_calibration.pickle")
        if self.calib_file.is_file():
            with self.calib_file.open("rb") as f:
                tmp = pickle.load(f)

            # Load the saved calib if the supplied calkit name matches the
            # saved one.
            if ((tmp.get("_freq", None) is not None) and
                    (tmp.get("calkit_name", None) == calkit_name)):
                # Incomplete calib will be erased/loaded according to the
                # reinit_incomplete flag
                if (reinit_incomplete is False or
                        ((tmp.get("load", None) is not None) and
                         (tmp.get("short", None) is not None) and
                         (tmp.get("open", None) is not None))):
                    cal_clear = False
                    self.__dict__.clear()
                    self.__dict__.update(tmp)
                    self.calib_file = _CALIB_DIR / (vna_name +
                                                    "_" + vna_sn + "_calibration.pickle")

        if cal_clear:
            self._blank_cal(calkit_name)

    def is_open_done(self):
        """
        Return the status of the open-calibration.

        Returns
        -------
        bool
            Status (true/false) of the open-calibration.

        """
        return self.open is not None

    def is_short_done(self):
        """
        Return the status of the short-calibration.

        Returns
        -------
        bool
            Status (true/false) of the short-calibration.

        """
        return self.short is not None

    def is_load_done(self):
        """
        Return the status of the load-calibration.

        Returns
        -------
        bool
            Status (true/false) of the load-calibration.

        """
        return self.load is not None

    def is_complete(self):
        """
        Return the status of the entire calibration (open + load + short).

        Returns
        -------
        bool
            Status of the entire calibration.

        """
        return ((self.freq is not None) and self.is_load_done() and
                self.is_short_done() and self.is_open_done())

    def validate_freq(self, v):
        """
        Validate the supplied vector with the calibration.

        Parameters
        ----------
        v : numpy.ndarray
            Frequency vector.

        Returns
        -------
        bool
            True, if the supplied frequency vector matches the calibration.
            False, otherwise.

        """
        # If no calibration has been done, return True.
        if not (self.is_load_done() or self.is_open_done() or
                self.is_short_done()):
            return True
        if ((self._freq.shape == v.shape) and
                (self._freq == v).all()):
            return True
        return False

    @property
    def freq(self):
        """
        Return the frequency vector used for calibration.

        Returns
        -------
        numpy.ndarray
            Frequency vector used for calibration.

        """
        return self._freq

    @freq.setter
    def freq(self, v):
        """
        Set the frequency vector for performing calibration.

        Parameters
        ----------
        v : numpy.ndarray
            Frequency vector for performing calibration..

        Returns
        -------
        None.

        """
        # If the existing frequency vector is None, or is equal to the
        # new one, keep the current calibration.
        # Otherwise erase the current calibration.
        if self._freq is None:
            self._freq = v
        if (self._freq.shape == v.shape) and (self._freq == v).all():
            return

        self._blank_cal(self.calkit_name)

    def calib_load(self, s11_raw):
        """
        Add measurement for load-calibration.

        Parameters
        ----------
        s11_raw : numpy.ndarray
            Measurement for load-calibration.

        Returns
        -------
        ret : bool
            Status of the entire calibration.

        """
        ret = False
        if not self.validate_meas(s11_raw):
            return ret
        self.load = s11_raw
        if self.is_complete():
            self._calc_error_terms()
            ret = True
        self.save()
        return ret

    def calib_open(self, s11_raw):
        """
        Add measurement for open-calibration.

        Parameters
        ----------
        s11_raw : numpy.ndarray
            Measurement for open-calibration.

        Returns
        -------
        ret : bool
            Status of the entire calibration.

        """
        ret = False
        if not self.validate_meas(s11_raw):
            return ret
        self.open = s11_raw
        if self.is_complete():
            self._calc_error_terms()
            ret = True
        self.save()
        return ret

    def calib_short(self, s11_raw):
        """
        Add measurement for short-calibration.

        Parameters
        ----------
        s11_raw : numpy.ndarray
            Measurement for short-calibration.

        Returns
        -------
        ret : bool
            Status of the entire calibration.

        """
        ret = False
        if not self.validate_meas(s11_raw):
            return ret
        self.short = s11_raw
        if self.is_complete():
            self._calc_error_terms()
            ret = True
        self.save()
        return ret

    def validate_meas(self, s11_raw):
        """
        Check if the measurement size matches that of the frequency vector.

        Parameters
        ----------
        s11_raw : numpy.ndarray
            Measurement for validating.

        Returns
        -------
        bool
            True, if the size of the measurement matches that of the
            frequency vector. False, otherwise.

        """
        if self.freq is None:
            warn("Please set frequency vector first.")
            return False
        if self.freq.size != s11_raw.size:
            warn("Measurement not compatible with frequency vector")
            return False
        return True

    def apply_correction(self, s11_raw):
        """
        Return the corrected measurement vector.

        Note
        ----
        If calibration is not complete, no correction is performed.

        Parameters
        ----------
        s11_raw : numpy.ndarray
            Measurement vector.

        Returns
        -------
        numpy.ndarray
            Corrected measurement vector.

        """
        if self.is_complete():
            e10e01 = self.eterms["e10e01"]
            e00 = self.eterms["e00"]
            e11 = self.eterms["e11"]

            delta = e00 * e11 - e10e01
            s11 = (s11_raw - e00) / (s11_raw * e11 - delta)
            return s11
        return s11_raw

    def save(self):
        """
        Save the calibration to a file.

        Returns
        -------
        None.

        """
        self.datetime = dt.now().strftime("%Y%m%d_%H%M%S")
        with self.calib_file.open("wb") as f:
            pickle.dump(self.__dict__, f)

    def reset_cal(self):
        """
        Erase the existing calib file (if any), and reinitialize a blank one.

        Returns
        -------
        None.

        """
        self.calib_file.unlink(missing_ok=True)
        self._blank_cal(self.calkit_name)

    @classmethod
    def port_legacy(cls, vna_name):
        """
        Port the calibration from the old format to the new one.

        Parameters
        ----------
        vna_name : str
            Model name of the VNA device.

        Returns
        -------
        obj : calib.Cal
            Object containing the ported calibration.

        """
        with (_CALIB_DIR / "calibration_legacy.pickle").open("rb") as f:
            legacy_cal = pickle.load(f)
        obj = cls(vna_name, legacy_cal["calkit"]["name"])
        if legacy_cal["complete"]:
            obj.calkit["open"] = legacy_cal["calkit"]["open"]
            obj.calkit["short"] = legacy_cal["calkit"]["short"]
            obj.calkit["load"] = legacy_cal["calkit"]["load"]
            obj.calkit["frequency"] = legacy_cal["calkit"]["frequency"]

            obj.eterms["e10e01"] = legacy_cal["eterms"]["e10e01"]
            obj.eterms["e00"] = legacy_cal["eterms"]["e00"]
            obj.eterms["e11"] = legacy_cal["eterms"]["e11"]

            obj.open = legacy_cal["open"]
            obj.short = legacy_cal["short"]
            obj.load = legacy_cal["load"]
            obj.freq = legacy_cal["freq"]

            obj.datetime = legacy_cal.get("date_time",
                                          dt.now().strftime("%Y%m%d_%H%M%S"))
            with obj.calib_file.open("wb") as f:
                pickle.dump(obj.__dict__, f)
        return obj

    def _blank_cal(self, calkit_name):
        """
        Initialize a blank calibration.

        Parameters
        ----------
        calkit_name : str
            Name of the CalKit to be used for calibration.

        Returns
        -------
        None.

        """
        self._freq = None
        self.calkit_name = calkit_name
        self.calkit = self._load_calkit(calkit_name)
        self.eterms = {}

        self.open = None
        self.short = None
        self.load = None
        self.datetime = None

    def _load_calkit(self, name):
        """
        Load the CalKit files.

        Parameters
        ----------
        name : str
            Name of the CalKit to be used for calibration.

        Returns
        -------
        dict
            CalKit parameters.

        """
        calkit = {}
        tmp = name.split("_", 1)
        prefix = tmp[0]
        suffix = tmp[1]

        open_filename = prefix + "_Open_" + suffix + ".s1p"
        short_filename = prefix + "_Short_" + suffix + ".s1p"
        load_filename = prefix + "_Load_" + suffix + ".s1p"
        with (_CALKIT_DIR / open_filename).open() as f:
            t_open = rf.Touchstone(f)
        with (_CALKIT_DIR / short_filename).open() as f:
            t_short = rf.Touchstone(f)
        with (_CALKIT_DIR / load_filename).open() as f:
            t_load = rf.Touchstone(f)
        calkit["open"] = t_open.sparameters[:, 1] + \
                         1j * t_open.sparameters[:, 2]
        calkit["short"] = t_short.sparameters[:, 1] + \
                          1j * t_short.sparameters[:, 2]
        calkit["load"] = t_load.sparameters[:, 1] + \
                         1j * t_load.sparameters[:, 2]
        calkit["frequency"] = t_load.sparameters[:, 0] * t_load.frequency_mult

        return calkit

    def _calc_error_terms(self):
        """
        Calculate the error terms from the measurements of load, open and short have been provided.

        Returns
        -------
        None.

        """
        G1 = np.interp(self.freq, self.calkit["frequency"],
                       self.calkit["open"])
        G2 = np.interp(self.freq, self.calkit["frequency"],
                       self.calkit["short"])
        G3 = np.interp(self.freq, self.calkit["frequency"],
                       self.calkit["load"])

        # Load calibration standards measurement
        Gm1 = self.open
        Gm2 = self.short
        Gm3 = self.load

        # Calibration
        e11 = -(-G3 * Gm2 +
                Gm3 * G2 -
                G1 * Gm3 +
                G1 * Gm2 +
                Gm1 * G3 -
                Gm1 * G2) / (-G3 * Gm3 * G2 +
                             G3 * G2 * Gm2 -
                             G1 * Gm1 * G3 +
                             G1 * Gm1 * G2 +
                             G1 * G3 * Gm3 -
                             G1 * G2 * Gm2)

        De = -(-Gm2 * Gm1 * G2 +
               Gm3 * G2 * Gm2 +
               Gm3 * Gm1 * G3 -
               Gm2 * G3 * Gm3 +
               Gm2 * G1 * Gm1 -
               Gm3 * G1 * Gm1) / (-G3 * Gm3 * G2 +
                                  G3 * G2 * Gm2 -
                                  G1 * Gm1 * G3 +
                                  G1 * Gm1 * G2 +
                                  G1 * G3 * Gm3 -
                                  G1 * G2 * Gm2)

        e00 = (G3 * Gm3 * G1 * Gm2 -
               G3 * Gm3 * Gm1 * G2 +
               G2 * Gm2 * Gm1 * G3 -
               Gm2 * G1 * Gm1 * G3 +
               Gm3 * G1 * Gm1 * G2 -
               Gm3 * G1 * G2 * Gm2) / (-G3 * Gm3 * G2 +
                                       G3 * G2 * Gm2 -
                                       G1 * Gm1 * G3 +
                                       G1 * Gm1 * G2 +
                                       G1 * G3 * Gm3 -
                                       G1 * G2 * Gm2)

        e10e01 = e00 * e11 - De

        self.eterms["e10e01"] = e10e01
        self.eterms["e00"] = e00
        self.eterms["e11"] = e11

    def __getitem__(self, k):
        """
        Return an attribute value using dict-like indexing.

        If the attribute is not present, then None is returned.

        Parameters
        ----------
        k : str
            Attribute name.

        """
        return getattr(self, k, None)

    def __setitem__(self, k, v):
        """
        Set an attribute value using dict-like indexing.

        Parameters
        ----------
        k : str
            Attribute name.
        v : any
            Attribute value.

        """
        return setattr(self, k, v)
