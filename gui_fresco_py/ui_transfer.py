# -*- coding: utf-8 -*-
"""
Created on Sat Sep 18 23:27:43 2021.

@author: Nishad Mandlik
"""

from gui_fresco_py.cust_widgets import CheckBox, RadioButton
from gui_fresco_py.discovery import Query, Response, BluetoothQuery, BluetoothResponse
from gui_fresco_py.file_transfer import Server, Client
import os
from pathlib import Path
import psutil
from PyQt5 import uic
from PyQt5.QtCore import pyqtSignal, Qt, QObject, QThread
from PyQt5.QtWidgets import QApplication, QFileDialog
import socket
from time import sleep

_SCRIPT_DIR = Path(__file__).parent
_MEAS_DIR = _SCRIPT_DIR / "../User/Measurements"
_RSRC_QRC_PATH = _SCRIPT_DIR / Path("../data/System/Design/resources.qrc")
_RSRC_PY_PATH = _SCRIPT_DIR / Path(
    "../resources_rc.py")  # TODO: this file has to be built after new resources are added
_DESKTOP_DIR = Path.home() / "Desktop"

"""
TODO: needs updated path to know where to find pyrcc5
it probably can't find it because of the elevated rights

the code below does some dirty tinkering adding the path where pyrcc5 was
found in my python virtual environment

needs to be extra checked whether it appears there on other machines as well
also, needs to be checked where it appears without a virtual environment
"""
# new_path = os.getenv('PATH').split(';')[0].split('venv')[0] + 'venv\\Scripts'
# print(new_path)
#
# os.environ["PATH"] += os.pathsep + new_path

os.system("pyrcc5 " + str(_RSRC_QRC_PATH) + " -o " + str(_RSRC_PY_PATH))

_UI_FILE = _SCRIPT_DIR.parent / Path("data/System/Design/transfer.ui")
form_cls, base_cls = uic.loadUiType(_UI_FILE)


class PairingSenderWorker(QObject, BluetoothResponse):
    sig_paired = pyqtSignal()
    sig_stopped = pyqtSignal()
    
    def __init__(self):
        QObject.__init__(self)
        BluetoothResponse.__init__(self, self.sig_stopped.emit, self.sig_paired.emit)


class PairingReceiverWorker(QObject, BluetoothQuery):
    """
    Worker class for Bluetooth pairing.

    Returns
    -------
    None.

    """

    sig_paired = pyqtSignal(str)
    sig_stopped = pyqtSignal()

    def __init__(self):
        QObject.__init__(self)
        BluetoothQuery.__init__(self, self.sig_stopped.emit, self.sig_paired.emit)


class DiscQueryWorker(QObject, Query):
    """
    Worker class for UDP-based discovery query.

    Returns
    -------
    None.

    """

    sig_dev_found = pyqtSignal(tuple)
    sig_stopped = pyqtSignal()

    def __init__(self):
        QObject.__init__(self)
        Query.__init__(self, self.sig_stopped.emit, self.sig_dev_found.emit)


class DiscRespWorker(QObject, Response):
    """
    Worker class for UDP-based discovery response.

    Returns
    -------
    None.

    """

    sig_stopped = pyqtSignal()

    def __init__(self):
        QObject.__init__(self)
        Response.__init__(self, self.sig_stopped.emit)


class ConnServerWorker(QObject, Server):
    """
    Worker class for TCP-based file-transfer server.

    Returns
    -------
    None.

    """

    sig_connected = pyqtSignal(tuple)
    sig_disconnected = pyqtSignal()
    sig_file_sending = pyqtSignal(str, int, int)
    sig_file_sent = pyqtSignal(str, int, int)

    def __init__(self):
        QObject.__init__(self)
        Server.__init__(self, cb_connected=self.sig_connected.emit,
                        cb_disconnected=self.sig_disconnected.emit,
                        cb_file_sending=self.sig_file_sending.emit,
                        cb_file_sent=self.sig_file_sent.emit)


class ConnClientWorker(QObject, Client):
    """
    Worker class for TCP-based file-transfer client.

    Returns
    -------
    None.

    """

    sig_connected = pyqtSignal()
    sig_disconnected = pyqtSignal()
    sig_files_list_recd = pyqtSignal(list)
    sig_request_files = pyqtSignal(list, Path)
    sig_file_receiving = pyqtSignal(str, int, int)
    sig_file_received = pyqtSignal(str, int, int)

    def __init__(self):
        QObject.__init__(self)
        Client.__init__(self, cb_connected=self.sig_connected.emit,
                        cb_disconnected=self.sig_disconnected.emit,
                        cb_files_list_recd=self.sig_files_list_recd.emit,
                        cb_file_receiving=self.sig_file_receiving.emit,
                        cb_file_received=self.sig_file_received.emit)


class Window(base_cls, form_cls):
    """
    Root widget of the application.

    Returns
    -------
    None.

    """

    def __init__(self):
        super(base_cls, self).__init__()
        self.setupUi(self)
        self.setWindowFlags(Qt.FramelessWindowHint)
        rect = self.frameGeometry()
        cur_pos = QApplication.desktop().cursor().pos()
        scr_id = QApplication.desktop().screenNumber(cur_pos)
        scr_cntr = QApplication.desktop().screenGeometry(scr_id).center()
        rect.moveCenter(scr_cntr)
        self.move(rect.topLeft())
        self.cfg_home()
        self.cfg_send_perf()
        self.cfg_recv_devs()
        self.cfg_recv_files()
        self.cfg_recv_perf()

        self.sent_files = []
        self.recd_files = []

        self.pair_thread = None
        self.pair_worker = None

        self.disc_thread = None
        self.disc_worker = None
        self.conn_thread = None
        self.conn_worker = None

        self.raise_()
        self.activateWindow()
        self.show()
        self.setCurrentIndex(0)

    def receive_callback(self):
        if self.cb_connection_type.currentText() == "Bluetooth":
            print('rx_bt_start() gets called')
            self.rx_bt_start()
        else:
            if self.cb_direct_conn.isChecked():
                self.set_ether_static(2, 1)
            self.rx_eth_disc_start()

    def send_callback(self):
        if self.cb_connection_type.currentText() == "Bluetooth":
            print('tx_bt_init() gets called')
            self.tx_bt_init()
        else:
            if self.cb_direct_conn.isChecked():
                self.set_ether_static(1, 2)
            self.tx_eth_init()

    def cfg_home(self):
        """
        Configure the items on the home screen.

        Returns
        -------
        None.

        """
        self.btn_close.clicked.connect(self.close)
        self.home_btn_recv.clicked.connect(self.receive_callback)
        self.home_btn_send.clicked.connect(self.send_callback)

    def cfg_send_perf(self):
        """
        Configure the items on the Tx-perform screen.

        Returns
        -------
        None.

        """
        self.send_perf_page_back_btn.clicked.connect(
            lambda: self.set_ether_dynamic() if
            self.cb_direct_conn.isChecked() else None)
        self.send_perf_page_succ_btn_no.clicked.connect(
            lambda: self.set_ether_dynamic() if
            self.cb_direct_conn.isChecked() else None)
        self.send_perf_page_succ_btn_yes.clicked.connect(
            lambda: self.set_ether_dynamic() if
            self.cb_direct_conn.isChecked() else None)
        self.send_perf_page_fail_btn_home.clicked.connect(
            lambda: self.set_ether_dynamic() if
            self.cb_direct_conn.isChecked() else None)

        self.send_perf_page_back_btn.clicked.connect(self.tx_disc_stop)
        self.send_perf_page_back_btn.clicked.connect(self.tx_conn_stop)
        self.send_perf_page_back_btn.clicked.connect(
            lambda: self.view("home"))
        self.send_perf_page_succ_btn_no.clicked.connect(
            lambda: self.tx_finish(False))
        self.send_perf_page_succ_btn_yes.clicked.connect(
            lambda: self.tx_finish(True))
        self.send_perf_page_fail_btn_home.clicked.connect(
            lambda: self.view("home"))

    def cfg_recv_devs(self):
        """
        Configure the items on the Rx-device-selection screen.

        Returns
        -------
        None.

        """
        self.recv_devs_btn_back.clicked.connect(self.rx_disc_stop)
        self.recv_devs_btn_back.clicked.connect(
            lambda: self.set_ether_dynamic() if
            self.cb_direct_conn.isChecked() else None)
        self.recv_devs_btn_back.clicked.connect(lambda: self.view("home"))
        self.recv_devs_btn_next.clicked.connect(self.rx_disc_stop)
        self.recv_devs_btn_next.clicked.connect(self.rx_conn_start)
        self.recv_devs_btn_next.clicked.connect(
            lambda: self.view("recv_files"))

    def cfg_recv_files(self):
        """
        Configure the items on the Rx-file-selection screen.

        Returns
        -------
        None.

        """
        self.recv_files_btn_next.clicked.connect(self.rx_conn_req_files)
        self.recv_files_cb_sel_all.stateChanged.connect(
            self.rx_files_sel_all_manual)

    def cfg_recv_perf(self):
        """
        Configure the items on the Rx-perform screen.

        Returns
        -------
        None.

        """
        self.recv_perf_btn_home.clicked.connect(
            lambda: self.set_ether_dynamic() if
            self.cb_direct_conn.isChecked() else None)
        self.recv_perf_btn_home.clicked.connect(
            lambda: self.view("home"))

    def set_ether_static(self, ip, gateway):
        """
        Configure the ethernet device to operate with a static IP.

        Parameters
        ----------
        ip : int
            LSB of the IP address (0-255).
        gateway : int
            LSB of the gateway (0-255).

        Returns
        -------
        None.

        """
        if_names = [name for name in psutil.net_if_addrs().keys()
                    if name.startswith("Ethernet")]
        if_name = None
        for name in if_names:
            if psutil.net_if_stats()[name].isup:
                if_name = name
                break
        if if_name is None:
            print("Failed to find active Ethernet interface")
            self.close()
        if (os.system('netsh interface ipv4 set address name="' + if_name +
                      '" source=static address=172.168.0.' + str(ip) +
                      ' mask=255.255.255.0 gateway=172.168.0.' +
                      str(gateway) + ' store=active')):
            print("Failed to configure the Ethernet interface")
            self.close()
        sleep(5)

    def set_ether_dynamic(self):
        """
        Configure the ethernet device to operate with DHCP.

        Returns
        -------
        None.

        """
        if_name = "Ethernet"
        os.system('netsh interface ipv4 set address name="' + if_name +
                  '" source=dhcp store=active')

    def tx_bt_init(self):
        """
        Configure device to Bluetooth file-sending mode.

        Returns
        -------
        None.

        """
        self.tx_bt_start()
        # self.tx_conn_start()
        self.send_perf_lab.setText("Waiting for receiver . . .")
        self.send_perf_stack.setEnabled(True)
        self.send_perf_stack.setVisible(True)
        self.view("send_perf_page_back", self.send_perf_stack)
        self.view("send_perf")

    def tx_eth_init(self):
        """
        Configure device to Ethernet file-sending mode.

        Returns
        -------
        None.

        """
        self.tx_disc_start()
        self.tx_conn_start()
        self.send_perf_lab.setText("Waiting for receiver . . .")
        self.send_perf_stack.setEnabled(True)
        self.send_perf_stack.setVisible(True)
        self.view("send_perf_page_back", self.send_perf_stack)
        self.view("send_perf")

    def tx_bt_start(self):
        """
        Configure and start auto-discovery responder.

        Returns
        -------
        None.

        """
        self.pair_thread = QThread()
        self.pair_worker = PairingSenderWorker()
        self.pair_worker.moveToThread(self.pair_thread)

        # Execute the worker's run function when the thread is started.
        self.pair_thread.started.connect(self.pair_worker.start)

        # Quit the thread when the worker's job is complete
        self.pair_worker.sig_stopped.connect(self.pair_thread.quit)

        # Delete objects
        self.pair_worker.sig_stopped.connect(self.pair_worker.deleteLater)
        self.pair_thread.finished.connect(self.pair_thread.deleteLater)

        self.pair_thread.start()

    def tx_disc_start(self):
        """
        Configure and start auto-discovery responder.

        Returns
        -------
        None.

        """
        self.disc_thread = QThread()
        self.disc_worker = DiscRespWorker()
        self.disc_worker.moveToThread(self.disc_thread)

        # Execute the worker's run function when the thread is started.
        self.disc_thread.started.connect(self.disc_worker.start)

        # Quit the thread when the worker's job is complete
        self.disc_worker.sig_stopped.connect(self.disc_thread.quit)

        # Delete objects
        self.disc_worker.sig_stopped.connect(self.disc_worker.deleteLater)
        self.disc_thread.finished.connect(self.disc_thread.deleteLater)

        self.disc_thread.start()

    def tx_disc_stop(self):
        """
        Stop auto-discovery responder.

        Returns
        -------
        None.

        """
        # This assignment is performed through a member function of the
        # self.disc_worker object. Since discovery responder is already running
        # infinitely, all other member function calls will stay in the
        # thread-queue forever. Hence, assignment is directly performed,
        # instead of using a member function.
        self.disc_worker.enabled = False

    def tx_conn_start(self):
        """
        Configure and start TCP server.

        Returns
        -------
        None.

        """
        self.sent_files = []
        self.conn_thread = QThread()
        self.conn_worker = ConnServerWorker()
        self.conn_worker.moveToThread(self.conn_thread)

        # Execute the worker's run function when the thread is started.
        self.conn_thread.started.connect(self.conn_worker.accept_conn)

        self.conn_worker.sig_connected.connect(
            lambda x: self.send_perf_lab.setText("Connected to " + x[0]))
        self.conn_worker.sig_connected.connect(self.tx_disc_stop)
        self.conn_worker.sig_connected.connect(self.conn_worker.start_serving)

        self.conn_worker.sig_file_sending.connect(
            lambda x, y, z:
            self.send_perf_lab.setText("Sending file (%d/%d) . . ." %
                                       (y, z)))
        self.conn_worker.sig_file_sent.connect(self.tx_file_sent)

        self.conn_worker.sig_disconnected.connect(self.tx_files_sent)

        # Quit the thread when the worker's job is complete
        self.conn_worker.sig_disconnected.connect(self.conn_thread.quit)

        # Delete objects
        self.conn_worker.sig_disconnected.connect(self.conn_worker.deleteLater)
        self.conn_thread.finished.connect(self.conn_thread.deleteLater)

        self.conn_thread.start()

    def tx_conn_stop(self):
        """
        Stop accepting incoming connections.

        Note
        ----
        This should be called only when the server is accepting new
        connections.

        Returns
        -------
        None.

        """
        self.conn_worker.enabled = False
        self.conn_thread.quit()
        self.conn_worker.deleteLater()
        # Thread need not be deleted in this function. It will be deleted due
        # to the signal emitted when it is quit.

    def tx_file_sent(self, file_name, count, total):
        """
        Update UI when a file is sent.

        Parameters
        ----------
        file_name : str
            Name of the sent file.
        count : int
            Number of files sent (including the current one) in the current
            session.
        total : int
            Total number if files requested by the client.

        Returns
        -------
        None.

        """
        self.sent_files.append(file_name)
        self.send_perf_lab.setText("Sent file (%d/%d) . . ." %
                                   (count, total))

    def tx_files_sent(self):
        """
        Update UI when all files are sent.

        Returns
        -------
        None.

        """
        if len(self.sent_files) > 0:
            self.send_perf_lab.setText("Files sent. Do you want to delete "
                                       "them from this device.")
            self.view("send_perf_page_succ", self.send_perf_stack)
        else:
            self.send_perf_lab.setText("No files sent.")
            self.view("send_perf_page_fail", self.send_perf_stack)
        self.send_perf_stack.setEnabled(True)
        self.send_perf_stack.setVisible(True)

    def tx_finish(self, delete):
        """
        Update UI when the transfer of all requested files in complete.

        Parameters
        ----------
        delete : bool
            If True, the sent files will be deleted from the server.

        Returns
        -------
        None.

        """
        if delete:
            for file_name in self.sent_files:
                (_MEAS_DIR / file_name).unlink()
        self.view("home")

    def rx_bt_start(self):
        """
        Configure and start data receiving from handheld sensor.
        :return: None.
        """
        self.pair_thread = QThread()
        self.pair_worker = PairingReceiverWorker()
        self.pair_worker.moveToThread(self.pair_thread)

        self.pair_thread.started.connect(self.pair_worker.start)

        self.pair_worker.sig_paired.connect(self.rx_paired)

        # self.disc_worker.sig_stopped.connect(self.rx_disc_stopped)

        # Quit the thread when the worker's job is complete
        self.pair_worker.sig_stopped.connect(self.pair_thread.quit)

        # Delete objects
        self.pair_worker.sig_stopped.connect(self.pair_worker.deleteLater)
        self.pair_thread.finished.connect(self.pair_thread.deleteLater)

        # what does this do?
        # while self.recv_devs_scroll_wid_vLay.count() > 0:
        #     child = \
        #         self.recv_devs_scroll_wid_vLay.takeAt(0).widget()
        #     if child is not None:
        #         child.deleteLater()

        self.recv_devs_scroll_wid_vLay.addStretch()
        self.recv_devs_lab.setText("Searching for sender(s) . . .")
        self.recv_devs_btn_next.setEnabled(False)
        self.recv_devs_btn_next.setVisible(False)
        self.view("recv_devs")

        self.pair_thread.start()

    def rx_eth_disc_start(self):
        """
        Configure and start auto-discovery querier.

        Returns
        -------
        None.

        """
        self.disc_thread = QThread()
        self.disc_worker = DiscQueryWorker()
        self.disc_worker.moveToThread(self.disc_thread)

        # Execute the worker's run function when the thread is started.
        self.disc_thread.started.connect(self.disc_worker.start)

        self.disc_worker.sig_dev_found.connect(self.rx_disc_dev_found)

        self.disc_worker.sig_stopped.connect(self.rx_disc_stopped)

        # Quit the thread when the worker's job is complete
        self.disc_worker.sig_stopped.connect(self.disc_thread.quit)

        # Delete objects
        self.disc_worker.sig_stopped.connect(self.disc_worker.deleteLater)
        self.disc_thread.finished.connect(self.disc_thread.deleteLater)

        while self.recv_devs_scroll_wid_vLay.count() > 0:
            child = \
                self.recv_devs_scroll_wid_vLay.takeAt(0).widget()
            if child is not None:
                child.deleteLater()

        self.recv_devs_scroll_wid_vLay.addStretch()
        self.recv_devs_lab.setText("Searching for sender(s) . . .")
        self.recv_devs_btn_next.setEnabled(False)
        self.recv_devs_btn_next.setVisible(False)
        self.view("recv_devs")

        self.disc_thread.start()

    def rx_disc_stop(self):
        """
        Stop the auto-discovery querier.

        Returns
        -------
        None.

        """
        self.disc_worker.stop()

    def rx_disc_stopped(self):
        """
        Update UI when the auto-discovery querier is stopped.

        Returns
        -------
        None.

        """
        devs = self.rx_disc_all_devs()
        if len(devs) == 0:
            self.recv_devs_lab.setText("No devices discovered")
            return
        self.recv_devs_lab.setText("Please select device")

    def rx_paired(self, addr):
        print('rx_paired says: ' + str(addr))

    def rx_disc_dev_found(self, addr):
        """
        Create, configure and show a Radio Button for the discovered device.

        Parameters
        ----------
        addr : tuple
            IP address (str) and port (int) of the discovered device.

        Returns
        -------
        None.

        """
        hostname = socket.gethostbyaddr(addr[0])[0]
        rb = RadioButton(hostname + " (" + addr[0] + ")",
                         self.recv_devs, 18, 5)
        self.recv_devs_scroll_wid_vLay.insertWidget(
            self.recv_devs_scroll_wid_vLay.count() - 1, rb)
        rb.toggled.connect(self.rx_disc_dev_clicked)

    def rx_disc_dev_clicked(self):
        """
        Update UI when any of the Radio Buttons is toggled.

        Returns
        -------
        None.

        """
        b = (self.rx_disc_sel_dev() is not None)
        self.recv_devs_btn_next.setEnabled(b)
        self.recv_devs_btn_next.setVisible(b)

    def rx_disc_all_devs(self):
        """
        Return a list of all discovered devices from the UI.

        Returns
        -------
        devs : list
            List of IP addresses of discovered devices.

        """
        devs = []

        # Last item is stretch
        for i in range(0, self.recv_devs_scroll_wid_vLay.count()):
            widget = self.recv_devs_scroll_wid_vLay.itemAt(i).widget()
            if widget is not None:
                txt = widget.text()
                devs.append(txt[txt.find("(") + 1: txt.find(")")])
        return devs

    def rx_disc_sel_dev(self):
        """
        Return the discovered device selected by the user.

        Returns
        -------
        dev : str or None
            IP address of the device, if a selection has been done.
            None otherwise.

        """
        dev = None

        # Last item is stretch
        for i in range(0, self.recv_devs_scroll_wid_vLay.count()):
            widget = self.recv_devs_scroll_wid_vLay.itemAt(i).widget()
            if (widget is not None) and (widget.isChecked()):
                txt = widget.text()
                dev = txt[txt.find("(") + 1: txt.find(")")]
        return dev

    def rx_conn_start(self):
        """
        Configure and start a TCP client.

        Returns
        -------
        None.

        """
        self.conn_thread = QThread()
        self.conn_worker = ConnClientWorker()
        self.conn_worker.moveToThread(self.conn_thread)

        # Execute the worker's connect function when the thread is started.
        self.conn_thread.started.connect(
            lambda: self.conn_worker.request_conn(self.rx_disc_sel_dev()))

        self.conn_worker.sig_connected.connect(
            self.conn_worker.request_files_list)

        self.conn_worker.sig_files_list_recd.connect(
            self.rx_conn_show_files_list)

        self.conn_worker.sig_request_files.connect(
            self.conn_worker.request_files)

        self.conn_worker.sig_file_receiving.connect(
            lambda x, y, z:
            self.send_perf_lab.setText("Receiving file (%d/%d) . . ." %
                                       (y, z)))

        self.conn_worker.sig_file_received.connect(self.rx_conn_file_received)

        # Quit the thread when the worker's job is complete
        self.conn_worker.sig_disconnected.connect(self.conn_thread.quit)

        # Delete objects
        self.conn_worker.sig_disconnected.connect(self.conn_worker.deleteLater)
        self.conn_thread.finished.connect(self.conn_thread.deleteLater)

        self.conn_thread.start()

    def rx_conn_show_files_list(self, file_names):
        """
        Create, configure and show checkboxes for the specified file names.

        Parameters
        ----------
        file_names : list
            List of names of files to be shown for selection.

        Returns
        -------
        None.

        """
        while self.recv_files_scroll_wid_vLay.count() > 0:
            child = \
                self.recv_files_scroll_wid_vLay.takeAt(0).widget()
            if child is not None:
                child.deleteLater()
        for file_name in file_names:
            cb = CheckBox(file_name.split(".")[0], self.recv_files, 16, 5)
            self.recv_files_scroll_wid_vLay.addWidget(cb)
            cb.stateChanged.connect(self.rx_files_sel_all_auto)
        self.recv_files_scroll_wid_vLay.addStretch()

    def rx_files_sel_all_auto(self, state):
        """
        Change state of the select-all checkbox based on states of other items.

        Parameters
        ----------
        state : bool
            New state for the select-all checkbox.

        Returns
        -------
        None.

        """
        b = (self.rx_conn_sel_files() == self.rx_conn_all_files())

        # Blocking of signals is necessary, otherwise changing state og the
        # checkbox will generate an unncessary toggle signal.
        self.recv_files_cb_sel_all.blockSignals(True)
        self.recv_files_cb_sel_all.setChecked(b)
        self.recv_files_cb_sel_all.blockSignals(False)

    def rx_files_sel_all_manual(self, state):
        """
        Configure checkboxes according to state of the select-all checkbox.

        Parameters
        ----------
        state : bool
            Stateof the select-all checkbox.

        Returns
        -------
        None.

        """
        for i in range(0, self.recv_files_scroll_wid_vLay.count()):
            widget = self.recv_files_scroll_wid_vLay.itemAt(i).widget()
            if widget is not None:
                widget.blockSignals(True)
                widget.setChecked(state)
                widget.blockSignals(False)

    def rx_conn_sel_files(self):
        """
        Return the list of files selected by the user.

        Returns
        -------
        file_names : list
            List of names of files selected by the user.

        """
        file_names = []

        # Last item is stretch
        for i in range(0, self.recv_files_scroll_wid_vLay.count()):
            widget = self.recv_files_scroll_wid_vLay.itemAt(i).widget()
            if (widget is not None) and (widget.checkState()):
                file_names.append(widget.text() + ".pickle")
        return file_names

    def rx_conn_all_files(self):
        """
        Return the list of all files available on the server.

        Returns
        -------
        file_names : list
            List of files available on the server.

        """
        file_names = []

        # Last item is stretch
        for i in range(0, self.recv_files_scroll_wid_vLay.count()):
            widget = self.recv_files_scroll_wid_vLay.itemAt(i).widget()
            if widget is not None:
                file_names.append(widget.text() + ".pickle")
        return file_names

    def rx_conn_req_files(self):
        """
        Configure UI and request files from the server.

        Returns
        -------
        None.

        """
        sel_files = self.rx_conn_sel_files()
        if len(sel_files) > 0:
            fd = QFileDialog(directory=str(_DESKTOP_DIR))
            fd.setFileMode(QFileDialog.Directory)
            fd.setOption(QFileDialog.ShowDirsOnly, True)
            fd.setOption(QFileDialog.DontUseNativeDialog, True)
            dst_dir = _SCRIPT_DIR
            if fd.exec_():
                dst_dir = Path(fd.selectedFiles()[0])
                self.recv_perf_lab.setText("Requesting Files . . .")
                self.recv_perf_btn_home.setEnabled(False)
                self.recv_perf_btn_home.setVisible(False)
                self.view("recv_perf")
                self.conn_worker.sig_request_files.emit(sel_files, dst_dir)
        else:
            self.recv_perf_lab.setText("No files received.")
            self.recv_perf_btn_home.setEnabled(True)
            self.recv_perf_btn_home.setVisible(True)
            self.view("recv_perf")
            self.conn_worker.request_end()

    def rx_conn_file_received(self, file_name, count, total):
        """
        Configure UIwhen a file isreceived.

        Parameters
        ----------
        file_name : str
            Name of the received file.
        count : int
            Number of files received (including the current one) in the current
            session.
        total : int
            Total number if files requested.

        Returns
        -------
        None.

        """
        if count == total:
            self.recv_perf_lab.setText("Files received.")
            self.recv_perf_btn_home.setEnabled(True)
            self.recv_perf_btn_home.setVisible(True)
            self.conn_worker.request_end()
        else:
            self.recv_perf_lab.setText("Received file (%d/%d) . . ." %
                                       (count, total))

    def view(self, widget_name, parent=None):
        """
        Switch the view to the specified page.

        Parameters
        ----------
        widget_name : str
            Name of the page.
        parent : instance of PyQt5.QtWidgets.QWidget, optional
            Parent wigdet whose child widget needs to be switched. If None,
            the root widget is chosen as the parent. The default is None.

        Returns
        -------
        None.

        """
        if parent is None:
            parent = self
        widget_id = parent.indexOf(getattr(self, widget_name))
        parent.setCurrentIndex(widget_id)
