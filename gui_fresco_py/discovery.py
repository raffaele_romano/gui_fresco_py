# -*- coding: utf-8 -*-
"""
Created on Sun Sep 19 00:58:39 2021.

@author: Nishad Mandlik
"""

import socket
from sys import platform
import time
import bluetooth

_QUERIER_PORT = 2017
_RESPONDER_PORT = 2018
_QUERY_MSG = "fresco"
_RESP_MSG = _QUERY_MSG[::-1]
_QUERY_RETRIES = 5
_SOCK_TIMEOUT = 1

_BACKLOG = 1
_BT_QUERIER_PORT = 7
_BT_QUERIER_MAC = bluetooth.read_local_bdaddr()[0]   #'14:13:33:D8:5B:12' 'C4:23:60:F0:C1:88'  # TODO: avoid hardcoding
_BT_RESPONDER_PORT = 7
_BT_RESPONDER_MAC = '14:13:33:D8:5B:12'  # '14:13:33:D8:5B:12' 'C4:23:60:F0:C1:88'  # TODO: avoid hardcoding

"""
InspironRaf 'D8:3B:BF:31:AD:C0' VictusRaf = '14:13:33:D8:5B:12'

may want to use `getmac` command on windows
or parse ipconfig
or `pip install ifconfg`
or this https://superuser.com/questions/892700/is-there-a-way-to-get-only-the-ethernet-mac-address-via-command-prompt-in-window
"""


class BluetoothBase:
    def __init__(self, cb_stopped=None, cb_paired=None):
        print('INITIALIZE SOCKET')
        self.sock = socket.socket(socket.AF_BLUETOOTH, socket.SOCK_STREAM, socket.BTPROTO_RFCOMM)

        if cb_stopped is None:
            self.cb_stopped = lambda: None
        else:
            self.cb_stopped = cb_stopped

        if cb_paired is None:
            self.cb_paired = lambda: None
        else:
            print(cb_paired)
            self.cb_paired = cb_paired

    def __del__(self):
        """
        Close the socket while destroying the object.

        Returns
        -------
        None.

        """
        self.sock.close()
        self.cb_stopped()


class BluetoothQuery(BluetoothBase):
    def __init__(self, cb_stopped=None, cb_paired=None):
        super().__init__(cb_stopped=cb_stopped, cb_paired=cb_paired)
        self.client = None
        self.state = 0
        self.disc_devs = []
        self.reset_disc_devs()
        print('THIS HAS TO RUN AT MOST ONCE!')
        self.sock.bind((_BT_QUERIER_MAC, _BT_QUERIER_PORT))

    def reset_disc_devs(self):
        """
        Clear the list of discovered devices.

        Returns
        -------
        None.

        """
        self.disc_devs = []

    def start(self):
        """
        Start broadcasting queries for discovery.

        Returns
        -------
        None.

        """
        self.sock.listen(_BACKLOG)
        self.client, address = self.sock.accept()
        print(f'[+] {address} is connected.')
        print(address)
        # input("Press Enter to continue...")
        self.cb_paired(address[0])
        # self.cb_paired()

    def get_disc_devs(self):
        """
        Return the list of discovered devices.

        Returns
        -------
        list
            List of address and port numbers of discovered devices.

        """
        return self.disc_devs

    def stop(self):
        """
        Stop sending discovery-queries.

        Note
        ----
        Sending of queries will be automatically stopped after a certain number
        of attempts. Thus, calling this function is not necessary. But it can
        be used to stop the queries before the maximum number of attempts is
        reached. Calling this function when discovery is not running has no
        effect.

        Returns
        -------
        None.

        """
        self.state = 2


class BluetoothResponse(BluetoothBase):
    """
    Responder class for BT discovery.

    Parameters
    ----------
    cb_stopped : function, optional
        See :py:class:`discovery.BluetoothBase`

    Returns
    -------
    None.

    """

    def __init__(self, cb_stopped=None, cb_paired=None):
        super().__init__(cb_stopped=cb_stopped, cb_paired=cb_paired)
        self.enabled = False

    def start(self):
        """
        Start responding to discovery-queries.

        Returns
        -------
        None.

        """
        self.enabled = True
        self.sock.connect((_BT_RESPONDER_MAC, _BT_RESPONDER_PORT))
        self.cb_paired(_BT_RESPONDER_MAC)

    def stop(self):
        """
        Stop responding to discovery-queries.

        Returns
        -------
        None.

        """
        self.enabled = False


class Base:
    """
    Base class for UDP-based auto-discovery.

    Parameters
    ----------
    port : int
        Port to be used for the service.
    cb_stopped : function, optional
        User-defined callback for discovery-stopped event. The function
        shouldn't accept any arguments. The default is None.

    Returns
    -------
    None.

    """

    def __init__(self, port, cb_stopped=None):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        if (platform == "linux") or (platform == "linux2"):
            self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
        if platform == "win32":
            self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind(("", port))
        self.sock.settimeout(_SOCK_TIMEOUT)

        if cb_stopped is None:
            self.cb_stopped = lambda: None
        else:
            self.cb_stopped = cb_stopped

    def __del__(self):
        """
        Close the socket while destroying the object.

        Returns
        -------
        None.

        """
        self.sock.close()


class Query(Base):
    """
    Querier class for UDP-based auto-discovery.

    Parameters
    ----------
    cb_stopped : function, optional
        See :py:class:`discovery.Base`
    cb_dev_disc : function, optional
        User-defined callback for device-discovered event. The function
        should accept a single argument which is the tuple containing the IP
        address and port of the discovered device. The default is None.

    Returns
    -------
    None.

    """

    def __init__(self, cb_stopped=None, cb_dev_disc=None):
        super().__init__(_QUERIER_PORT, cb_stopped=cb_stopped)
        self.state = 0
        self.reset_disc_devs()
        if cb_dev_disc is None:
            self.cb_dev_disc = lambda x: None
        else:
            self.cb_dev_disc = cb_dev_disc

    def start(self):
        """
        Start broadcasting queries for discovery.

        Returns
        -------
        None.

        """
        i = 0
        start_ts = 0
        self.reset_disc_devs()
        self.state = 0
        while 1:
            if self.state == 0:
                if i == _QUERY_RETRIES:
                    self.state = 2
                else:
                    self.sock.sendto(_QUERY_MSG.encode("utf-8"),
                                     ('<broadcast>', _RESPONDER_PORT))
                    i += 1
                    start_ts = time.time()
                    self.state = 1
            elif self.state == 1:
                if time.time() - start_ts < _SOCK_TIMEOUT:
                    try:
                        msg, addr = self.sock.recvfrom(20)
                    except socket.timeout:
                        continue
                    if ((msg.decode("utf-8") == _RESP_MSG) and
                            (addr not in self.disc_devs)):
                        self.disc_devs.append(addr)
                        self.cb_dev_disc(addr)
                else:
                    # Next retry
                    self.state = 0
            elif self.state == 2:
                self.state = 0
                break
        self.cb_stopped()

    def get_disc_devs(self):
        """
        Return the list of discovered devices.

        Returns
        -------
        list
            List of address and port numbers of discovered devices.

        """
        return self.disc_devs

    def reset_disc_devs(self):
        """
        Clear the list of discovered devices.

        Returns
        -------
        None.

        """
        self.disc_devs = []

    def stop(self):
        """
        Stop sending discovery-queries.

        Note
        ----
        Sending of queries will be automatically stopped after a certain number
        of attempts. Thus, calling this function is not necessary. But it can
        be used to stop the queries before the maximum number of attempts is
        reached. Calling this function when discovery is not running has no
        effect.

        Returns
        -------
        None.

        """
        self.state = 2


class Response(Base):
    """
    Responder class for UDP-based auto-discovery.

    Parameters
    ----------
    cb_stopped : function, optional
        See :py:class:`discovery.Base`

    Returns
    -------
    None.

    """

    def __init__(self, cb_stopped=None):
        super().__init__(_RESPONDER_PORT, cb_stopped=cb_stopped)
        self.enabled = False

    def start(self):
        """
        Start responding to discovery-queries.

        Returns
        -------
        None.

        """
        self.enabled = True
        while self.enabled:
            try:
                msg, addr = self.sock.recvfrom(20)
            except socket.timeout:
                continue
            if msg.decode("utf-8") == _QUERY_MSG:
                self.sock.sendto(_RESP_MSG.encode("utf-8"), addr)
        self.enabled = False
        self.cb_stopped()

    def stop(self):
        """
        Stop responding to discovery-queries.

        Returns
        -------
        None.

        """
        self.enabled = False
