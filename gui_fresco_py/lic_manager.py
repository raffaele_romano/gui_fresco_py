# -*- coding: utf-8 -*-
"""
Created on Tue Aug  3 11:37:39 2021.

@author: Nishad Mandlik
"""

from gui_fresco_py import crypto
from gui_fresco_py.hierarchy import Collection as Coll
from pathlib import Path
import os
import sys

_SCRIPT_DIR = Path(__file__).parent
_LIC_FILE_PATH = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'data', 'User', 'license.lic')


class License:
    """
    Class for managing the license.

    Parameters
    ----------
    key : str, optional
        Password for the license. The default is "Vertigo_2017".
    reset : bool, optional
        If True, a blank license is initialized. The default is False.

    Returns
    -------
    None.

    """

    # TODO: load the key from a file maybe?
    def __init__(self, key="Vertigo_2017", reset=False):
        self.lic_path = Path(_LIC_FILE_PATH)
        self.key = key
        if reset:
            self.license = {}
        else:
            with self.lic_path.open("rb") as f:
                self.license = crypto.decrypt(f.read(), self.key)

    def set_company(self, company_name):
        """
        Set the company name in the license.

        Parameters
        ----------
        company_name : str
            Name of the issuing company.

        Returns
        -------
        None.

        """
        self.license["company"] = company_name
        self.__save_file()

    def set_license_id(self, license_id):
        """
        Set the license ID.

        Parameters
        ----------
        license_id : str
            License ID.

        Returns
        -------
        None.

        """
        self.license["license_id"] = license_id
        self.__save_file()

    def get_client_id(self):
        """
        Return the client ID.

        Returns
        -------
        str
            Client ID.

        """
        return self.license.get("client_id")

    def set_client_id(self, client_id):
        """
        Set the ID of the client to whom the license is issued.

        Parameters
        ----------
        client_id : str
            Client ID.

        Returns
        -------
        None.

        """
        self.license["client_id"] = client_id
        self.__save_file()

    def get_client_name(self):
        """
        Return the client name.

        Returns
        -------
        str
            Client Name.

        """
        return self.license.get("client_name")

    def set_client_name(self, client_name):
        """
        Set the name of the client to whom the license is issued.

        Parameters
        ----------
        client_name : str
            Name of the client.

        Returns
        -------
        None.

        """
        self.license["client_name"] = client_name
        self.__save_file()

    def get_collection(self):
        """
        Return the collection of licensed fruits, breeds and attributes.

        Returns
        -------
        hierarchy.Collection
            Collection of licensed components.

        """
        coll_name = self.license.get("coll_name")
        if coll_name is None:
            fruits = None
        else:
            fruits = self.license.get(coll_name)

        return Coll(coll_name, fruits=fruits)

    def set_collection(self, coll):
        """
        Update the licensed collection.

        Parameters
        ----------
        coll : hierarchy.Collection
            New collection which needs to be updated in the license.

        Returns
        -------
        None.

        """
        curr_coll_name = self.license.get("coll_name")
        if (curr_coll_name is not None and
                self.license.get(curr_coll_name) is not None):
            self.license.pop(curr_coll_name)
        self.license["coll_name"] = coll.get_name()
        if (sys.version_info.major == 3 and
                sys.version_info.minor >= 9):
            # Introduced in Python 3.9
            self.license |= coll.to_dict()
        else:
            # TODO: Deprecated. Will be removed in the future.
            # Python 3.9 is already available for Windows.
            # Ubuntu 22.04 (Next LTS Release) will also have 3.9 or
            # higher
            self.license == {**self.license, **(coll.to_dict())}
        self.__save_file()

    def __save_file(self):
        """
        Encrypt and save the license information to a file.

        Returns
        -------
        None.

        """
        with self.lic_path.open("wb") as f:
            f.write(crypto.encrypt(self.license, self.key))
