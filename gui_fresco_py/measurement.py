# -*- coding: utf-8 -*-
"""
Created on Mon Aug 16 10:12:13 2021.

@author: Nishad Mandlik
"""

from datetime import datetime as dt
import numpy as np
from pathlib import Path
import pickle

_SCRIPT_DIR = Path(__file__).parent
_MEAS_DIR = Path("data/User/Measurements")


class Meas:
    """
    Class for holding measurements.

    Parameters
    ----------
    session_name : str
        Name of the measurement session.
    n_fruits : int
        Number of fruits to be measured.
    n_meas : int
        Number of measurements to be performed per fruit.
    freq_vect : numpy.ndarray
        Frequency vector.
    vna_name: str
        Name of the VNA model used for measurement.
    vna_sn: str
        Serial number of the VNA used for measurement.
    power : str or float
        VNA power setting.
    cal_eterms : dict or None, optional
        Error terms that will be used for measurement correction.
        Default is None.
    info : str or None, optional
        Additional info to be stored with the measurement. Default is None.

    Returns
    -------
    None.

    """

    def __init__(self, session_name, n_fruits, n_meas, n_attrs, freq_vect, vna_name, vna_sn,
                 power, cal_eterms=None, info=None, attributes=None):
        self.name = session_name
        n_points = freq_vect.size
        self.s11_raw = np.empty((n_fruits, n_meas, n_points),
                                dtype=complex)
        self.s11_raw[:] = np.NaN
        self.s11 = np.empty((n_fruits, n_meas, n_points), dtype=complex)
        self.s11[:] = np.NaN
        self.attributes = attributes
        self.pred_attrs = np.empty((n_fruits, n_meas, n_attrs), dtype=float)
        self.freq = freq_vect
        self.vna_name = vna_name
        self.vna_sn = vna_sn
        self.power = power
        self.cal_eterms = cal_eterms
        self.info = info
        self.sample_count = -1
        self.sensorT_vct = np.empty((n_fruits, n_meas), dtype=float)  # float but it might be int
        # self.sensorT_vct = np.NaN

    def add_meas(self, s11_raw, s11=None, sensor_t=None, attrs=None):
        """
        Add a measurement to the object.

        Parameters
        ----------
        s11_raw : numpy.ndarray
            Raw measurement.
        s11 : numpy.ndarray, optional
            Corrected measurement. The default is None.

        sensor_t : vna temperature. The default is None

        attrs : list with predicted attributes
                The default is None.

        Returns
        -------
        None.

        """
        self.sample_count += 1
        fruit_id = self.sample_count // self.n_meas
        meas_id = self.sample_count % self.n_meas
        self.s11_raw[fruit_id, meas_id] = s11_raw
        self.s11[fruit_id, meas_id] = (s11 if s11 is not None else s11_raw)
        self.pred_attrs[fruit_id, meas_id, :] = attrs
        self.sensorT_vct[fruit_id, meas_id] = sensor_t
        #   add here attributes prediction
        #   pred_attrs = np.empty((n_fruits, n_meas, n_attrs)

    def get_sample(self, sample_id):
        """
        Return the arrays for the specified measurement index.

        Parameters
        ----------
        sample_id : int
            Sample index of the measurement to be returned.

        Returns
        -------
        numpy.ndarray
            Raw measurement.
        numpy.ndarray
            Corrected measurement.

        Sensor temperature.

        list with attributes.

        """
        fruit_id = sample_id // self.n_meas
        meas_id = sample_id % self.n_meas
        return (self.s11_raw[fruit_id, meas_id], self.s11[fruit_id, meas_id], self.sensorT_vct[fruit_id, meas_id],
                self.pred_attrs[fruit_id, meas_id, :])

    def get_latest_sample(self):
        """
        Return the arrays for the latest measurement.

        Returns
        -------
        tuple of numpy.ndarray
            Latest raw and corrected measurements.

        """
        return self.get_sample(self.sample_count)

    def is_started(self):
        """
        Check if the measurement has started.

        Returns
        -------
        bool
            True, if the measurement has started. False, otherwise.

        """
        return self.sample_count > -1

    def is_complete(self):
        """
        Check if the measurement is complete.

        Returns
        -------
        bool
            True, if the measurement is complete. False, otherwise.

        """
        return (self.sample_count ==
                (self.s11.shape[0] * self.s11.shape[1]) - 1)

    @staticmethod
    def from_pkl(file_name, parent_dir=None):
        """
        Load the measurement object from file.

        Parameters
        ----------
        file_name : str
            File name for the saved measurement.
        parent_dir : str or pathlib.Path or None, optional
            Parent directory which holds the measurement files. If None, then
            the default measurement directory is considered.
            The default is None.

        Returns
        -------
        obj : measurement.Meas
            Measurement loaded from disk.

        """
        if file_name[-7:] != ".pickle":
            file_name += ".pickle"

        parent_dir = _MEAS_DIR if (parent_dir is None) else Path(parent_dir)

        with (parent_dir / file_name).open("rb") as f:
            obj = pickle.load(f)
        return obj

    def to_pkl(self):
        """
        Save the measurement to file.

        Returns
        -------
        None.

        """
        _MEAS_DIR.mkdir(parents=True, exist_ok=True)

        filename = self.name + "_" + dt.now().strftime("%Y%m%d_%H%M%S")
        with (_MEAS_DIR / (filename + ".pickle")).open("wb") as f:
            pickle.dump(self, f)

    @property
    def n_fruits(self):
        """
        Return the number of fruits to be measured.

        Returns
        -------
        int
            Number of fruits to be measured.

        """
        return self.s11.shape[0]

    @property
    def n_meas(self):
        """
        Return the number of measurements per fruit.

        Returns
        -------
        int
            Number of measurements per fruit.

        """
        return self.s11.shape[1]

    @property
    def n_points(self):
        """
        Return the number of frequency points for measurement.

        Returns
        -------
        int
            Number of frequency points for measurement..

        """
        return self.s11.shape[2]

    def __getitem__(self, k):
        """
        Return an attribute value using dict-like indexing.

        If the attribute is not present, then None is returned.

        Parameters
        ----------
        k : str
            Attribute name.

        """
        return getattr(self, k, None)
