import serial
import time
import serial.tools.list_ports


class Gauge:
    """

    Class for communicating with DFR gauge

    --------------------------------------
    Parameters
        com_port : string
        demo : boolean

    """

    def __init__(self, demo=False):
        self.last_voltage = None
        self.actual_voltage = None
        self.last_percentage = None
        self.actual_percentage = None
        self.is_present = False
        self.demo = demo

        if not demo:

            ports = list(serial.tools.list_ports.comports())
            for p in ports:
                if 'LattePanda Leonardo' in p.description:
                    print('Initializing COM port')
                    # Connection to port
                    # s = serial.Serial(p.device)
                    self.ser = serial.Serial(p.device, 9600, timeout=1)
                    i = 'Begin'
                    # print('Serial obj created')
                    self.ser.write(i.encode())
                    # print('Serial written')
                    time.sleep(0.75)
                    error = self.ser.readline().decode('ascii').strip()

                    if error == '0':
                        self.is_present = True


    def read_voltage(self):
        if not self.is_present:
            if self.demo:
                return '15.05'
        else:
            i = 'ReadV'
            self.ser.write(i.encode())
            time.sleep(0.5)
            return self.ser.readline().decode('ascii').strip()

    def read_percentage(self):
        if not self.is_present:
            if self.demo:
                return '86'
        else:
            i = 'ReadP'
            self.ser.flush()
            # self.ser.flushInput()
            # self.ser.flushOutput()
            time.sleep(0.1)
            self.ser.write(i.encode())
            time.sleep(0.5)
            perc = self.ser.readline().decode('ascii').strip()
            # print(perc)
            return perc

    def close_serial(self):
        if self.is_present:
            self.ser.close()
