# -*- coding: utf-8 -*-
"""
Created on Wed Aug  4 08:30:53 2021.

@author: Nishad Mandlik
"""

from copy import deepcopy as dc
from datetime import datetime
import json
import sys


class Base:
    """
    Base class for various levels in the hierarchical fruit structure.

    Parameters
    ----------
    name : str
        Name of the class object.
    child_type : type or None
        Type of the immediate children in the hierarchy.
    children : obj or list or dict or None, optional
        See :py:func:`hierarchy.Base.update_children`. The default is None.

    Returns
    -------
    None.

    """

    def __init__(self, name, child_type, children=None):
        self.name = name.lower()
        self.child_type = child_type
        self.update_children(children, True)

    def get_name(self):
        """
        Return the name of the object.

        Returns
        -------
        str
            Name of the object.

        """
        return self.name.capitalize()

    def get_child_names(self):
        """
        Return the names of the immediate children.

        Returns
        -------
        list
            Names of the immediate children.

        """
        return [child.name.capitalize() for child in self.children]

    def get_children(self):
        """
        Return a list immediate child objects.

        Returns
        -------
        list
            Immediate child objects.

        """
        return self.children

    def update_children(self, children, reset):
        """
        Update the existing list of children according to the supplied parameters.

        Parameters
        ----------
        children : obj or list or dict or str or None
            If a single object or list of objects is passed, the current list
            is updated with those object(s). If a dictionary or a JSON string
            is passed, child objects are created accordingly,
            and the current list is updated.
        reset : bool
            If True, the current list of children is discarded and replaced
            by a list of new objects. Otherwise, new objects are appended to
            the current list. However, if a child with the same name is found
            in the existing list, it is replaced with a new one.

        Returns
        -------
        None.

        """
        if reset:
            self.reset_children()

        if children is None:
            return

        if self.child_type is None:
            return

        if type(children) == str:
            children = json.loads(children)

        if type(children) == self.child_type:
            children = [children]
        elif type(children) == dict:
            tmp = []
            for key in children:
                child = self.child_type(key.lower())
                child.update_children(children[key], reset=True)
                tmp.append(child)
            children = tmp

        for new_child in children:
            self.remove_child(new_child.name)
            self.children.append(dc(new_child))

    def reset_children(self):
        """
        Remove all children attached to the current object.

        Returns
        -------
        None.

        """
        self.children = []

    def get_child(self, name):
        """
        Return the child corresponding to the given name.

        Parameters
        ----------
        name : str
            Name of the child object.

        Returns
        -------
        obj or None
            Child object matching the given name, or None.

        """
        name = name.lower()
        child = None
        for child in self.children:
            if child.name == name:
                return child
        return None

    def remove_child(self, name):
        """
        Remove the child object corresponding to the given name.

        Parameters
        ----------
        name : str
            Name of the child to be removed.

        Returns
        -------
        None.

        """
        name = name.lower()
        self.children = [child for child in self.children
                         if (child.name != name)]

    def to_dict(self):
        """
        Convert the subsequent hierarchical structure (starting from the current object) to a Python dictionary.

        Returns
        -------
        dict
            Dictionary representing the subsequent hierarchical structure.

        """
        tmp = {}
        if self.child_type is not None and len(self.children) != 0:
            for child in self.children:
                if (sys.version_info.major == 3 and
                        sys.version_info.minor >= 9):
                    # Introduced in Python 3.9
                    tmp |= child.to_dict()
                else:
                    # TODO: Deprecated. Will be removed in the future.
                    # Python 3.9 is already available for Windows.
                    # Ubuntu 22.04 (Next LTS Release) will also have 3.9 or
                    # higher
                    tmp == {**tmp, **(child.to_dict())}

        return {self.name.capitalize(): tmp}

    def to_json(self):
        """
        Convert the subsequent hierarchical structure (starting from the current object) to a JSON string.

        Returns
        -------
        str
            JSON string representing the subsequent hierarchical structure.

        """
        return json.dumps(self.to_dict(), indent=4)

    def __getitem__(self, name):
        """
        Equivalent to :py:func:`hierarchy.Base.get_child`.

        This magic function allows child objects to be accessed via
        dictionary-like indexing.

        """
        return self.get_child(name)


class Collection(Base):
    """
    Class representing collection of multiple fruits.

    Parameters
    ----------
    name : str
        Name of the collection.
    fruits : fruit.Fruit or list or dict or str or None, optional
        See :py:func:`hierarchy.Base.update_children`. The default is None.

    Returns
    -------
    None.

    """

    def __init__(self, name=None, fruits=None):
        if name is None:
            name = "coll_" + datetime.today().strftime('%Y%m%d-%H%M%S')
        super().__init__(name, Fruit, fruits)

    def get_fruit_names(self):
        """
        Return the names of the associated fruits.

        See :py:func:`hierarchy.Base.get_child_names`.

        """
        return self.get_child_names()

    def get_fruits(self):
        """
        Return the associated fruit objects.

        See :py:func:`hierarchy.Base.get_children`.

        """
        return self.get_children()

    def update_fruits(self, fruits, reset=False):
        """
        Update the existing list of fruits according to the supplied parameters.

        See :py:func:`hierarchy.Base.update_children`.

        """
        return self.update_children(fruits, reset)

    def reset_fruits(self):
        """
        Remove all fruits from the collection.

        See :py:func:`hierarchy.Base.reset_children`.

        """
        return self.reset_children()

    def get_fruit(self, name):
        """
        Return the fruit corresponding to the given name.

        See :py:func:`hierarchy.Base.get_child`.

        """
        return self.get_child(name)

    def remove_fruit(self, name):
        """
        Remove the fruit object corresponding to the given name.

        See :py:func:`hierarchy.Base.remove_child`.

        """
        return self.remove_child(name)


class Fruit(Base):
    """
    Class for fruit.

    Parameters
    ----------
    name : str
        Name of the fruit.
    breeds : fruit.Breed or list or dict or str or None, optional
        See :py:func:`hierarchy.Base.update_children`. The default is None.

    Returns
    -------
    None.

    """

    def __init__(self, name, breeds=None):
        super().__init__(name, Breed, breeds)

    def get_breed_names(self):
        """
        Return the names of the associated breeds.

        See :py:func:`hierarchy.Base.get_child_names`.

        """
        return self.get_child_names()

    def get_breeds(self):
        """
        Return the associated breed objects.

        See :py:func:`hierarchy.Base.get_children`.

        """
        return self.get_children()

    def update_breeds(self, breeds, reset=False):
        """
        Update the existing list of breeds according to the supplied parameters.

        See :py:func:`hierarchy.Base.update_children`.

        """
        return self.update_children(breeds, reset)

    def reset_breeds(self):
        """
        Remove all breeds attached to the current fruit.

        See :py:func:`hierarchy.Base.reset_children`.

        """
        return self.reset_children()

    def get_breed(self, name):
        """
        Return the breed corresponding to the given name.

        See :py:func:`hierarchy.Base.get_child`.

        """
        return self.get_child(name)

    def remove_breed(self, name):
        """
        Remove the breed object corresponding to the given name.

        See :py:func:`hierarchy.Base.remove_child`.

        """
        return self.remove_child(name)


class Breed(Base):
    """
    Class for breed.

    Parameters
    ----------
    name : str
        Name of the breed.
    attrs : fruit.Attr or list or dict or str or None, optional
        See :py:func:`hierarchy.Base.update_children`. The default is None.

    Returns
    -------
    None.

    """

    def __init__(self, name, attrs=None):
        super().__init__(name, Attr, attrs)

    def get_attr_names(self):
        """
        Return the names of the associated attributes.

        See :py:func:`hierarchy.Base.get_child_names`.

        """
        return self.get_child_names()

    def get_attrs(self):
        """
        Return the associated attribute objects.

        See :py:func:`hierarchy.Base.get_children`.

        """
        return self.get_children()

    def update_attrs(self, attrs, reset=False):
        """
        Update the existing list of attributes according to the supplied parameters.

        See :py:func:`hierarchy.Base.update_children`.

        """
        return self.update_children(attrs, reset)

    def reset_attrs(self):
        """
        Remove all attributes attached to the current frbreeduit.

        See :py:func:`hierarchy.Base.reset_children`.

        """
        return self.reset_children()

    def get_attr(self, name):
        """
        Return the attribute corresponding to the given name.

        See :py:func:`hierarchy.Base.get_child`.

        """
        return self.get_child(name)

    def remove_attr(self, name):
        """
        Remove the attribute object corresponding to the given name.

        See :py:func:`hierarchy.Base.remove_child`.

        """
        return self.remove_child(name)


class Attr(Base):
    """
    Class for attribute.

    Parameters
    ----------
    name : str
        Name of the attribute.

    Returns
    -------
    None.

    """

    def __init__(self, name):
        super().__init__(name, None)
