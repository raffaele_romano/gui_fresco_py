# -*- coding: utf-8 -*-
"""
Created on Mon Aug 16 10:11:51 2021.

@author: Nishad Mandlik
"""

from datetime import datetime as dt
import json
import numpy as np
from pathlib import Path

_SCRIPT_DIR = Path(__file__).parent
_DEFAULT_SETTINGS_FILE = _SCRIPT_DIR / Path("../data/System/Settings/default.json")
_USER_SETTINGS_FILE = Path("data/User/Settings/user.json")


class Settings():
    """
    Class for the VNA and measurement settings used by the program.

    Parameters
    ----------
    vna_name : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """

    def __init__(self, vna_name):
        _USER_SETTINGS_FILE.parent.mkdir(parents=True, exist_ok=True)

        # This will also initialize the __commit variable
        self._disable_commit()

        self.vna = vna_name

        dflt = self.get_defaults()
        self._disable_commit()
        for k in dflt:
            self[k] = dflt[k]
        if _USER_SETTINGS_FILE.is_file():
            user = self.get_user_defined()
            for k in user:
                self[k] = user[k]

        # Save is required because user config might not exist or
        # the default config may have more entries than the user config
        self._enable_commit()

    def save(self):
        """
        Save the current configuration if it differs from current user-config, or if new entries have been added to the default-config.

        Returns
        -------
        None.

        """
        dflt = self.get_defaults()
        user_prev = {}
        settings_dict = {}
        if _USER_SETTINGS_FILE.is_file():
            # Get all user-settings for all VNAs. This is needed because the
            # settings file may contain settings for multiple VNA models.
            # Only those for the current model need to be operated on. The rest
            # should remain as they are.
            with _USER_SETTINGS_FILE.open("r") as f:
                settings_dict = json.load(f)

            # Get settings for current VNA model
            user_prev = self.get_user_defined()

            if ("datetime" in user_prev):
                user_prev.pop("datetime")

                # If the already-saved entries match the current settings,
                # and if no new entries are present in the defaults, then,
                # no need to save.

                # Entries in saved settings which do not match the current ones
                tmp1 = [k for k in user_prev
                        if (user_prev[k] != self.__dict__[k])]

                # Entries in default settings which are not present in the
                # saved settings
                tmp2 = [k for k in dflt if k not in user_prev]
                if (len(tmp1) == 0 and len(tmp2) == 0):
                    # No need to save
                    return

        # Get the current value. If not available, get value from defaults.
        user_new = {k: getattr(self, k, dflt[k]) for k in dflt}
        user_new["datetime"] = dt.now().strftime("%Y%m%d_%H%M%S")
        settings_dict[self.vna] = user_new
        with _USER_SETTINGS_FILE.open("w") as f:
            json.dump(settings_dict, f, indent=4)

    def get_defaults(self):
        """
        Get the default configuration.

        Returns
        -------
        dict
            Dictionary containing the parameters and their default values.

        """
        return self._get_from_file(_DEFAULT_SETTINGS_FILE)

    def get_user_defined(self):
        """
        Get the user-defined configuration.

        Returns
        -------
        dict
            Dictionary containing the parameters and their user-defined values.

        """
        return self._get_from_file(_USER_SETTINGS_FILE)

    def get_freq_vect(self):
        """
        Return the vector containing the frequency points at which the measurements will be performed.

        Returns
        -------
        numpy.ndarray
            Array containing the frequency points.

        """
        return np.linspace(self.start_freq, self.stop_freq, self.n_points)

    def set_freq_range(self, start, stop, n):
        """
        Set the frequency range to be used for measurements.

        Parameters
        ----------
        start : float
            Lower-limit of the frequency range (in Hz).
        stop : float
            Upper-limit of the frequency range (in Hz).
        n : int
            Number of frequency points (including the the upper and lower
                                        limits).

        Returns
        -------
        None.

        """
        self._disable_commit()
        self.start_freq = start
        self.stop_freq = stop
        self.n_points = n
        self._enable_commit()

    def get_freq_range(self):
        """
        Return the values that define the frequency range for measurements.

        Returns
        -------
        float
            Lower-limit of the frequency range (in Hz).
        float
            Upper-limit of the frequency range (in Hz).
        float
            Number of frequency points (including the the upper and lower
            limits).

        """
        return (self.start_freq, self.stop_freq, self.n_points)

    def set_n_scans(self, n_fruits, n_meas):
        """
        Set the number of scans to be performed.

        Parameters
        ----------
        n_fruits : int
            Number of fruits to be measured.
        n_meas : int
            Number of measurements per fruit.

        Returns
        -------
        None.

        """
        self._disable_commit()
        self.n_fruits = n_fruits
        self.n_meas = n_meas
        self._enable_commit()

    def get_n_scans(self):
        """
        Return the parameters defining the number of scans to be performed.

        Returns
        -------
        int
            Number of fruits to be measured.
        int
            Number of measurements per fruit.

        """
        return (self.n_fruits, self.n_meas)

    def _disable_commit(self):
        """
        Disable saving of changed settings to file.

        Note
        ----
        The setting is maintained via a boolean variable, which is set to
        False by this function. Python performs this operation via the
        __setattr__ function. The custom __setattr__ function can perform an
        additional save-to-disk operation.
        See :py:func:`settings.Settings.__setattr__`.
        However, the user need not worry about disabling the commit flag.
        No save-to-disk will occur while the commit flag is being set to False.

        Attention
        ---------
        This function is intended for internal use by member functions. When
        multiple settings are changed in a function, it is redundant to write
        to disk after setting the value of each parameter. It is more
        efficient to save changes to disk after all the necessary parameters
        have been updated. This technique has already been implemented in the
        member functions, and should not be directly performed through objects.

        Returns
        -------
        None.

        """
        self.__commit = False

    def _enable_commit(self):
        """
        Enable saving of changed settings to file.

        Note
        ----
        The setting is maintained via a boolean variable, which is set to
        False by this function. Python performs this operation via the
        __setattr__ function. The custom __setattr__ function can perform an
        additional save-to-disk operation.
        See :py:func:`settings.Settings.__setattr__`.
        After updating the settings and enabling the commit flag, the updated
        settings will be automatically saved to disk.
        The :py:func:`settings.Settings.save` function does not need not be
        separately called.

        Attention
        ---------
        This function is intended for internal use by member functions. When
        multiple settings are changed in a function, it is redundant to write
        to disk after setting the value of each parameter. It is more
        efficient to save changes to disk after all the necessary parameters
        have been updated. This technique has already been implemented in the
        member functions, and should not be directly performed through objects.

        Returns
        -------
        None.

        """
        # This will also automatically save the config since setattr will
        # encounter __commit=True
        self.__commit = True

    def _get_from_file(self, file):
        """
        Load the settings dictionary from a JSON file.

        Parameters
        ----------
        file : pathlib.Path
            Path of the JSON file

        Returns
        -------
        dict
            Dictionary containing the settings.

        """
        with file.open("r") as f:
            settings_dict = json.load(f).get(self.vna, {})
        return settings_dict

    def __setattr__(self, k, v):
        """
        Assign values to the object attributes.

        Note
        ----
        The object dictionary is first updated with the given key and value.
        After this, if the commit flag is True, then the
        :py:func:`settings.Settings.save` function is called.

        Parameters
        ----------
        k : str
            Attribute name.
        v : any
            Attribute Value.

        Returns
        -------
        None.

        """
        self.__dict__[k] = v
        if (self.__commit):
            self.save()

    def __getattr__(self, k):
        """
        Return None since the specified attribute is not found.

        Note
        ----
        This function is called only when the normal method __getattribute__
        fails. Hence, it can be said that if this function has been called,
        then the attribute is not available in the object.

        Parameters
        ----------
        k : str
            Attribute name.

        Returns
        -------
        None.

        """
        # Called only if __getattribute__ fails (attribute is not found)
        return None

    def __getitem__(self, k):
        """
        Return the value corresponding to the given attribute name.

        Note
        ----
        This function has been implemented to allow dict-type indexing.
        If the attribute is not found, None is returned.
        See :py:func:`getattr`

        """
        return getattr(self, k, None)

    def __setitem__(self, k, v):
        """
        Alias for :py:func:`settings.Settings.__setattr__`

        Note
        ----
        This function has been implemented to allow dict-type indexing.

        """
        return setattr(self, k, v)
