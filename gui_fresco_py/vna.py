# -*- coding: utf-8 -*-
"""
Created on Mon Aug 16 10:10:12 2021.

@author: Nishad Mandlik
"""

import numpy as np
import pyvisa
import psutil
import subprocess
import sys
from warnings import warn
import time

# For some reason terminal command fails to enable in the network interface
# when the default port (5025) is used.
_PORT = 5555


class VNA:
    """
    Class for communicating with the VNA via the RVNA application.

    Returns
    -------
    None.

    """

    def __init__(self):
        for p in psutil.process_iter():
            if p.name().startswith("RVNA"):
                p.terminate()
                time.sleep(2)

        # TODO: add step about adding the RVNA executable folder to the system path variable
        try:
            subprocess.Popen(["rvna", "EnableSocket:%d" % (_PORT,)])
        except Exception as e:
            print("Error establishing SCPI connection to RVNA: ", e)

        rm = pyvisa.ResourceManager('@py')
        time.sleep(1)

        n_retries = 10
        print("Waiting for SCPI server...")
        while True:
            try:
                self.scpi = rm.open_resource('TCPIP0::127.0.0.1::%d::SOCKET' %
                                             (_PORT,))
                break
            except:
                n_retries -= 1
                if n_retries == 0:
                    print("SCPI Server Not Found")
                    sys.exit()
                time.sleep(1)
        self.scpi.read_termination = '\n'

        # # make the cmt UI not visible
        # self.scpi.write()

        n_retries = 25
        self.scpi.timeout = 10000
        while True:
            try:
                ready = self.scpi.query("SYST:READ?")
                if ready != "1":
                    time.sleep(1)
                    raise Exception
                break
            except:
                n_retries -= 1

                # In the software times out, cancel the program.
                if n_retries == 0:
                    print("Error, timeout waiting for instrument to be ready.")
                    print("Check that VNA is powered on and connected to PC.")
                    print("The status Ready should appear in the lower right "
                          "corner of the VNA application window.")
                    input("\nPress Enter to Exit Program\n")
                    sys.exit()
                else:
                    print("Instrument not ready! Waiting... %d" % (n_retries,),
                          end="\r", flush=True)

            else:
                print("Instrument ready! Continuing...")
                break

        self.scpi.timeout = 20000
        self.vna_name = self.scpi.query("*IDN?").split(",")[1].strip()
        self.vna_sn = self.scpi.query("*IDN?").split(",")[2].strip()
        self.is_standby_on = False
        # Set the trigger to internal to keep the VNA running continuously.
        # This makes the warm-up time shorter.
        self.scpi.write("TRIG:SOUR INT")

        # to hide the CMT window
        # self.scpi.write("SYSTem:HIDE")

    def apply_settings(self, settings):
        """
        Apply the frequency and power settings to the VNA.

        If the supplied settings are incompatible with the VNA, the settings
        are modified in-place to generate suitable values.

        Parameters
        ----------
        settings : settings.Settings
            Object containing the frequency and power parameters to be applied
            to the VNA.

        Returns
        -------
        None.

        """
        # Set start-freq
        min_freq = self.get_min_freq()
        if settings["start_freq"] < min_freq:
            warn("Cannot Set Start-Frequency Below %d Hz" % min_freq)
            settings["start_freq"] = min_freq
        self.scpi.write("SENS1:FREQ:STAR %d" % (settings["start_freq"],))

        # Set stop-freq (max. 14 GHz for R140)
        max_freq = self.get_max_freq()
        if settings["stop_freq"] > max_freq:
            warn("Cannot Set Stop-Frequency Above %f Hz" % (max_freq,))
            settings["stop_freq"] = max_freq
        self.scpi.write("SENS1:FREQ:STOP %f" % (settings["stop_freq"],))

        # Set number of frequency points
        self.scpi.write("SENS1:SWE:POIN %d" % (settings["n_points"],))

        # Set IF bandwidth
        self.scpi.write("SENS1:BWID %d" % (settings["ifbw"],))

        # Set power (level for R140, numeric for R180)
        power = settings["power"]
        if type(power) is str:
            self.scpi.write("SOUR1:POW:STAT %s" % (power,))
        else:
            self.scpi.write("SOUR1:POW %f" % (power,))

    def meas_s11(self):
        """
        Trigger the output signal on the VNA and measure the reflected signal.

        Returns
        -------
        numpy.ndarray
            Array containing the measured vectors at various frequency points.

        """
        # VNA will measure when receiving bus command
        self.scpi.write("TRIG:SOUR BUS")
        time.sleep(0.1)

        # Sending start measurement trigger
        self.scpi.write("TRIG:SING")

        # Waiting for the VNA to finish the sweep
        ready = self.scpi.query("*OPC?")

        tmp = np.fromstring(self.scpi.query("CALC1:DATA:SDAT?"),
                            dtype=np.float64, sep=",")

        return (tmp[0::2] + 1j * tmp[1::2]).astype(complex)

    def get_temperature(self):
        """
        Return the temperature of the VNA.

        Returns
        -------
        float
            Temperature of the VNA hardware.

        """

        try:
            time.sleep(0.2)
            t = float(self.scpi.query("SYSTem:TEMPerature:SENSor? 1"))

        except:
            t = np.NaN

        return t

    def set_standby_on(self):

        if not self.is_standby_on:

            self.scpi.write("SYST:STAN:STAT ON")  # Turn RF OFF
            self.is_standby_on = True
            # print('Stand-by On')

    def set_standby_off(self):
        if self.is_standby_on:

            self.scpi.write("SYST:STAN:STAT OFF")  # Turn RF ON
            if self.vna_name == 'R140':
                # R140 is using an older USB driver that requires a longer waiting time
                time.sleep(1)    # waiting time set according to CMT
            else:
                time.sleep(0.25)    # waiting time set according to CMT

            # print('Stand-by Off')
            self.is_standby_on = False

    def get_min_freq(self):
        """
        Return the minimum compatible frequency for the attached VNA device.

        Returns
        -------
        float
            Minimum possible frequency (in Hz) for the VNA.

        """
        return float(self.scpi.query("SERV:SWE:FREQ:MIN?"))

    def get_max_freq(self):
        """
        Return the maximum compatible frequency for the attached VNA device.

        Returns
        -------
        float
            Maximum possible frequency (in Hz) for the VNA.

        """
        return float(self.scpi.query("SERV:SWE:FREQ:MAX?"))

    def close_vna(self):
        """
        Closes vna application

        Returns
        -------
            None.

        """
        # self.com.SCPI.SYSTem.TERMinate()
        self.scpi.write("SYST:TERM")
