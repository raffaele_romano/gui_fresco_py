# -*- coding: utf-8 -*-
"""
Created on Sun Aug 22 02:40:34 2021.

@author: Nishad Mandlik
"""
import json
import os
import requests
import semver
import shutil
import subprocess
import sys
import tempfile
import time
from copy import deepcopy as dc
from math import ceil, floor
from pathlib import Path

import numpy as np
from PyQt5 import uic
from PyQt5.QtCore import pyqtSignal, Qt, QEvent, QObject, QRect, QThread
from PyQt5.QtGui import QCursor, QIcon
from PyQt5.QtGui import QMovie
from PyQt5.QtWidgets import QMessageBox, QLabel, QDialog, QVBoxLayout
from matplotlib import pyplot as plt
from scipy.signal import savgol_filter

from gui_fresco_py.cust_widgets import CheckBox
from gui_fresco_py.data import Holder
import gui_fresco_py.resources_rc

plt.set_loglevel("critical")

_SCRIPT_DIR = Path(__file__).parent
_LOCAL_APPDATA_DIR = os.getenv('LOCALAPPDATA')

_RSRC_QRC_PATH = _SCRIPT_DIR / Path("../data/System/Design/resources.qrc")
_RSRC_PY_PATH = _SCRIPT_DIR / Path("dresources_rc.py")
_SCANNER_CURRENT_VERSION_FILE_URL = _LOCAL_APPDATA_DIR / Path(
    "Programs/Vertigo/scanner/data/System/Settings/version.json")

_LOADING_BAR_PATH = _SCRIPT_DIR / Path("../data/System/Design/loading-bar.gif")
_SCANNER_VERSION_FILE_URL = "https://surfdrive.surf.nl/files/index.php/s/BmINqg1jgT0wRdV/download"
_SCANNER_SETUP_DESTINATION = _LOCAL_APPDATA_DIR / Path("Temp/scanner_setup.exe")

# Check if the app is running as a script (not as a frozen executable)
if not getattr(sys, 'frozen', False):
    # Only run this in a development environment
    os.system("pyrcc5 " + str(_RSRC_QRC_PATH) + " -o " + str(_RSRC_PY_PATH))



def read_version_file(file_path=_SCANNER_CURRENT_VERSION_FILE_URL):
    """
    Reads version from version file

    Returns
    -------
    Actual version

    """
    try:
        # Read the version info from the JSON file
        with open(file_path, 'r') as f:
            version_info = json.load(f)
            current_version = version_info.get("version", "Unknown")
            # print(f"Current version: {current_version}")
            return current_version
    except FileNotFoundError:
        print(f"{file_path} not found.")
        return '0.0.0'
    except json.JSONDecodeError:
        print(f"Error decoding {file_path}.")
        return None


SCANNER_CURRENT_VERSION = read_version_file(_SCANNER_CURRENT_VERSION_FILE_URL)

_UI_FILE = _SCRIPT_DIR / Path("../data/System/Design/scanner.ui")
form_cls, base_cls = uic.loadUiType(_UI_FILE)

_ATTR_PARAMS = {"Acidity": [False, 0, 5], "Ta": [False, 0, 5], "Brix": [False, 0, 35], "Firmness": [True, 0, 80],
                "Firmness(kg)": [True, 0, 40], "Firmness(lb)": [True, 0, 80],
                "Dry matter": [False, 5, 45], "Oil": [False, 0, 30], "Juiciness": [False, 20, 80]}
_DEMO_FRUIT_DICT = {

    "avocado": {
        "demo firm": {"firmness": {"min_value": 10, "max_value": 30, "unit": 'lbs'},
                      "dry matter": {"min_value": 15, "max_value": 36, "unit": '%'}},
        "demo ripe": {"firmness": {"min_value": 2, "max_value": 10, "unit": 'lbs'},
                      "oil": {"min_value": 5, "max_value": 30, "unit": '%'},
                      "dry matter": {"min_value": 15, "max_value": 36, "unit": '%'}},
    },

    "mango": {
        "demo firm": {"brix": {"min_value": 8, "max_value": 16, "unit": '%'},
                      "firmness": {"min_value": 10, "max_value": 30, "unit": 'lbs'},
                      "dry matter": {"min_value": 8, "max_value": 20, "unit": '%'}},
        "demo ripe": {"brix": {"min_value": 10, "max_value": 20, "unit": '%'},
                      "firmness": {"min_value": 2, "max_value": 10, "unit": 'lbs'},
                      "dry matter": {"min_value": 8, "max_value": 20, "unit": '%'}},
    },

    "pear": {

        "demo": {"brix": {"min_value": 9, "max_value": 15, "unit": '%'},
                 "firmness": {"min_value": 2, "max_value": 8, "unit": 'Kg'}}
    },

    "apple": {

        "demo": {"brix": {"min_value": 10, "max_value": 17, "unit": '%'},
                 "firmness": {"min_value": 3, "max_value": 8, "unit": 'Kg'}}
    }
}


class ScanWorker(QObject):
    """
    Worker object for running scans.

    Parameters
    ----------
    data : data.Holder
        Data holder conatining the VNA interface.

    Returns
    -------
    None.

    """

    finished = pyqtSignal()

    def __init__(self, data):
        super().__init__()
        self.data = data

    def run(self):
        """
        Perform a scan using the VNA, amd emit a "finished" signal when done.

        Returns
        -------
        None.

        """
        self.data.perform_meas()
        self.finished.emit()


class Window(base_cls, form_cls):
    """
    Root widget of the application.

    Returns
    -------
    None.

    """

    def __init__(self):
        super(base_cls, self).__init__()
        self.last_temperature = None
        self.last_batt_perc = None
        self.current_batt_icon_path = None
        self.current_icon_obj_name = None

        self.update_agent = UpdateHandler(self)
        if self.update_agent.check_internet_connection():
            self.update_agent.check_version()

        self.data = Holder()
        print('Initializing UI')
        self.setupUi(self)
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.move(0, 0)
        self.cfg_home()
        self.cfg_vna_settings()
        self.cfg_calib()
        self.cfg_scan_stp_1()
        self.cfg_scan_stp_2()
        self.cfg_scan_stp_3()
        self.cfg_scan_perf()
        self.cfg_scan_done()
        self.cfg_summary()

        # Used for deciding the target page for the 'back' button on the
        # scan_perform page. Values: normal/quick
        self.scan_type = "normal"

        self.raise_()
        self.activateWindow()
        self.show()
        self.setCurrentIndex(0)
        plt.style.use('dark_background')

    def cfg_home(self):
        """
        Configure the items on the home screen.

        Returns
        -------
        None.

        """

        self.btn_vna_settings.clicked.connect(lambda:
                                              self.view("vna_settings"))
        self.btn_calib.clicked.connect(self.view_calib)
        # self.btn_close.clicked.connect(self.close)
        self.btn_close.clicked.connect(self.close_event)
        self.btn_scan.clicked.connect(lambda: self.setup_scan("normal"))
        self.btn_quick.clicked.connect(lambda: self.setup_scan("quick"))

        self.update_temperature_label(self.last_temperature)

        # Create and start the temperature thread
        self.temperature_thread = TemperatureThread(self)
        self.temperature_thread.update_signal.connect(self.update_temperature_label)
        self.temperature_thread.request_temperature.connect(self.get_temperature_from_sensor)
        self.temperature_thread.turn_rf_ON.connect(self.data.v.set_standby_off)
        self.temperature_thread.turn_rf_OFF.connect(self.data.v.set_standby_on)
        self.temperature_thread.start()

        if self.data.g.is_present:
            # Create and start the battery thread
            self.battery_thread = BatteryThread(self)
            # self.battery_thread.update_percentage.connect(self.update_batt_perc_label)
            self.battery_thread.update_percentage.connect(self.update_battery_icon)
            self.battery_thread.request_battery_percentage.connect(self.get_batt_perc_from_sensor)
            # self.battery_thread.request_battery_voltage.connect(self.get_batt_volt_from_sensor)
            self.battery_thread.start()
            # print('battery thread start')
            self.label_batt_perc.setVisible(False)
            # self.update_battery_icon(self.last_batt_perc)
            self.label_batt_icon.setVisible(True)

        else:
            self.label_batt_perc.setVisible(False)
            self.label_batt_icon.setVisible(False)

    def cfg_vna_settings(self):
        """
        Configure the items on the VNA settings screen.

        Returns
        -------
        None.

        """
        min_freq = self.data.vna_min_freq / 1e9
        max_freq = self.data.vna_max_freq / 1e9
        power = self.data.s["power"]

        self.spin_start_freq.setMinimum(min_freq)
        self.spin_start_freq.setMaximum(max_freq)

        self.btn_start_freq_dn.clicked.connect(
            lambda: self.spin_start_freq.setValue(
                ceil((self.spin_start_freq.value() -
                      self.spin_start_freq.singleStep()) /
                     self.spin_start_freq.singleStep()) *
                self.spin_start_freq.singleStep()))

        self.btn_start_freq_up.clicked.connect(
            lambda: self.spin_start_freq.setValue(
                floor((self.spin_start_freq.value() +
                       self.spin_start_freq.singleStep()) /
                      self.spin_start_freq.singleStep()) *
                self.spin_start_freq.singleStep()))

        self.spin_stop_freq.setMinimum(min_freq)
        self.spin_stop_freq.setMaximum(max_freq)

        self.btn_stop_freq_dn.clicked.connect(
            lambda: self.spin_stop_freq.setValue(
                ceil((self.spin_stop_freq.value() -
                      self.spin_stop_freq.singleStep()) /
                     self.spin_stop_freq.singleStep()) *
                self.spin_stop_freq.singleStep()))

        self.btn_stop_freq_up.clicked.connect(
            lambda: self.spin_stop_freq.setValue(
                floor((self.spin_stop_freq.value() +
                       self.spin_stop_freq.singleStep()) /
                      self.spin_stop_freq.singleStep()) *
                self.spin_stop_freq.singleStep()))

        self.btn_npoints_dn.clicked.connect(
            lambda: self.spin_npoints.setValue(self.spin_npoints.value() - 1))

        self.btn_npoints_up.clicked.connect(
            lambda: self.spin_npoints.setValue(self.spin_npoints.value() + 1))

        if type(power) is str:
            self.view("pow_level", self.stack_pow)
            self.btn_pow_level_low.clicked.connect(self.low_pow_level)
            self.btn_pow_level_high.clicked.connect(self.high_pow_level)
        else:
            self.view("pow_numeric", self.stack_pow)
            self.btn_pow_numeric_dn.clicked.connect(
                lambda: self.spin_pow_numeric.setValue(
                    self.spin_pow_numeric.value() -
                    self.spin_pow_numeric.singleStep()))
            self.btn_pow_numeric_up.clicked.connect(
                lambda: self.spin_pow_numeric.setValue(
                    self.spin_pow_numeric.value() +
                    self.spin_pow_numeric.singleStep()))

        self.btn_ifbw_dn.clicked.connect(self.decr_ifbw)
        self.btn_ifbw_up.clicked.connect(self.incr_ifbw)

        self.vna_set_btn_back.clicked.connect(self.cancel_vna_set)
        self.vna_set_btn_next.clicked.connect(self.commit_vna_set)

        self.settings_to_ui()

    def cfg_calib(self):
        """
        Configure the items on the calibration screen.

        Returns
        -------
        None.

        """
        self.btn_calib_back.clicked.connect(lambda: self.view("home"))
        self.btn_calib_reset.clicked.connect(self.calib_reset)
        self.btn_cal_load.clicked.connect(lambda: self.calibrate("load"))
        self.btn_cal_open.clicked.connect(lambda: self.calibrate("open"))
        self.btn_cal_short.clicked.connect(lambda: self.calibrate("short"))

    def cfg_scan_stp_1(self):
        """
        Configure the items on the first scan-setup screen.

        Returns
        -------
        None.

        """
        self.txt_sess_name.installEventFilter(self)
        self.default_sess_name = self.txt_sess_name.text()

        self.combo_fruit.installEventFilter(self)
        fruits = [fruit.capitalize() for fruit in self.data.get_fruit_names()]
        self.combo_fruit.addItems(fruits)
        self.combo_fruit.currentIndexChanged.connect(self.chg_fruit)

        self.combo_breed.installEventFilter(self)
        self.combo_breed.currentIndexChanged.connect(self.chg_breed)
        self.scan_stp_1_btn_back.clicked.connect(lambda:
                                                 self.scan_stp_1_leave("home"))
        self.scan_stp_1_btn_next.clicked.connect(lambda:
                                                 self.scan_stp_1_leave(
                                                     "scan_setup_2"))

    def cfg_scan_stp_2(self):
        """
        Configure the items on the second scan-setup screen.

        Returns
        -------
        None.

        """

        self.scan_stp_2_btn_back.clicked.connect(lambda:
                                                 self.scan_stp_1_enter())
        self.scan_stp_2_btn_next.clicked.connect(self.scan_stp_3_enter)

    def cfg_scan_stp_3(self):
        """
        Configure the items on the third scan-setup screen.

        Returns
        -------
        None.

        """

        # if self.data.g.is_present:
        #     self.update_batt_perc_label(self.last_batt_perc)

        self.scan_stp_3_btn_back.clicked.connect(lambda:
                                                 self.scan_stp_3_leave(
                                                     "scan_setup_2"))
        self.scan_stp_3_btn_next.clicked.connect(lambda:
                                                 self.scan_stp_3_leave(
                                                     "scan_perform"))
        self.spin_nfruits.installEventFilter(self)
        self.spin_nmeas.installEventFilter(self)
        self.btn_nfruits_up.clicked.connect(self.scan_stp_3_nfruits_up_clk)
        self.btn_nfruits_dn.clicked.connect(self.scan_stp_3_nfruits_dn_clk)
        self.btn_nmeas_up.clicked.connect(self.scan_stp_3_nmeas_up_clk)
        self.btn_nmeas_dn.clicked.connect(self.scan_stp_3_nmeas_dn_clk)

    def cfg_scan_perf(self):
        """
        Configure the items on the scan-execution screen.

        Returns
        -------
        None.

        """
        if self.data.g.is_present:
            # self.update_batt_perc_label(self.last_batt_perc)
            # battery_icon = QIcon(str(_BATT_ICON_PATH))
            # self.scan_perf_batt_icon.setPixmap(battery_icon.pixmap(24, 35))  # Adjust the size as needed
            self.scan_perf_batt_perc.setVisible(False)
            self.scan_perf_batt_icon.setVisible(True)
            self.update_battery_icon(self.data.g.last_percentage)

        self.scan_perf_btn_back.clicked.connect(self.scan_perf_back)
        self.scan_perf_btn_stop.clicked.connect(self.stop_meas)
        self.scan_perf_btn_repeat.clicked.connect(self.perform_meas)
        self.scan_perf_btn_next.clicked.connect(self.commit_meas)

    def cfg_scan_done(self):
        """
        Configure the items on the first scan-done screen.

        Returns
        -------
        None.

        """
        self.btn_summary.clicked.connect(lambda: self.view("summary"))
        self.btn_home.clicked.connect(lambda: self.view("home"))

    def cfg_summary(self):
        """
        Configure the items on the first summary screen.

        Returns
        -------
        None.

        """
        self.btn_smry_home.clicked.connect(lambda: self.view("home"))

    def view_calib(self):
        """
        Enter calibration.

        Returns
        -------
        None.

        """
        self.view("calib")
        for line in self.cal_plot.canvas.ax.get_lines():
            line.remove()
        self.cal_plot.canvas.draw()
        self.cal_plot.canvas.flush_events()
        self.style_calib_btns()

    def close_event(self):
        """
        Closes the application window when close button is pressed

        Returns
        -------
        None.

        """

        self.data.v.close_vna()
        self.temperature_thread.terminate()
        if self.data.g.is_present:
            if self.battery_thread.isRunning():
                self.battery_thread.terminate()

        sys.exit(0)

    def calibrate(self, kind):
        """
        Perform calibration with the specified connection.

        Parameters
        ----------
        kind : str
            Type of calibration ("load", "open" or "short").

        Returns
        -------
        None.

        """

        self.data.v.set_standby_off()

        # Perform Calibration
        self.data.calibrate(kind, self.cal_plot.canvas.ax)
        self.cal_plot.canvas.draw()
        self.cal_plot.canvas.flush_events()
        self.style_calib_btns()

        if not self.temperature_thread.isRunning():
            print('temp thread start')
            self.temperature_thread.start()

    def calib_reset(self):
        """
        Reset the calibration.

        Returns
        -------
        None.

        """
        reset = self.question(self.calib, "Reset",
                              "Do you want to reset calibration?")
        if reset == QMessageBox.Yes:
            self.data.reset_calib()
            self.style_calib_btns()

    def style_calib_btns(self):
        """
        Change the style of the calib buttons according to the calib status.

        Returns
        -------
        None.

        """
        style_pending = """
            QPushButton {
                border-color: #ca3e47;
                background: transparent;
                font-size: 28px;
            }

            QPushButton:hover {
                background: '#ca3e47';
            }
            """
        style_done = """
            QPushButton {
                border-color: #69f796;
                background: #69f796;
                font-size: 28px;
                color: black;
            }

            QPushButton:hover {
                background: #69f796;
                color: black;
            }
            """
        status = self.data.get_calib_status("individual")
        if status["load"]:
            self.btn_cal_load.setStyleSheet(style_done)
        else:
            self.btn_cal_load.setStyleSheet(style_pending)
        if status["open"]:
            self.btn_cal_open.setStyleSheet(style_done)
        else:
            self.btn_cal_open.setStyleSheet(style_pending)
        if status["short"]:
            self.btn_cal_short.setStyleSheet(style_done)
        else:
            self.btn_cal_short.setStyleSheet(style_pending)

    def update_temperature_label(self, temperature):

        """
        Updates the temperature label.
        Parameters
        ----------
        temperature : int

        Returns
        -------
        None.

        """

        if temperature is not None:

            temp_label = None

            for child in self.findChildren(QLabel):
                if isinstance(child, QLabel) and "_temp" in child.objectName():
                    # print(child.objectName())
                    temp_label = child

                if temp_label:

                    # Update the label with the new temperature data
                    temperature_str = f"T: {temperature:.1f} °C"

                    temp_label.setText(temperature_str)
                    # Change the label color based on the temperature value and its change rate
                    if 'min_temperature' in self.data.s.__dict__ and temperature < self.data.s.min_temperature:
                        temp_label.setStyleSheet("QLabel { color : red; }")
                    elif self.last_temperature is not None and np.abs(temperature - self.last_temperature) > 0.05:
                        temp_label.setStyleSheet("QLabel { color : yellow; }")
                    else:
                        temp_label.setStyleSheet("QLabel { color : white; }")

                self.last_temperature = temperature

    def get_temperature_from_sensor(self):
        # print('thread reads temp')
        temperature_data = self.data.v.get_temperature()
        self.temperature_thread.read_temperature_from_sensor(temperature_data)

    def get_batt_perc_from_sensor(self):
        self.data.g.last_percentage = ceil(float(self.data.g.read_percentage()))
        # print(f'percentage read : {self.data.g.last_percentage}')
        # print(int(float(self.data.g.read_percentage())))
        self.battery_thread.read_percentage_from_sensor(self.data.g.last_percentage)

    def get_batt_volt_from_sensor(self):

        self.data.g.last_voltage = int(float(self.data.g.read_voltage()))
        print(self.data.g.last_voltage)
        print(f'Estimated perc = {100 * (self.data.g.last_voltage - 3200) / (4200 - 3200)}')

    def update_batt_perc_label(self, percentage):
        label = None
        if percentage is not None:

            for child in self.currentWidget().children():
                if isinstance(child, QLabel) and "batt_perc" in child.objectName():
                    # print(child.objectName())
                    label = child
                    break
            if label:
                # Update the label with the new battery data
                label.setText(f'{int(percentage)} %')

                # Change the label color based on the percentage value
                if 15 <= percentage < 30:
                    label.setStyleSheet("QLabel { color : yellow; }")
                elif percentage < 15:
                    label.setStyleSheet("QLabel { color : red; }")
                else:
                    label.setStyleSheet("QLabel { color : white; }")

            self.last_batt_perc = int(percentage)

    def update_battery_icon(self, percentage):

        # print('update battery icon running')

        icon_label = None
        if percentage is not None:

            for child in self.currentWidget().children():
                if isinstance(child, QLabel) and "batt_icon" in child.objectName():
                    # print(child.objectName())
                    icon_label = child
                    break
            if icon_label:
                new_icon_obj_name = icon_label.objectName()
                new_icon_path = self.get_battery_icon_path(percentage)
                # battery_icon = QIcon(str(new_icon_path))
                # # find the label containing 'batt_icon'
                # icon_label.setPixmap(battery_icon.pixmap(35, 35))
                # self.current_batt_icon_path = new_icon_path

                # Check if the icon needs updating
                if new_icon_path != self.current_batt_icon_path or new_icon_obj_name != self.current_icon_obj_name:
                    battery_icon = QIcon(str(new_icon_path))
                    # find the label containing 'batt_icon'
                    icon_label.setPixmap(battery_icon.pixmap(35, 35))
                    self.current_batt_icon_path = new_icon_path
                    self.current_icon_obj_name = new_icon_obj_name

    def get_battery_icon_path(self, battery_level):

        if battery_level >= 75:
            return _SCRIPT_DIR / Path("../data/System/Design/battery/battery_icon_100.png")
        elif 50 <= battery_level < 75:
            return _SCRIPT_DIR / Path("../data/System/Design/battery/battery_icon_70.png")
        elif 25 <= battery_level < 50:
            return _SCRIPT_DIR / Path("../data/System/Design/battery/battery_icon_50.png")
        elif 10 <= battery_level < 25:
            return _SCRIPT_DIR / Path("../data/System/Design/battery/battery_icon_20.png")
        elif 3 <= battery_level < 10:
            return _SCRIPT_DIR / Path("../data/System/Design/battery/battery_icon_empty.png")
        else:
            return _SCRIPT_DIR / Path("../data/System/Design/battery/battery_icon_off.png")

    def decr_ifbw(self):
        """
        Decrement IFBW value in spin-box.

        Returns
        -------
        None.

        """
        if (self.spin_ifbw.value() % 3) == 0:
            self.spin_ifbw.setValue(self.spin_ifbw.value() / 3)
        else:
            self.spin_ifbw.setValue((self.spin_ifbw.value() // 10) * 3)

    def incr_ifbw(self):
        """
        Increment IFBW value in spin-box.

        Returns
        -------
        None.

        """
        if (self.spin_ifbw.value() % 3) == 0:
            self.spin_ifbw.setValue((self.spin_ifbw.value() // 3) * 10)
        else:
            self.spin_ifbw.setValue(self.spin_ifbw.value() * 3)

    def low_pow_level(self):
        """
        Configure style of power-level buttons for low-power mode.

        Returns
        -------
        None.

        """
        self.btn_pow_level_low.setEnabled(False)
        self.btn_pow_level_high.setEnabled(True)

    def high_pow_level(self):
        """
        Configure style of power-level buttons for high-power mode.

        Returns
        -------
        None.

        """
        self.btn_pow_level_low.setEnabled(True)
        self.btn_pow_level_high.setEnabled(False)

    def cancel_vna_set(self):
        """
        Leave the VNA Settings page. Prompt for discarding changes, if any.

        Returns
        -------
        None.

        """
        # Check if any changes in the settings have occurred
        start_chg = (self.data.s["start_freq"] / 1e9 !=
                     self.spin_start_freq.value())
        stop_chg = (self.data.s["stop_freq"] / 1e9 !=
                    self.spin_stop_freq.value())
        npoints_chg = (self.data.s["n_points"] !=
                       self.spin_npoints.value())
        ifbw_chg = (self.data.s["ifbw"] !=
                    self.spin_ifbw.value())
        pow_chg = False
        if self.stack_pow.currentWidget().objectName() == "pow_numeric":
            pow_chg = (self.data.s["power"] != self.spin_pow_numeric.value())
        elif self.stack_pow.currentWidget().objectName() == "pow_level":
            pow_tmp = self.data.s["power"]
            if ((self.btn_pow_level_high.isEnabled() is False) and
                    (self.btn_pow_level_low.isEnabled() is True)):
                pow_tmp = "HIGH"
            elif ((self.btn_pow_level_high.isEnabled() is True) and
                  (self.btn_pow_level_low.isEnabled() is False)):
                pow_tmp = "LOW"
            pow_chg = (self.data.s["power"] != pow_tmp)

        if start_chg or stop_chg or npoints_chg or ifbw_chg or pow_chg:
            cancel = self.question(self.vna_settings, "Cancel",
                                   "Do you want to discard your changes?")
            if cancel != QMessageBox.Yes:
                return
            else:
                self.settings_to_ui()
        self.view("home")

    def commit_vna_set(self):
        """
        Save the changes to the VNA settings.

        Returns
        -------
        None.

        """
        start_freq = self.spin_start_freq.value() * 1e9
        stop_freq = self.spin_stop_freq.value() * 1e9
        npoints = self.spin_npoints.value()

        if not self.data.validate_freq_calib(start_freq, stop_freq, npoints):
            reset = self.question(self.vna_settings, "Incompatible Settings",
                                  "Frequency settings are incompatible with "
                                  "calibration.\nDo you want to proceed?\n"
                                  "(Calibration will be lost.)")
            if reset != QMessageBox.Yes:
                return

        if self.stack_pow.currentWidget().objectName() == "pow_numeric":
            self.data.s["power"] = self.spin_pow_numeric.value()
        elif self.stack_pow.currentWidget().objectName() == "pow_level":
            if ((self.btn_pow_level_high.isEnabled() is False) and
                    (self.btn_pow_level_low.isEnabled() is True)):
                self.data.s["power"] = "HIGH"
            elif ((self.btn_pow_level_high.isEnabled() is True) and
                  (self.btn_pow_level_low.isEnabled() is False)):
                self.data.s["power"] = "LOW"
        self.data.s["ifbw"] = self.spin_ifbw.value()
        self.data.set_freq(start_freq, stop_freq, npoints)
        self.view("home")

    def settings_to_ui(self):
        """
        Fill in the widgets with values from the current settings.

        Returns
        -------
        None.

        """
        start_freq = (self.data.s["start_freq"] / 1e9)
        stop_freq = (self.data.s["stop_freq"] / 1e9)
        power = self.data.s["power"]
        self.spin_start_freq.setValue(start_freq)
        self.spin_stop_freq.setValue(stop_freq)
        self.spin_npoints.setValue(self.data.s["n_points"])
        self.spin_ifbw.setValue(self.data.s["ifbw"])
        if self.stack_pow.currentWidget().objectName() == "pow_numeric":
            self.spin_pow_numeric.setValue(power)
        elif self.stack_pow.currentWidget().objectName() == "pow_level":
            if power == "LOW":
                self.low_pow_level()
            elif power == "HIGH":
                self.high_pow_level()

    def setup_scan(self, scan_type):
        """
        Initiate scan procedure according to the type.

        Parameters
        ----------
        scan_type : str
            Scan type ("normal" or "quick").

        Returns
        -------
        None.

        """
        self.scan_type = scan_type
        if self.scan_type == "normal":
            self.scan_stp_1_enter(self.default_sess_name)
        elif self.scan_type == "quick":
            self.start_scan()

    def get_session_name(self):
        """
        Return the name of the current measurement session.

        Warning
        -------
        Using this method when a session is not in progress may cause
        unexpected behaviour.

        Returns
        -------
        str
            Session name.

        """
        if self.scan_type == "normal":
            return self.txt_sess_name.text()
        elif self.scan_type == "quick":
            return "QuickScan"

    def start_scan(self):
        """
        Init the measurement object and switch to the scan execution page.

        Returns
        -------
        None.

        """
        self.smry_tab_attrs.clear()
        show_smry = False
        if self.scan_type == "quick":
            self.data.init_scan(self.get_session_name(),
                                self.tab_spec.canvas.ax,
                                scan_info=None, default_n_scans=True)
            self.init_attrs_tiles()

        else:

            fruit_name = self.combo_fruit.currentText()
            breed_name = self.combo_breed.currentText()
            attributes = self.get_curr_attrs()

            if fruit_name == "--Not Selected--":
                scan_info = None
            elif breed_name == "--Not Selected--":
                scan_info = fruit_name
            else:
                scan_info = fruit_name + ", " + breed_name

            self.data.init_scan(self.get_session_name(),
                                self.tab_spec.canvas.ax,
                                scan_info=scan_info, default_n_scans=False, attrs=attributes)
            self.smry_tab_attrs.set_nfruits(self.data.m.n_fruits)
            self.init_attrs_tiles()

            for attr in attributes:
                show_smry = True

                self.smry_tab_attrs.add_attr(attr, _ATTR_PARAMS[attr][1],
                                             _ATTR_PARAMS[attr][2],
                                             _ATTR_PARAMS[attr][0])

        self.btn_summary.setEnabled(show_smry)
        self.btn_summary.setVisible(show_smry)

        self.tab_spec.canvas.draw()
        self.tab_spec.canvas.flush_events()
        self.view("scan_perform")
        self.enable_scan_perf_btns()
        self.scan_perf_status_lab.setText("Connect a fruit and click (🡢)")
        self.update_temperature_label(self.last_temperature)

        if self.data.g.is_present:
            # self.update_batt_perc_label(self.last_batt_perc)
            # self.scan_perf_batt_perc.setText(f'{int(self.last_batt_perc)} %')
            # self.scan_perf_batt_perc.setStyleSheet("QLabel { color : white; }")
            self.update_battery_icon(ceil(self.data.g.last_percentage))
            self.scan_perf_batt_icon.setVisible(True)
            self.scan_perf_batt_perc.setVisible(False)
            if self.battery_thread.isRunning():
                self.battery_thread.terminate()
        else:
            self.scan_perf_batt_icon.setVisible(False)
            self.scan_perf_batt_perc.setVisible(False)

    def disable_scan_perf_btns(self):
        """
        Disable and hide the buttons on the scan-perform page.

        Note
        ----
        The state of the "stop" button is not changed.

        Returns
        -------
        None.

        """
        self.scan_perf_btn_back.setEnabled(False)
        self.scan_perf_btn_repeat.setEnabled(False)
        self.scan_perf_btn_next.setEnabled(False)

    def enable_scan_perf_btns(self):
        """
        Enable and show the buttons on the scan-perform page.

        Note
        ----
        The state of the "stop" button is not changed.

        Returns
        -------
        None.

        """
        self.scan_perf_btn_back.setEnabled(True)
        self.scan_perf_btn_repeat.setEnabled(True)
        self.scan_perf_btn_next.setEnabled(True)

    def commit_meas(self):
        """
        Add the measured values to the measurement object.

        Returns
        -------
        None.

        """
        if self.data.s11_raw is not None:
            self.data.commit_meas()
        if self.data.scan_complete:
            self.data.save_scan()
            attr_idx = 0
            for attr in self.get_curr_attrs():
                # rand_flts = np.random.rand(self.smry_tab_attrs.get_nfruits())
                # min_val = _ATTR_PARAMS[attr][1]
                # max_val = _ATTR_PARAMS[attr][2]
                # rand_ints = ((rand_flts * (max_val-min_val)) +
                #              min_val).astype(int)
                # pred_attrs[fruit_id,meas_id, attr_idx]

                pred_avg = np.mean(self.data.m.pred_attrs, 1)
                pred_vct = pred_avg[:, attr_idx]
                self.smry_tab_attrs.set_vals(attr, pred_vct)
                attr_idx += 1

            self.view("scan_done")
        else:
            self.perform_meas()

    def perform_meas(self):
        """
        Execute a single sweep for measuring s11 values.

        Returns
        -------
        None.

        """

        sample_count = self.data.m.sample_count + 1
        fruit = (sample_count // self.data.m.n_meas) + 1
        meas = (sample_count % self.data.m.n_meas) + 1
        self.scan_perf_status_lab.setText("Fruit %d, Meas %d In Progress" %
                                          (fruit, meas))

        self.disable_scan_perf_btns()

        # Stop temperature Thread
        if self.temperature_thread.isRunning():
            self.temperature_thread.terminate()
            # print('T thread terminated')

        self.data.v.set_standby_off()

        self.scan_thread = QThread()
        self.scan_worker = ScanWorker(self.data)
        self.scan_worker.moveToThread(self.scan_thread)

        # Execute the worker's run function when the thread is started.
        self.scan_thread.started.connect(self.scan_worker.run)

        # Quit the thread when the worker's job is complete
        self.scan_worker.finished.connect(self.scan_thread.quit)

        # Delete objects
        self.scan_worker.finished.connect(self.scan_worker.deleteLater)
        self.scan_thread.finished.connect(self.scan_thread.deleteLater)

        # Actions to be performed after the scan is performed
        self.scan_thread.finished.connect(
            lambda: self.scan_perf_status_lab.setText(
                "Fruit %d, Meas %d Complete" % (fruit, meas)))
        self.scan_thread.finished.connect(self.enable_scan_perf_btns)
        self.scan_thread.finished.connect(self.tab_spec.canvas.draw)
        self.scan_thread.finished.connect(self.tab_spec.canvas.flush_events)
        self.scan_thread.finished.connect(
            lambda:
            self.update_temperature_label(self.data.sensorT))
        # add make prediction here
        self.scan_thread.finished.connect(self.predict_attrs)
        self.scan_thread.finished.connect(
            lambda: self.update_attrs_tiles(self.data.attributes, self.data.pred_attrs))
        self.scan_thread.finished.connect(self.temperature_thread.start)

        self.scan_thread.start()

    def stop_meas(self):
        """
        Stop the current measurement session. Prompt before exit.

        Returns
        -------
        None.

        """
        stop = self.question(self.scan_perform, "Cancel",
                             "Are you sure you want to cancel?")
        if stop != QMessageBox.Yes:
            return
        save = self.question(self.scan_perform, "Save",
                             "Do you want to save the data?",
                             QMessageBox.Yes)
        if save == QMessageBox.Yes:
            self.data.save_scan()
        self.view("home")

    def question(self, parent, title, ques, default=QMessageBox.No):
        """
        Display a boolean-response box and return the answer by the user.

        Parameters
        ----------
        parent : instance of PyQt5.QtWidgets.QWidget, optional
            Parent widget. The default is None.
        title : str
            Title of the dialogue box window.
        ques : str
            Question to be displayed in the dialogue-box.
        default : PyQt5.QtWidgets.QMessageBox.StandardButton, optional
            Default response button. The default is QMessageBox.No.

        Returns
        -------
        PyQt5.QtWidgets.QMessageBox.StandardButton
            Button clicked by the user.

        """
        mb = QMessageBox(parent)
        mb.setWindowTitle(title)
        mb.setText(ques)
        mb.setStyleSheet(
            """
            * {
                color: white;
                font-size: 20px;
                margin: 0;
                padding: 0;
                border: 1px solid white;
            }

            QLabel {
                border: none;
            }

            QPushButton {
                background: white;
                color: black;
                border: none;
                width: 150px;
                padding: 10px 0;
                margin: 10px;
            }

            QPushButton:hover {
                background: #d65a31;
                color: black;
                border: none;
            }
            """
        )
        mb.addButton(QMessageBox.Yes).setCursor(
            QCursor(Qt.PointingHandCursor))
        mb.addButton(QMessageBox.No).setCursor(
            QCursor(Qt.PointingHandCursor))
        mb.setDefaultButton(default)
        return mb.exec()

    def scan_perf_back(self):
        """
        Execute the back operation on the scan execution page.

        Returns
        -------
        None.

        """
        if self.data.scan_started:
            # Move backwards in the measurements
            self.data.s11_raw, self.data.s11, self.data.sensorT, self.data.pred_attrs = self.data.m.get_latest_sample()
            self.data.m.sample_count -= 1
            sample_count = self.data.m.sample_count + 1
            fruit = (sample_count // self.data.m.n_meas) + 1
            meas = (sample_count % self.data.m.n_meas) + 1
            self.scan_perf_status_lab.setText("Fruit %d, Meas %d Complete" %
                                              (fruit, meas))
            for line in self.tab_spec.canvas.ax.get_lines():
                line.remove()
            if self.data.m.n_meas == 1:
                index = self.data.m.sample_count + 1
            else:
                index = (self.data.m.sample_count + 1) % self.data.m.n_meas

            self.tab_spec.canvas.ax.set_prop_cycle(None)
            for i in range(sample_count - index, sample_count):
                _, s11, _, pred_attrs = self.data.m.get_sample(i)
                self.tab_spec.canvas.ax.plot(self.data.s.get_freq_vect(),
                                             abs(s11), linewidth=4)

            self.data._update_ax()
            self.tab_spec.canvas.draw()
            self.tab_spec.canvas.flush_events()
            self.update_temperature_label(self.data.sensorT)
            self.update_attrs_tiles(self.data.attributes, self.data.pred_attrs)
        else:
            # Move to previous page
            if self.scan_type == "normal":
                self.scan_stp_3_enter()
            elif self.scan_type == "quick":
                self.view("home")

    def scan_stp_1_enter(self, sess_name=None):
        """
        Enter the first scan-setup page.

        Parameters
        ----------
        sess_name : str, optional
            Session name. The default is None.

        Returns
        -------
        None.

        """
        self.txt_sess_name.removeEventFilter(self)
        if sess_name is not None:
            self.txt_sess_name.setText(sess_name)
        self.view("scan_setup_1")

        defaults = {}

        geom = self.scan_stp_1_logo.geometry()
        defaults["logo"] = [geom.x(), geom.y(), geom.width(), geom.height()]

        geom = self.lab_sess_name.geometry()
        defaults["lab_sess_name"] = [geom.x(), geom.y(), geom.width(),
                                     geom.height()]

        geom = self.txt_sess_name.geometry()
        defaults["txt_sess_name"] = [geom.x(), geom.y(), geom.width(),
                                     geom.height()]

        geom = self.lab_fruit.geometry()
        defaults["lab_fruit"] = [geom.x(), geom.y(), geom.width(),
                                 geom.height()]

        geom = self.combo_fruit.geometry()
        defaults["combo_fruit"] = [geom.x(), geom.y(), geom.width(),
                                   geom.height()]

        geom = self.lab_breed.geometry()
        defaults["lab_breed"] = [geom.x(), geom.y(), geom.width(),
                                 geom.height()]

        geom = self.combo_breed.geometry()
        defaults["combo_breed"] = [geom.x(), geom.y(), geom.width(),
                                   geom.height()]

        geom = self.scan_stp_1_hLay.geometry()
        defaults["hLay"] = [geom.x(), geom.y(), geom.width(),
                            geom.height()]

        self.scan_stp_1_defaults = defaults
        self.txt_sess_name.installEventFilter(self)

        if self.data.g.is_present:
            # self.update_batt_perc_label(self.data.g.last_percentage)
            self.update_battery_icon(self.data.g.last_percentage)

        # To force the user to manually set focus on the lineEdit element.
        # Otherwise, shift-up does not work for some reason.
        self.scan_stp_1_logo.setFocus()

    def scan_stp_1_leave(self, next_page):
        """
        Leave the first scan-setup page and switch to the specified page.

        Parameters
        ----------
        next_page : str
            Name of the page to be viewed next.

        Returns
        -------
        None.

        """
        self.scan_stp_1_shift_down()
        self.view(next_page)

    def scan_stp_1_shift_up(self):
        """
        Configure the 1st scan-setup page to make space for the touch-keyboard.

        Returns
        -------
        None.

        """
        geom = dc(self.scan_stp_1_defaults["logo"])
        w_new = 0.4 * geom[2]
        h_new = 0.4 * geom[3]
        w_diff = geom[2] - w_new
        self.scan_stp_1_logo.setGeometry(geom[0] + w_diff / 2, geom[1],
                                         w_new, h_new)
        y_new = geom[1] + h_new + 15

        geom = dc(self.scan_stp_1_defaults["lab_sess_name"])
        y_diff = geom[1] - y_new
        geom[1] = y_new
        self.lab_sess_name.setGeometry(*geom)

        geom = dc(self.scan_stp_1_defaults["txt_sess_name"])
        geom[1] -= y_diff
        self.txt_sess_name.setGeometry(*geom)
        y_new = geom[1] + geom[3] + 15

        geom = dc(self.scan_stp_1_defaults["lab_fruit"])
        y_diff = geom[1] - y_new
        geom[1] = y_new
        self.lab_fruit.setGeometry(*geom)

        geom = dc(self.scan_stp_1_defaults["combo_fruit"])
        geom[1] -= y_diff
        self.combo_fruit.setGeometry(*geom)
        y_new = geom[1] + geom[3] + 15

        geom = dc(self.scan_stp_1_defaults["lab_breed"])
        y_diff = geom[1] - y_new
        geom[1] = y_new
        self.lab_breed.setGeometry(*geom)

        geom = dc(self.scan_stp_1_defaults["combo_breed"])
        geom[1] -= y_diff
        self.combo_breed.setGeometry(*geom)
        y_new = geom[1] + geom[3] + 15

        geom = dc(self.scan_stp_1_defaults["hLay"])
        geom[1] = y_new
        self.scan_stp_1_hLay.setGeometry(QRect(*geom))

    def scan_stp_1_shift_down(self):
        """
        Configure the first scan-setup page to occupy the entire screen.

        Returns
        -------
        None.

        """
        geom = self.scan_stp_1_defaults["logo"]
        self.scan_stp_1_logo.setGeometry(*geom)

        geom = self.scan_stp_1_defaults["lab_sess_name"]
        self.lab_sess_name.setGeometry(*geom)

        geom = self.scan_stp_1_defaults["txt_sess_name"]
        self.txt_sess_name.setGeometry(*geom)

        geom = self.scan_stp_1_defaults["lab_fruit"]
        self.lab_fruit.setGeometry(*geom)

        geom = self.scan_stp_1_defaults["combo_fruit"]
        self.combo_fruit.setGeometry(*geom)

        geom = self.scan_stp_1_defaults["lab_breed"]
        self.lab_breed.setGeometry(*geom)

        geom = self.scan_stp_1_defaults["combo_breed"]
        self.combo_breed.setGeometry(*geom)

        geom = self.scan_stp_1_defaults["hLay"]
        self.scan_stp_1_hLay.setGeometry(QRect(*geom))

    def get_curr_fruit(self):
        """
        Get the name of the currently chosen fruit.

        Returns
        -------
        str
            Name of the fruit chosen by the user.

        """
        return self.combo_fruit.currentText()

    def chg_fruit(self):
        """
        Reconfigure the combo-box for breeds, according to the selected fruit.

        Returns
        -------
        None.

        """
        breeds = self.data.get_breed_names(self.get_curr_fruit())
        item_0 = self.combo_breed.itemText(0)
        self.combo_breed.clear()
        self.combo_breed.addItem(item_0)
        self.combo_breed.addItems(breeds)
        self.combo_breed.setCurrentIndex(0)

    def get_curr_breed(self):
        """
        Get the name of the currently chosen breed.

        Returns
        -------
        str
            Name of the breed chosen by the user.

        """
        return self.combo_breed.currentText()

    def chg_breed(self):
        """
        Reconfigure the combo-box for attributes, according to the selected fruit and breed.

        Returns
        -------
        None.

        """
        attrs = self.data.get_attr_names(self.get_curr_fruit(),
                                         self.get_curr_breed())

        while self.scan_stp_2_vLay_inner.count() > 0:
            self.scan_stp_2_vLay_inner.takeAt(0).widget().deleteLater()
        if len(attrs) == 0:
            self.lab_attrs.setText("No Attributes")
            return
        self.lab_attrs.setText("Attributes")
        for attr in attrs:
            cb = CheckBox(attr, self.scan_setup_2)
            self.scan_stp_2_vLay_inner.addWidget(cb)

    def get_curr_attrs(self):
        """
        Get the name of the currently chosen attributes.

        Returns
        -------
        str
            Name of the attributes chosen by the user.

        """
        attrs = []
        for i in range(0, self.scan_stp_2_vLay_inner.count()):
            widget = self.scan_stp_2_vLay_inner.itemAt(i).widget()
            if widget.checkState():
                attrs.append(widget.text())
        return attrs

    def scan_stp_3_enter(self):
        """
        Enter the third scan-setup page.

        Returns
        -------
        None.

        """
        self.view("scan_setup_3")

        defaults = {}

        geom = self.scan_stp_3_logo.geometry()
        defaults["logo"] = [geom.x(), geom.y(), geom.width(), geom.height()]

        geom = self.lab_nfruits.geometry()
        defaults["lab_nfruits"] = [geom.x(), geom.y(), geom.width(),
                                   geom.height()]

        geom = self.scan_stp_3_hLay_0.geometry()
        defaults["hLay_0"] = [geom.x(), geom.y(), geom.width(), geom.height()]

        geom = self.lab_nmeas.geometry()
        defaults["lab_nmeas"] = [geom.x(), geom.y(), geom.width(),
                                 geom.height()]

        geom = self.scan_stp_3_hLay_1.geometry()
        defaults["hLay_1"] = [geom.x(), geom.y(), geom.width(), geom.height()]

        geom = self.scan_stp_3_hLay_2.geometry()
        defaults["hLay_2"] = [geom.x(), geom.y(), geom.width(), geom.height()]

        self.scan_stp_3_defaults = defaults

    def scan_stp_3_leave(self, next_page):
        """
        Leave the third scan-setup page and switch to the specified page.

        Parameters
        ----------
        next_page : str
            Name of the page to be viewed next.

        Returns
        -------
        None.

        """
        self.scan_stp_3_shift_down()
        if next_page == "scan_perform":
            self.data.set_n_scans(self.spin_nfruits.value(),
                                  self.spin_nmeas.value())
            self.start_scan()
            return
        self.view(next_page)

    def scan_stp_3_shift_up(self):
        """
        Configure the 3rd scan-setup page to make space for the touch-keyboard.

        Returns
        -------
        None.

        """
        geom = dc(self.scan_stp_3_defaults["logo"])
        w_new = 0.6 * geom[2]
        h_new = 0.6 * geom[3]
        w_diff = geom[2] - w_new
        self.scan_stp_3_logo.setGeometry(geom[0] + w_diff / 2, geom[1],
                                         w_new, h_new)
        y_new = geom[1] + h_new + 25

        geom = dc(self.scan_stp_3_defaults["lab_nfruits"])
        y_diff = geom[1] - y_new
        geom[1] = y_new
        self.lab_nfruits.setGeometry(*geom)

        geom = dc(self.scan_stp_3_defaults["hLay_0"])
        geom[1] -= y_diff
        self.scan_stp_3_hLay_0.setGeometry(QRect(*geom))
        y_new = geom[1] + geom[3] + 25

        geom = dc(self.scan_stp_3_defaults["lab_nmeas"])
        y_diff = geom[1] - y_new
        geom[1] = y_new
        self.lab_nmeas.setGeometry(*geom)

        geom = dc(self.scan_stp_3_defaults["hLay_1"])
        geom[1] -= y_diff
        self.scan_stp_3_hLay_1.setGeometry(QRect(*geom))
        y_new = geom[1] + geom[3] + 25

        geom = dc(self.scan_stp_3_defaults["hLay_2"])
        geom[1] = y_new
        self.scan_stp_3_hLay_2.setGeometry(QRect(*geom))

    def scan_stp_3_shift_down(self):
        """
        Configure the 3rd scan-setup page to occupy the entire screen.

        Returns
        -------
        None.

        """
        geom = self.scan_stp_3_defaults["logo"]
        self.scan_stp_3_logo.setGeometry(*geom)

        geom = self.scan_stp_3_defaults["lab_nfruits"]
        self.lab_nfruits.setGeometry(*geom)

        geom = self.scan_stp_3_defaults["hLay_0"]
        self.scan_stp_3_hLay_0.setGeometry(QRect(*geom))

        geom = self.scan_stp_3_defaults["lab_nmeas"]
        self.lab_nmeas.setGeometry(*geom)

        geom = self.scan_stp_3_defaults["hLay_1"]
        self.scan_stp_3_hLay_1.setGeometry(QRect(*geom))

        geom = self.scan_stp_3_defaults["hLay_2"]
        self.scan_stp_3_hLay_2.setGeometry(QRect(*geom))

    def scan_stp_3_nfruits_up_clk(self):
        """
        Increment the number of fruits in the spin-box.

        Returns
        -------
        None.

        """
        self.scan_stp_3_shift_down()
        self.spin_nfruits.setValue(self.spin_nfruits.value() + 1)

    def scan_stp_3_nfruits_dn_clk(self):
        """
        Decrement the number of fruits in the spin-box.

        Returns
        -------
        None.

        """
        self.scan_stp_3_shift_down()
        self.spin_nfruits.setValue(self.spin_nfruits.value() - 1)

    def scan_stp_3_nmeas_up_clk(self):
        """
        Increment the number of measurements per fruits in the spin-box.

        Returns
        -------
        None.

        """
        self.scan_stp_3_shift_down()
        self.spin_nmeas.setValue(self.spin_nmeas.value() + 1)

    def scan_stp_3_nmeas_dn_clk(self):
        """
        Decrement the number of measurements per fruits in the spin-box.

        Returns
        -------
        None.

        """
        self.scan_stp_3_shift_down()
        self.spin_nmeas.setValue(self.spin_nmeas.value() - 1)

    def event_filter(self, obj, evt):
        """
        Handle focus events to predict display of touch-keyboard.

        Parameters
        ----------
        obj : instance of PyQt5.QtWidgets.QWidget.
            DESCRIPTION.
        evt : TYPE
            DESCRIPTION.

        Returns
        -------
        bool
            Output of :py:func:`PyQt5.QtWidgets.QWidget.eventFilter` .

        """
        if ((obj.objectName() == "spin_nfruits") or
                (obj.objectName() == "spin_nmeas")):
            if evt.type() == QEvent.FocusIn:
                self.scan_stp_3_shift_up()
        elif obj.objectName() == "txt_sess_name":
            if evt.type() == QEvent.FocusIn:
                self.scan_stp_1_shift_up()
        elif ((obj.objectName() == "combo_breed") or
              (obj.objectName() == "combo_fruit")):
            if evt.type() == QEvent.FocusIn:
                self.scan_stp_1_shift_down()
            # elif (evt.type() == QEvent.FocusOut):
            #     self.scan_stp_1_shift_down()
        return super().event_filter(obj, evt)

    def view(self, widget_name, parent=None):
        """
        Switch the view to the specified page.

        Parameters
        ----------
        widget_name : str
            Name of the page.
        parent : instance of PyQt5.QtWidgets.QWidget, optional
            Parent wigdet whose child widget needs to be switched. If None,
            the root widget is chosen as the parent. The default is None.

        Returns
        -------
        None.

        """
        if parent is None:
            parent = self
        widget_id = parent.indexOf(getattr(self, widget_name))
        if widget_id < 2:
            # self.update_temperature_label(self.last_temperature)
            # Start the temperature thread if not already running

            if not self.temperature_thread.isRunning():
                self.temperature_thread.start()
            if self.data.g.is_present and not self.battery_thread.isRunning():
                self.battery_thread.start()
                # print('battery thread start')

        # else:
        # # Stop the temperature thread if running
        # if self.temperature_thread.isRunning():
        #     self.temperature_thread.terminate()
        #     print('T thread terminated')
        parent.setCurrentIndex(widget_id)

    def init_attrs_tiles(self):

        for idx in range(6):  # 6 is the maximum number of attributes
            exec('self.label_attr' + str(idx + 1) + '.setHidden(True)')

        if self.scan_type == 'normal':

            # make tiles not visible
            attrs = self.get_curr_attrs()

            # activate what is selected
            for idx in range(len(attrs)):
                exec('self.label_attr' + str(idx + 1) + '.setHidden(False)')
                label_str = attrs[idx] + '\n\n' + '--'
                exec('self.label_attr' + str(idx + 1) + '.setText(label_str)')

    def demo_update_attrs_tiles(self):
        """
        Demo function


        Makes predictions and updates tiles with available attributes.

        Returns
        -------
        None.

        """
        print('update tiles running')
        s11_raw = self.data.s11_raw
        s11 = self.data.s11

        # calculate predicted values
        attrs = self.get_curr_attrs()

        # update tiles
        # create list with random values to be picked

        acidity_list = [1.1, 2, 2.3, 3.5, 4]
        brix_list = [12.5, 14, 16, 18, 20]
        juiciness_list = [25, 32.5, 36, 39, 41]
        firmness_list = [0.5, 1, 1.5, 5, 7]
        drymatter_list = [18, 20, 23, 27, 33]

        import random

        # activate what is selectedadd
        for idx in range(len(attrs)):

            if attrs[idx] == 'Acidity':
                predicted_attr = str(random.choice(acidity_list)) + ' %'
            elif attrs[idx] == 'Brix':
                predicted_attr = str(random.choice(brix_list)) + ' %'
            elif attrs[idx] == 'Firmness':
                predicted_attr = str(random.choice(firmness_list)) + ' kg'
            elif attrs[idx] == 'Dry matter':
                predicted_attr = str(random.choice(drymatter_list)) + ' %'
            elif attrs[idx] == 'Juiciness':
                predicted_attr = str(random.choice(juiciness_list)) + ' %'
            lbl_str = attrs[idx] + "\n\n" + predicted_attr

            exec('self.label_attr' + str(idx + 1) + '.setText(lbl_str)')

    def predict_attrs(self):
        """

        Makes attributes prediction for the single s11raw and s11

        Returns
        -------
        None.

        """
        if self.data.scan_info is not None:

            ## get scan info
            # fruit, breed = self.data.scan_info.split(',')

            # get attributes
            attrs = self.get_curr_attrs()

            predicted_attrs = []

            for attr in attrs:
                predicted_attrs.append(
                    self.predict_attr(attr, self.data.m.freq, self.data.s11_raw, self.data.s11, self.data.sensorT,
                                      self.data.models_dict))

            self.data.pred_attrs = predicted_attrs
            self.update_attrs_tiles(attrs, predicted_attrs)

    def predict_attr(self, attribute, freq, s11_raw, s11, sensorT, models_dict=None):

        """

        Calculates the model prediction for a single attribute. If a demo model is used the prediction is randomly generated.

        Parameters
        ----------
        attribute : str
            Name of the attribute.

        s11_raw : ndarray
            Last measured s11 raw as a complex numbers array

        s11: ndarray
            Last measured s11 as a complex number array

        sensorT: float
            Sensor temperature for the measurement

        Returns
        -------
        predicted attrbute : float
            The predicted attribute


        """

        attrs_dict = {
            'acidity': 'acidity',
            'firmness': 'firmness',
            'dry matter': 'drymatter',
            'brix': 'brix',
            'juiciness': 'juiciness'
        }

        try:

            y = np.empty([], dtype=float)

            fruit, breed_patch = self.data.scan_info.split(',')
            breed_patch = breed_patch.lstrip()

            observation = attribute.lower()

            # demo code begins
            if 'demo' in breed_patch.lower():

                import random

                y = random.uniform(_DEMO_FRUIT_DICT[fruit.lower()][breed_patch.lower()][observation]['min_value'],
                                   _DEMO_FRUIT_DICT[fruit.lower()][breed_patch.lower()][observation]['max_value'])

            else:

                # breed_patch = breed_patch.replace(" ", "_")
                # breed, probe = breed_patch.split("_", 1)
                # model_folder_name = breed_patch
                # input_var = 's11'

                # # # temporary load a measurement file
                # #
                # # fn = "D:\\Raffaele\\Vertigo\\git\\GUI_fresco_py\\User\\Measurements\\Test_annurca_20230301_171504.pickle"
                # # fn = 'D:\\Raffaele\\Surfdrive\\Vertigo\\Fresco\\Experiments\\EZ\\20220926\\meas\\EZ_20220926_3493_20220812_023631.pickle'
                # fn = 'D:\\Raffaele\\Surfdrive\\Vertigo\\Fresco\\Experiments\\Model_verification_20230302\\Model_eval_20230306_mango_20230306_140815.pickle'
                # fn = 'D:\\Raffaele\\Surfdrive\\Vertigo\\Fresco\\Experiments\\NP_202210\\mango\\meas\\NP_20221011_A_20221011_142912.pickle'
                # fn = 'D:\\Raffaele\\Surfdrive\\Vertigo\\Fresco\\Experiments\\Model_verification_20230302\\Model_eval_20230303_tomato_20230303_135246.pickle'
                # fn = 'D:\\Raffaele\\Surfdrive\\Vertigo\\Fresco\\Experiments\\NP_pilot\\avo\\week_30\\230726virus285gr5br_20230319_045002.pickle'
                # fn = 'D:\\Raffaele\\Surfdrive\\Vertigo\\Fresco\\Experiments\\NP_pilot\\avo\\DM_test\\z231013io56083mt14_20230320_130227.pickle'
                # fn = 'D:\\Raffaele\\Surfdrive\\Vertigo\\Fresco\\Experiments\\Liquid calibration\\Test_REF_mango_models\\meas\\20240411_mango_20240411_120114.pickle'

                # fn = 'D:\\Raffaele\\Surfdrive\\Vertigo\\Fresco\\Experiments\\Liquid calibration\\Test_recal\\20240509_mango\\20240509_mango_m04_20240509_112130.pickle'
                # tmp_m = joblib.load(fn)
                # # #
                # s11 = tmp_m.s11[1, 0, :]
                #
                # # ###

                model_dict = self.data.models_dict[observation]
                model_str = model_dict['model_str']

                if model_str == 'PLS':

                    model = model_dict['model']
                    model_params = model_dict['model_param']

                    components = model_params[0]
                    sorted_ind = model_params[1]
                    sorted_feat = model_params[3]
                    wav = model_params[2]

                    freq0 = freq

                    if 'param' in model_dict and model_dict['param'] == 's11raw':
                        s11 = s11_raw

                    if 'remove0to2' in model_dict:
                        if model_dict['remove0to2']:
                            s11 = s11[np.where(freq == 2.0e9)[0][0] + 1:]
                            freq = freq0[np.where(freq0 == 2.0e9)[0][0] + 1:]

                    if 'remove8to10' in model_dict:
                        if model_dict['remove8to10']:
                            s11 = np.concatenate(
                                [s11[0:np.where(freq == 8.0e9)[0][0]], s11[np.where(freq == 10.25e9)[0][0]:]])
                            freq = np.concatenate(
                                [freq[0:np.where(freq == 8.0e9)[0][0]], s11[np.where(freq == 10.25e9)[0][0]:]])

                    if 'remove8to14' in model_dict:
                        if model_dict['remove8to14']:
                            s11 = s11[0:np.where(freq == 8.0e9)[0][0]]
                            freq = freq[0:np.where(freq == 8.0e9)[0][0]]
                            model_dict['remove_phase12to14'] = False

                    x_real = np.real(s11)
                    x_imag = np.imag(s11)

                    x = np.concatenate([x_real, x_imag])

                    # if model_dict['add_sensorT'] and sorted_ind[np.where(sorted_feat == 'sensorT')[0][0]] == 112:  # need to check where sensorT is in the training data
                    #     # in the training data sensorT might be after real and imag or after mag and phase
                    #     # mostly present after mag and phase
                    #     x = np.append(x, self.data.sensorT)

                    if model_dict['add_mag_phase']:

                        x_mag = np.abs(s11)

                        if len(sorted_feat) > 0:
                            if '2.25_GHz_unw_phase' in sorted_feat:
                                x_phase = np.unwrap(np.angle(s11))
                            else:
                                x_phase = np.angle(s11)
                        else:
                            x_phase = np.unwrap(np.angle(s11))

                        x = np.concatenate([x, x_mag, x_phase])

                        if 'remove_phase12to14' in model_dict and model_dict['remove_phase12to14']:
                            x = np.delete(x, np.s_[-9:])
                            x_phase = np.delete(x_phase, np.s_[-9:])
                            print('12 to 14 GHz phase removed')

                    if model_dict['add_sensorT'] and sorted_ind[np.where(sorted_feat == 'sensorT')[0][
                        0]] > 200:  # need to check where sensorT is in the training data
                        # in the training data sensorT might be after real and imag or after mag and phase
                        # mostly present after mag and phase
                        x = np.append(x, self.data.sensorT)

                    if model_dict['add_derivatives']:

                        if model_dict['add_mag_phase']:
                            x = np.concatenate([x,
                                                savgol_filter(x_real, 5, polyorder=2, deriv=1),
                                                savgol_filter(x_imag, 5, polyorder=2, deriv=1),
                                                savgol_filter(x_mag, 5, polyorder=2, deriv=1),
                                                savgol_filter(x_phase, 5, polyorder=2, deriv=1),
                                                savgol_filter(x_real, 5, polyorder=2, deriv=2),
                                                savgol_filter(x_imag, 5, polyorder=2, deriv=2),
                                                savgol_filter(x_mag, 5, polyorder=2, deriv=2),
                                                savgol_filter(x_phase, 5, polyorder=2, deriv=2)
                                                ])
                        else:
                            # der for R I
                            x = np.concatenate([x,
                                                savgol_filter(x_real, 5, polyorder=2, deriv=1),
                                                savgol_filter(x_imag, 5, polyorder=2, deriv=1),
                                                savgol_filter(x_real, 5, polyorder=2, deriv=2),
                                                savgol_filter(x_imag, 5, polyorder=2, deriv=2),
                                                ])

                    if len(sorted_feat) > 0:  # include the pre-processing steps
                        x_sorted = x[sorted_ind]  # sorting wavelength
                        x_selected = x_sorted[wav:]  # wavelength are removed
                    else:
                        x_selected = x

                    print('X processed')

                    y = float(model.predict(x_selected.reshape(1, len(x_selected))))
                    y = round(y, 2)

                # elif model_str == 'DNN':
                #
                #     # when DNN is used the model is saved + the scaler (after 2022-10 derivatives are also added)
                #     # from tensorflow.keras.models import load_model


        except Exception as e:

            # Log the error if loading fails
            QMessageBox.critical(None, 'Error', f"An error occurred while predicting {observation}\n")

            y = 0
            return y

        print('Raw predicted %s : %.2f' % (observation, y))

        y = max(_ATTR_PARAMS[observation.capitalize()][1], y)
        y = min(_ATTR_PARAMS[observation.capitalize()][2], y)

        # print('predicted ' + observation + ' = ' + str(y))
        print('predicted %s : %.2f' % (observation, y))
        return y

    def update_attrs_tiles(self, attrs, predicted_attrs):
        """

        Updates tiles with predicted attributes.

        Returns
        -------
        None.

        """
        # print('new update tiles running')
        # # s11_raw = self.data.s11_raw
        # # s11 = self.data.s11
        #
        # # calculate predicted values
        # attrs = self.data.attributes commented on 2022-02-10
        # predicted_attrs = self.data.pred_attrs
        # update tiles
        if attrs:
            try:
                fruit, breed_patch = self.data.scan_info.split(',')
                if attrs is not None:

                    for idx in range(len(attrs)):

                        if attrs[idx] == 'Acidity':
                            unit_str = ' %'
                        elif attrs[idx] == 'Brix':
                            unit_str = ' %'
                        elif attrs[idx] == 'Firmness':
                            if fruit in ['mango', 'avocado']:
                                unit_str = ' lbs'
                            else:
                                unit_str = 'kg'
                        elif attrs[idx] == 'Dry matter':
                            unit_str = ' %'
                        elif attrs[idx] == 'Juiciness':
                            unit_str = ' %'
                        else:
                            unit_str = ''

                        # lbl_str = attrs[idx] + "\n\n" + str(predicted_attrs[idx]) + unit_str
                        lbl_str = '%s \n\n %.2f %s' % (attrs[idx], predicted_attrs[idx], unit_str)
                        exec('self.label_attr' + str(idx + 1) + '.setText(lbl_str)')

            except:
                print('An error occurred in update_attrs_tiles')


class TemperatureThread(QThread):
    update_signal = pyqtSignal(float)
    request_temperature = pyqtSignal()
    turn_rf_ON = pyqtSignal()
    turn_rf_OFF = pyqtSignal()

    def __init__(self, parent=None):
        super(TemperatureThread, self).__init__(parent)
        self.target_temperature = parent.data.s.target_temperature  # Target VNA temperature
        self.tolerance = parent.data.s.temperature_tolerance  # Tolerance for temperature control
        # self.rf_on = True  # Flag to track RF power state

    def run(self):
        while True:
            self.request_temperature.emit()  # Request temperature from the main window
            time.sleep(5)

    def read_temperature_from_sensor(self, temperature):
        self.update_signal.emit(temperature)
        # print(f'Temperature reading: {temperature}')
        # Temperature control logic
        if temperature > (self.target_temperature + self.tolerance):
            self.turn_rf_OFF.emit()
            # self.parent().data.v.scpi.write("SYST:STAN:STAT ON")  # Turn RF OFF
            # self.rf_on = False
            # print("VNA Temperature above threshold, RF OFF")
        elif temperature < (self.target_temperature - self.tolerance):
            self.turn_rf_ON.emit()
            # self.parent().data.v.scpi.write("SYST:STAN:STAT OFF")  # Turn RF ON
            # self.rf_on = True
            # print("VNA Temperature below threshold, RF ON")
        # else:
        #     self.turn_rf_OFF.emit()


class BatteryThread(QThread):
    update_voltage = pyqtSignal(float)
    update_percentage = pyqtSignal(float)
    request_battery_percentage = pyqtSignal()
    request_battery_voltage = pyqtSignal()

    def __init__(self, parent=None):
        super(BatteryThread, self).__init__(parent)

    def run(self):
        while True:
            self.request_battery_percentage.emit()  # Request battery percentage
            # print('battery perc request')
            # self.request_battery_voltage.emit() # Not reading voltage at the moment
            time.sleep(300)  # read the battery once in 3 minutes

    def read_percentage_from_sensor(self, percentage):
        self.update_percentage.emit(percentage)

    def read_voltage_from_sensor(self, voltage):
        self.update_voltage.emit(voltage)


class DownloadThread(QThread):
    download_complete = pyqtSignal()
    download_progress = pyqtSignal(int, int)  # Signal to emit progress (downloaded, total)

    def __init__(self, url, destination):
        super().__init__()
        self.url = url
        self.destination = destination

    def run(self):
        response = requests.get(self.url, stream=True)
        total_size = int(response.headers.get('content-length', 0))
        downloaded_size = 0

        with open(self.destination, 'wb') as f:
            for chunk in response.iter_content(chunk_size=8192):
                if chunk:
                    f.write(chunk)
                    downloaded_size += len(chunk)
                    self.download_progress.emit(downloaded_size, total_size)

        self.download_complete.emit()


class LoadingDialog(QDialog):
    def __init__(self, parent):
        super().__init__()
        self.setWindowFlags(Qt.FramelessWindowHint)
        # self.setWindowTitle("Downloading scanner app udpate...")
        self.setFixedSize(150, 60)
        self.setModal(True)

        layout = QVBoxLayout()

        # Loading GIF
        self.loading_label = QLabel()
        self.loading_label.setAlignment(Qt.AlignCenter)
        self.movie = QMovie(str(_LOADING_BAR_PATH))
        self.loading_label.setMovie(self.movie)
        layout.addWidget(self.loading_label)

        # Progress Label
        self.progress_label = QLabel("0MB / 0MB", self)
        self.progress_label.setAlignment(Qt.AlignCenter)
        layout.addWidget(self.progress_label)

        self.setLayout(layout)
        self.movie.start()

    def update_progress(self, downloaded, total):
        downloaded_mb = downloaded / (1024 * 1024)
        total_mb = total / (1024 * 1024)
        self.progress_label.setText(f"{downloaded_mb:.2f}MB / {total_mb:.2f}MB")


class UpdateHandler:

    def __init__(self, main_window):
        self.main_window = main_window
        self.download_url = None
        self.target_app = 'scanner'
        self.temp_dir = tempfile.TemporaryDirectory()

        if _SCANNER_CURRENT_VERSION_FILE_URL.is_file():
            with _SCANNER_CURRENT_VERSION_FILE_URL.open("r") as f:
                tmp = json.load(f)
                self.scanner_current_version = tmp['version']

    def check_version(self):
        try:

            version_file_url = _SCANNER_VERSION_FILE_URL
            current_version = SCANNER_CURRENT_VERSION

            response = requests.get(version_file_url)
            response.raise_for_status()  # Raise an exception for HTTP errors

            # Check if the response is empty
            if not response.content:
                print("Error: The response is empty.")
            else:
                # Attempt to parse the JSON content
                version_data = response.json()

                # Find the latest version
                latest_entry = max(version_data, key=lambda x: semver.VersionInfo.parse(x["version"]))
                latest_version = latest_entry["version"]
                self.download_url = latest_entry["url"]

                print(f"Current version: {current_version}")
                print(f"Latest version: {latest_version}")

                if semver.compare(current_version, latest_version) < 0:
                    reply = QMessageBox.question(
                        None,
                        "Update Available",
                        f"A new version {latest_version} is available. Do you want to download it?",
                        QMessageBox.Yes | QMessageBox.No,
                        QMessageBox.No
                    )

                    if reply == QMessageBox.Yes:
                        # self.destination_path = os.path.join(self.temp_dir.name,
                        #                                      f'setup_{self.target_app}_' + latest_version.replace('.',
                        #                                         '_') + '.exe')
                        self.get_file()
                else:
                    print("You already have the latest version.")

        except requests.exceptions.RequestException as e:
            print(f"HTTP Request failed: {e}")

        except json.JSONDecodeError as e:
            print(f"JSON decoding failed: {e}")
            print(
                f"Response content: {response.text[:500]}")  # Print the first 500 characters of the response for inspection

    def get_file(self):

        # self.log_edit.append("Download Status: Downloading...")

        # Open the loading dialog
        self.loading_dialog = LoadingDialog(self.main_window)
        self.loading_dialog.show()

        # url = "https://example.com/yourfile.zip"  # Replace with your file URL
        # destination = "yourfile.zip"  # Replace with your desired download path

        self.download_thread = DownloadThread(self.download_url, _SCANNER_SETUP_DESTINATION)
        self.download_thread.download_progress.connect(self.loading_dialog.update_progress)
        self.download_thread.download_complete.connect(self.on_download_complete)
        self.download_thread.start()
        # self.download_thread.wait()  # Wait for the download to complete

    @staticmethod
    def check_internet_connection():
        # try:
        #     subprocess.check_output(["ping", "-c", "1", "8.8.8.8"])
        #     return True
        # except subprocess.CalledProcessError:
        #     return False
        # def is_connected():
        try:
            # Try to reach a reliable website (e.g., Google)
            response = requests.get("https://www.google.com", timeout=5)
            # If the request is successful, return True
            return True if response.status_code == 200 else False
        except requests.ConnectionError:
            # If a connection error occurs, return False
            return False

    def on_download_complete(self):
        # self.log_edit.append("Download Status: Complete")
        self.loading_dialog.movie.stop()
        self.loading_dialog.accept()  # Close the loading dialog
        self.prompt_to_run_file()

    def update_scanner_app(self):
        self.target_app = 'scanner'

        if self.check_internet_connection():
            print("Connected to internet.")
            # self.log_edit.append("Connected to internet.")
            self.check_version('scanner')
        else:
            print("Not connected to internet.")
            # self.log_edit.append("Not connected to internet.")
        # self.log_edit.append(f"File loaded: {file_path}")

    def update_BT_transfer_app(self):
        self.target_app = 'BT_transfer'
        print('update BT transfer app button pressed')
        if self.check_internet_connection():
            print("Connected to internet.")
            # self.log_edit.append("Connected to internet.")
            self.check_version('bt_transfer')
        else:
            print("Not connected to internet.")
            # self.log_edit.append("Not connected to internet.")

    def update_license(self):
        print('update license button pressed')

    def cleanup(self):
        # # Clean up the temporary directory
        # self.temp_dir.cleanup()
        temp_dir = os.path.dirname(self.destination_path)

        while True:
            try:
                shutil.rmtree(temp_dir)
                break
            except:
                time.sleep(5)

    def prompt_to_run_file(self):
        reply = QMessageBox.question(
            None,
            "Install new vertsion",
            "The download is complete. Do you want to install the new version?",
            QMessageBox.Yes | QMessageBox.No,
            QMessageBox.No
        )

        if reply == QMessageBox.Yes:
            self.run_installer_in_thread()
            time.sleep(10)
            self.main_window.close_event()
            # Run the installer in a separate thread

        # else:
        #     self.cleanup()

    def run_installer_in_thread(self):
        # Create a thread for running the installer
        self.installer_thread = InstallerThread(_SCANNER_SETUP_DESTINATION)

        # # Connect the finished signal to the cleanup method
        # self.installer_thread.finished_signal.connect(self.cleanup)

        # Start the thread
        self.installer_thread.start()

    def run_downloaded_file(self):
        # try:
        #     os.startfile(self.destination_path)
        # except Exception as e:
        #     QMessageBox.critical(None, "Error", f"Failed to run the file: {e}")
        # finally:
        #     self.cleanup()
        try:
            print('running setup')
            # Run the installer and wait for it to finish
            subprocess.run([_SCANNER_SETUP_DESTINATION], check=True)

            # # Clean up after the installation is done
            # self.cleanup()
        except subprocess.CalledProcessError as e:
            QMessageBox.critical(None, "Installation Failed", f"Failed to run the installer: {e}")


class InstallerThread(QThread):
    finished_signal = pyqtSignal()  # Signal to emit when installation is done

    def __init__(self, installer_path):
        super().__init__()
        self.installer_path = installer_path

    def run(self):
        try:
            # Run the installer
            subprocess.run([self.installer_path], check=True)
        except subprocess.CalledProcessError as e:
            QMessageBox.critical(None, "Installation Failed", f"Failed to run the installer: {e}")
        finally:
            # Emit signal that installation is finished
            self.finished_signal.emit()

            # Optionally, handle the error (log it, retry, etc.)
    # Exit the program after running the installer

    # def get_main_window(self):
    #     for widget in QApplication.topLevelWidgets():
    #         if isinstance(widget, QMainWindow):
    #             return widget
    #     return None
    #
    # def close_main_window(self):
    #     # Window.close_event(gui_fresco_py.ui_scanner.Window)
    #     self.main_window = self.get_main_window()
    #     if self.main_window:
    #         self.main_window.close()
