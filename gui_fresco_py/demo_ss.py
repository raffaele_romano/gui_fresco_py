import sys
import time
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QProgressBar, QLabel, QFrame, QVBoxLayout
from PyQt5.QtCore import Qt, QTimer, pyqtSignal, QThread
from PyQt5.QtGui import QPixmap

class SplashScreen(QWidget):

    loadingCompleteSignal = pyqtSignal()

    def __init__(self):
        super().__init__()
        self.setWindowTitle('Splash Screen Example')
        self.setFixedSize(500, 500)
        self.setWindowFlag(Qt.FramelessWindowHint)
        self.setAttribute(Qt.WA_TranslucentBackground)

        self.counter = 0
        self.n = 300  # total instance

        self.initUI()

        self.timer = QTimer()
        self.timer.timeout.connect(self.loading)
        self.timer.start(30)

    def initUI(self):
        layout = QVBoxLayout()
        self.setLayout(layout)

        self.frame = QFrame()
        layout.addWidget(self.frame)

        self.labelTitle = QLabel(self.frame)
        self.labelTitle.setObjectName('LabelTitle')

        # center labels
        self.labelTitle.resize(self.width() - 10, 150)
        self.labelTitle.move(0, 40)  # x, y
        self.labelTitle.setText('Splash Screen')
        self.labelTitle.setAlignment(Qt.AlignCenter)

        self.labelDescription = QLabel(self.frame)
        self.labelDescription.resize(self.width() - 10, 50)
        self.labelDescription.move(0, self.labelTitle.height())
        self.labelDescription.setObjectName('LabelDesc')
        self.labelDescription.setText('<strong>Working on Task #1</strong>')
        self.labelDescription.setAlignment(Qt.AlignCenter)

        self.progress_bar = QProgressBar(self.frame)
        self.progress_bar.resize(self.width() - 200 - 10, 50)
        self.progress_bar.move(100, self.labelDescription.y() + 130)
        self.progress_bar.setAlignment(Qt.AlignCenter)
        self.progress_bar.setFormat('%p%')
        self.progress_bar.setTextVisible(True)
        self.progress_bar.setRange(0, self.n)
        self.progress_bar.setValue(20)

        self.labelLoading = QLabel(self.frame)
        self.labelLoading.resize(self.width() - 10, 50)
        self.labelLoading.move(0, self.progress_bar.y() + 70)
        self.labelLoading.setObjectName('LabelLoading')
        self.labelLoading.setAlignment(Qt.AlignCenter)
        self.labelLoading.setText('loading...')

    def loading(self):
        self.progress_bar.setValue(self.counter)

        if self.counter == int(self.n * 0.3):
            self.labelDescription.setText('<strong>Working on Task #2</strong>')
        elif self.counter == int(self.n * 0.6):
            self.labelDescription.setText('<strong>Working on Task #3</strong>')
        elif self.counter >= self.n:
            self.timer.stop()

            # Emit the loading complete signal
            self.loadingCompleteSignal.emit()

            self.close()

            time.sleep(1)

            self.myApp = MyApp()
            self.myApp.show()

        self.counter += 1

    def updateProgressBar(self, value):
        # Slot to update the progress bar
        self.progressBar.setValue(value)

class WorkerThread(QThread):
    updateProgressSignal = pyqtSignal(int)

    def run(self):
        # Simulate the completion of method_one
        time.sleep(1)
        print('1 done')
        self.updateProgressSignal.emit(33)


        # Simulate the completion of method_two
        time.sleep(2)
        print('2 done')
        self.updateProgressSignal.emit(66)

        # Simulate the completion of method_three
        time.sleep(3)
        print('3 done')
        self.updateProgressSignal.emit(100)

class MyApp(QWidget):
    updateProgressSignal = pyqtSignal(int)

    def __init__(self):
        super().__init__()
        self.window_width, self.window_height = 1200, 800
        self.setMinimumSize(self.window_width, self.window_height)

        layout = QVBoxLayout()
        self.setLayout(layout)

        # Connect the updateProgressSignal to the updateProgressBar slot in SplashScreen
        self.updateProgressSignal.connect(self.splash_screen.updateProgressBar)

        # Create a reference to the SplashScreen instance
        self.splash_screen = None

        # Call the methods with a delay using QTimer
        self.callMethodWithDelay(self.method_one, 1000)  # Delay of 1000 milliseconds (1 second)
        self.callMethodWithDelay(self.method_two, 2000)  # Delay of 2000 milliseconds (2 seconds)
        self.callMethodWithDelay(self.method_three, 3000)  # Delay of 3000 milliseconds (3 seconds)

    def callMethodWithDelay(self, method, delay_ms):
        # Call a method with a specified delay using QTimer
        timer = QTimer(self)
        timer.timeout.connect(method)
        timer.start(delay_ms)

    def method_one(self):
        # Simulate the completion of method_one
        # You can add your actual logic here
        print('Method one running')
        self.updateProgressSignal.emit(33)  # Emit signal to update progress to 33%

    def method_two(self):
        # Simulate the completion of method_two
        # You can add your actual logic here
        print('Method two running')
        self.updateProgressSignal.emit(66)  # Emit signal to update progress to 66%

    def method_three(self):
        # Simulate the completion of method_three
        # You can add your actual logic here
        print('Method 3 running')
        self.updateProgressSignal.emit(100)  # Emit signal to update progress to 100%

if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setStyleSheet('''
        #LabelTitle {
            font-size: 60px;
            color: #93deed;
        }

        #LabelDesc {
            font-size: 30px;
            color: #c2ced1;
        }

        #LabelLoading {
            font-size: 30px;
            color: #e8e8eb;
        }

        QFrame {
            background-color: #2F4454;
            color: rgb(220, 220, 220);
        }

        QProgressBar {
            background-color: #DA7B93;
            color: rgb(200, 200, 200);
            border-style: none;
            border-radius: 10px;
            text-align: center;
            font-size: 30px;
        }

        QProgressBar::chunk {
            border-radius: 10px;
            background-color: qlineargradient(spread:pad x1:0, x2:1, y1:0.511364, y2:0.523, stop:0 #1C3334, stop:1 #376E6F);
        }
    ''')

    splash = SplashScreen()
    splash.show()

    try:
        sys.exit(app.exec_())
    except SystemExit:
        print('Closing Window...')
