# -*- coding: utf-8 -*-
"""
Created on Tue Aug  3 13:49:10 2021

@author: Nishad Mandlik
"""

import hashlib
import pickle
from Crypto.Cipher import AES
from base64 import b64encode, b64decode
import random
import secrets


def _pad_bytes(unpadded):
    """
    Pad the byte-array to make its length a multiple of the AES Block Size.

    Parameters
    ----------
    unpadded : bytes
        Unpadded byte-array.

    Returns
    -------
    padded : bytes
        Padded Byte-array.

    """
    # Get the number of padding bytes
    # (4 bytes = 2 random start bytes +
    # 2 bytes to store number of padding bytes)
    num_pad = AES.block_size - (len(unpadded) + 4) % AES.block_size

    padded = secrets.token_bytes(2) + num_pad.to_bytes(2, "little") + \
             unpadded + secrets.token_bytes(num_pad)

    return padded


def _unpad_bytes(padded):
    """
    Remove the padding, and retrieve the original byte-array.

    Parameters
    ----------
    padded : bytes
        Padded byte-array.

    Returns
    -------
    unpadded : bytes
        Original (unpadded) byte-array.

    """
    num_pad = int.from_bytes(padded[2:4], "little")
    unpadded = padded[4: -num_pad]
    return unpadded


def encrypt_aes(decr, key):
    """
    Encrypt the byte array with the supplied key, according to the AES algo.

    Parameters
    ----------
    decr : bytes
        Original (decrypted) byte-array.
    key : str
        Password to be used for encryption.

    Returns
    -------
    encr : bytes
        Encrypted byte-array.

    """
    key = hashlib.sha256(key.encode()).digest()
    padded = _pad_bytes(decr)

    # Generate a random initialization vector
    iv = secrets.token_bytes(16)

    cipher = AES.new(key, AES.MODE_CBC, iv)
    encr = cipher.encrypt(padded)

    # Insert the IV at a random position in the encrypted data
    iv_pos = random.randint(0, len(encr))
    encr = encr[:iv_pos] + iv + encr[iv_pos:]

    # Append a random block containing the position of the IV at the start
    encr = secrets.token_bytes(14) + iv_pos.to_bytes(2, "little") + encr
    return encr


def decrypt_aes(encr, key):
    """
    Decrypt the encrypted byte-array using the supplied key, accroding to the AES algo.

    Parameters
    ----------
    encr : bytes
        Encrypted byte-array.
    key : str
        Password to be used for decryption.

    Returns
    -------
    decr : bytes
        Decrypted byte-array.

    """
    key = hashlib.sha256(key.encode()).digest()

    # Retrieve the position of IV
    iv_pos = int.from_bytes(encr[14:16], "little")
    encr = encr[16:]

    # Seperate IV and the encrypted data
    iv = encr[iv_pos:iv_pos + 16]
    encr = encr[:iv_pos] + encr[iv_pos + 16:]

    cipher = AES.new(key, AES.MODE_CBC, iv)
    padded = cipher.decrypt(encr)

    decr = _unpad_bytes(padded)
    return decr


def encrypt(obj, key):
    """
    Encrypt the given Python Object using the supplied key.

    Parameters
    ----------
    obj : obj
        Python Object which needs to be encrypted.
    key : str
        Password to be used for encryption.

    Returns
    -------
    encr : bytes
        Encrypted byte-array encoded in Base64 format.

    """

    # Serialize the object and encrypt it
    obj_bytes = pickle.dumps(obj)
    obj_encr = encrypt_aes(obj_bytes, key)

    # Encode Key to bytes, and encrypt it
    key_bytes = key.encode()
    key_encr = encrypt_aes(key_bytes, key)

    # Insert the encrypted key at a random position in the encrypted object.
    # Two keys might generate the same SHA value. This technique can be used
    # to check if the key is the correct one.
    key_encr_pos = random.randint(0, len(obj_encr))
    encr = obj_encr[:key_encr_pos] + key_encr + obj_encr[key_encr_pos:]

    # Append a random block containing the key position and
    # key length at the start, and encode to Base64
    encr = b64encode(secrets.token_bytes(12) +
                     key_encr_pos.to_bytes(2, "little") +
                     len(key_encr).to_bytes(2, "little") + encr)
    return encr


def decrypt(encr, key):
    """
    Decrypt the byte-array using the supplied key, and retrieve the original Python Object.

    Parameters
    ----------
    encr : bytes
        Encrypted byte-array encoded in Base64 format.
    key : str
        Password to be used for encryption.

    Returns
    -------
    obj : obj
        Decrypted (original) Python Object.

    """
    # Decode Base64 to Bytes
    encr = b64decode(encr)

    # Retrieve the position of encrypted key
    key_encr_pos = int.from_bytes(encr[12:14], "little")
    key_encr_len = int.from_bytes(encr[14:16], "little")
    encr = encr[16:]

    # Retrieve the encrypted key and decrypt it
    key_retrieved = decrypt_aes(
        encr[key_encr_pos:key_encr_pos + key_encr_len], key)
    if key_retrieved.decode() != key:
        return None

    # Retrieve the encrypted object and decrypt it
    obj_bytes = decrypt_aes(encr[:key_encr_pos] +
                            encr[key_encr_pos + key_encr_len:],
                            key)

    # Deserialize the object
    obj = pickle.loads(obj_bytes)

    return obj
