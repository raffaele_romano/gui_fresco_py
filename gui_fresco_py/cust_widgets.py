# -*- coding: utf-8 -*-
"""
Created on Sun Aug 22 19:45:44 2021.

@author: Nishad Mandlik
"""

from PyQt5.QtCore import QSize, Qt, pyqtSignal as Signal
from PyQt5.QtGui import QCursor, QFont
from PyQt5.QtWidgets import (QCheckBox, QGridLayout, QHBoxLayout, QLabel,
                             QPushButton, QSizePolicy, QVBoxLayout,
                             QRadioButton, QSpacerItem, QTabWidget, QWidget)
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.figure import Figure


class Card(QWidget):
    """
    Widget for a card displaying a title and a percentage with a value-dependent colour.

    Parameters
    ----------
    name : str
        Name of the widget object.
    font_sz_ttl : int
        Font size for the card title.
    font_sz_val : int
        Font size for the value (in %) displayed on the card.
    min_val : int, optional
        Lower limit for the percentage. This is used for the colour-coding.
        The default is 0.
    max_val : int, optional
        Upper limit for the percentage. This is used for the colour-coding.
        The default is 100.
    inverse_clr : bool, optional
        If False, colour coding is performed as red (lowest value) to
        green (highest value). If True, colour coding is performed as
        green (lowest value) to red (highest value). The default is False.
    parent : instance of PyQt5.QtWidgets.QWidget, optional
        Parent widget under whom the current widget should be nested.
        The default is None.

    Returns
    -------
    None.

    """

    def __init__(self, name, font_sz_ttl, font_sz_val, min_val=0, max_val=100,
                 inverse_clr=False, parent=None):
        super().__init__(parent)
        size_policy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        size_policy.setHorizontalStretch(0)
        size_policy.setVerticalStretch(0)
        size_policy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
        self.setSizePolicy(size_policy)
        self.setObjectName(name)
        self.vlay = QVBoxLayout(self)
        self.vlay.setContentsMargins(0, 0, 0, 0)
        self.vlay.setSpacing(5)
        self.vlay.setObjectName(name + "_vLay")
        self.lab_ttl = QLabel(self)
        size_policy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        size_policy.setHorizontalStretch(0)
        size_policy.setVerticalStretch(0)
        size_policy.setHeightForWidth(
            self.lab_ttl.sizePolicy().hasHeightForWidth())
        self.lab_ttl.setSizePolicy(size_policy)
        font = QFont()
        font.setPointSize(font_sz_ttl)
        self.lab_ttl.setFont(font)
        self.lab_ttl.setStyleSheet("color: white;")
        self.lab_ttl.setAlignment(Qt.AlignCenter)
        self.lab_ttl.setObjectName(name + "_lab_ttl")
        self.vlay.addWidget(self.lab_ttl)
        self.lab_val = QLabel(self)
        size_policy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        size_policy.setHorizontalStretch(0)
        size_policy.setVerticalStretch(0)
        size_policy.setHeightForWidth(
            self.lab_val.sizePolicy().hasHeightForWidth())
        self.lab_val.setSizePolicy(size_policy)
        font = QFont()
        font.setPointSize(font_sz_val)
        font.setBold(True)
        font.setWeight(75)
        self.lab_val.setFont(font)
        self.lab_val.setStyleSheet("color: white;")
        self.lab_val.setAlignment(Qt.AlignCenter)
        self.lab_val.setObjectName(name + "_lab_val")
        self.vlay.addWidget(self.lab_val)

        self.min_val = min_val
        self.max_val = max_val
        self.inverse_clr = inverse_clr

    def set_ttl(self, title):
        """
        Set card title.

        Parameters
        ----------
        title : str
            Card title.

        Returns
        -------
        None.

        """
        self.lab_ttl.setText(title)

    def set_val(self, value):
        """
        Set card value. The color is set automatically.

        Parameters
        ----------
        value : int
            Value (in %) to be displayed on the card.

        Returns
        -------
        None.

        """
        value = max(self.min_val, min(self.max_val, value))
        self.lab_val.setText(
            str(value))  # percentage is removed because can't be used with any attr + "%", possible to add customized strings
        clr1 = int((value - self.min_val) / (self.max_val - self.min_val) * 255)
        clr2 = 255 - clr1
        self.lab_val.setStyleSheet("color: #%02x%02x00;" %
                                   ((clr1, clr2) if self.inverse_clr else
                                    (clr2, clr1)))


class CardsGrid(QWidget):
    """
    Widget for displaying a grid of cards.

    Parameters
    ----------
    name : str
        Name of the widget object.
    nrows : int
        Number of rows in the grid.
    ncols : int
        Number of columns in the grid.
    font_sz_ttl : int
        Font size for the title of cards.
    font_sz_val : int
        Font size for the value of cards.
    min_val : int, optional
        Lower limit for the percentage. This is used for the colour-coding.
        The default is 0.
    max_val : int, optional
        Upper limit for the percentage. This is used for the colour-coding.
        The default is 100.
    inverse_clr : bool, optional
        If False, colour coding is performed as red (lowest value) to
        green (highest value). If True, colour coding is performed as
        green (lowest value) to red (highest value). The default is False.
    parent : instance of PyQt5.QtWidgets.QWidget, optional
        Parent widget under whom the current widget should be nested.
        The default is None.

    Returns
    -------
    None.

    """

    def __init__(self, name, nrows, ncols, font_sz_ttl, font_sz_val,
                 min_val=0, max_val=100, inverse_clr=False, parent=None):
        super().__init__(parent)
        size_policy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        size_policy.setHorizontalStretch(0)
        size_policy.setVerticalStretch(0)
        size_policy.setHeightForWidth(
            self.sizePolicy().hasHeightForWidth())
        self.setSizePolicy(size_policy)
        self.glay = QGridLayout(self)
        self.glay.setVerticalSpacing(16)
        self.glay.setObjectName(name + "_gLay")
        self.inverse_clr = inverse_clr
        self.cards = [[None for i in range(0, ncols)] for j in range(0, nrows)]
        for j in range(0, nrows):
            for i in range(0, ncols):
                self.cards[j][i] = Card("%s_%d_%d" % (name, j, i),
                                        font_sz_ttl, font_sz_val,
                                        min_val=min_val, max_val=max_val,
                                        inverse_clr=inverse_clr,
                                        parent=self)
                self.glay.addWidget(self.cards[j][i], j, i, 1, 1)

    def set_card_ttl(self, row, col, ttl):
        """
        Set the title of the card at the specified position.

        Parameters
        ----------
        row : int
            Row index of the card.
        col : int
            Column index of the card.
        ttl : str
            Card title.

        Returns
        -------
        None.

        """
        self.cards[row][col].set_ttl(ttl)

    def set_card_val(self, row, col, val):
        """
        Set the value to be displayed on the card at the specified position.

        Parameters
        ----------
        row : int
            Row index of the card.
        col : int
            Column index of the card.
        val : str
            Value (in %) to be displayed.

        Returns
        -------
        None.

        """
        self.cards[row][col].set_val(val)


class CheckBox(QCheckBox):
    """
    Class for custom QCheckBox.

    Parameters
    ----------
    name : str
        Object name.
    parent : instance of PyQt5.QtWidgets.QWidget, optional
        Parent widget. The default is None.
    font_size : int, optional
        Font-size of the text. The default is 20.
    vmargin : int, optional
        Margin at the left and right edges. The default is 5.

    Returns
    -------
    None.

    """

    def __init__(self, name, parent=None, font_size=20, vmargin=20):
        super().__init__(name, parent)
        font = QFont()
        font.setPointSize(font_size)
        self.setFont(font)
        self.setCursor(QCursor(Qt.PointingHandCursor))
        self.setStyleSheet(
            "QCheckBox {\n"
            "    border-radius: 0;\n"
            "    background: transparent;\n"
            "    margin: 0 " + str(vmargin) + "px;\n"
                                              "    padding: 5px 5px; \n"
                                              "}\n"
                                              "\n"
                                              "QCheckBox:hover {\n"
                                              "    border-top-left-radius: 15px;\n"
                                              "    border-bottom-right-radius: 15px;\n"
                                              "    background: rgba(255, 255, 255, 80);\n"
                                              "    color: #ffffff;\n"
                                              "\n"
                                              "}\n"
                                              "\n"
                                              "QCheckBox::indicator {\n"
                                              "    width: 30px;\n"
                                              "    height: 30px;\n"
                                              "    border-top-left-radius: 7px;\n"
                                              "    border-bottom-right-radius: 7px;\n"
                                              "}\n"
                                              "\n"
                                              "QCheckBox::indicator:unchecked {\n"
                                              "    border: 0; \n"
                                              "    background: #ca3e47;\n"
                                              "    margin: 0 10px 0 10px;\n"
                                              "}\n"
                                              "\n"
                                              "QCheckBox::indicator:checked {\n"
                                              "    border: 2px solid white; \n"
                                              "    background: #69f796;\n"
                                              "    margin: 0 8px 0 8px;\n"
                                              "}")
        self.setIconSize(QSize(20, 20))
        self.setTristate(False)
        self.setChecked(False)
        self.setObjectName("cb_" + name)


class RadioButton(QRadioButton):
    """
    Class for custom QRadioButton.

    Parameters
    ----------
    name : str
        Object name.
    parent : instance of PyQt5.QtWidgets.QWidget, optional
        Parent widget. The default is None.
    font_size : int, optional
        Font-size of the text. The default is 20.
    vmargin : int, optional
        Margin at the left and right edges. The default is 5.

    Returns
    -------
    None.

    """

    def __init__(self, name, parent=None, font_size=20, vmargin=20):
        super().__init__(name, parent)
        font = QFont()
        font.setPointSize(font_size)
        self.setFont(font)
        self.setCursor(QCursor(Qt.PointingHandCursor))
        self.setStyleSheet(
            "QRadioButton {\n"
            "    border-radius: 0;\n"
            "    background: transparent;\n"
            "    margin: 0 " + str(vmargin) + "px;\n"
                                              "    padding: 5px 5px; \n"
                                              "}\n"
                                              "\n"
                                              "QRadioButton:hover {\n"
                                              "    border-top-left-radius: 15px;\n"
                                              "    border-bottom-right-radius: 15px;\n"
                                              "    background: rgba(255, 255, 255, 80);\n"
                                              "    color: #ffffff;\n"
                                              "\n"
                                              "}\n"
                                              "\n"
                                              "QRadioButton::indicator {\n"
                                              "    width: 30px;\n"
                                              "    height: 30px;\n"
                                              "    border-radius: 15px;\n"
                                              "}\n"
                                              "\n"
                                              "QRadioButton::indicator:unchecked {\n"
                                              "    border: 0; \n"
                                              "    background: #ca3e47;\n"
                                              "    margin: 0 10px 0 10px;\n"
                                              "    border-radius: 15px;\n"
                                              "}\n"
                                              "\n"
                                              "QRadioButton::indicator:checked {\n"
                                              "    border: 2px solid white; \n"
                                              "    background: #69f796;\n"
                                              "    margin: 0 8px 0 8px;\n"
                                              "    border-radius: 17px;\n"
                                              "}")
        self.setIconSize(QSize(20, 20))
        self.setChecked(False)
        self.setObjectName("cb_" + name)


class MplCanvas(FigureCanvasQTAgg):
    """
    Modified matplotlib-canvas for drawing the figure.

    Returns
    -------
    None.

    """

    def __init__(self):
        self.fig = Figure()
        self.fig.patch.set_facecolor("None")
        self.fig.subplots_adjust(left=0.1, right=0.98, top=0.98, bottom=0.1)
        self.ax = self.fig.add_subplot(111)
        self.init_canvas()
        super().__init__(self.fig)
        self.setStyleSheet("width: 426px;")
        super().setSizePolicy(QSizePolicy.Expanding,
                              QSizePolicy.Expanding)
        super().updateGeometry()

    def init_canvas(self):
        """
        Erase the axes object and set up the canvas for new plots.

        Returns
        -------
        None.

        """
        self.ax.clear()
        self.ax.patch.set_alpha(0)
        self.ax.spines['bottom'].set_color('#ffffff')
        self.ax.spines['top'].set_color('#ffffff')
        self.ax.spines['right'].set_color('#ffffff')
        self.ax.spines['left'].set_color('#ffffff')
        self.ax.tick_params(axis='x', colors='#ffffff', which='both')
        self.ax.tick_params(axis='y', colors='#ffffff', which='both')
        self.ax.yaxis.label.set_color('#ffffff')
        self.ax.xaxis.label.set_color('#ffffff')
        self.ax.title.set_color('#ffffff')
        self.ax.grid(True, 'both', 'both', color="#ffffff")


class MultiPage(QWidget):
    """
    Multiple page widget with page-title display and side-nav buttons.

    Parameters
    ----------
    name : str
        Name of the widget object.
    parent : instance of PyQt5.QtWidgets.QWidget, optional
        Parent widget. The default is None.

    Returns
    -------
    None.

    """

    currentChanged = Signal(int)

    def __init__(self, name, parent=None):
        super().__init__(parent)
        self.setStyleSheet(
            """
            QPushButton {
                border: none;
                color: white;
                margin: 0;
                padding: 0;
            }

            QPushButton:hover {
                border: none;
                color: #ca3e47;
            }
            """)
        self.setObjectName(name)
        self.hlay = QHBoxLayout(self)
        self.hlay.setContentsMargins(0, 0, 0, 0)
        self.hlay.setObjectName(name + "_hLay")

        self.btn_prev = QPushButton(self)
        size_policy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        size_policy.setHorizontalStretch(0)
        size_policy.setVerticalStretch(0)
        size_policy.setHeightForWidth(
            self.btn_prev.sizePolicy().hasHeightForWidth())
        self.btn_prev.setSizePolicy(size_policy)
        self.btn_prev.setMinimumSize(QSize(0, 0))
        self.btn_prev.setCursor(QCursor(Qt.PointingHandCursor))
        self.btn_prev.setObjectName(name + "_btn_prev")
        self.hlay.addWidget(self.btn_prev)
        self.btn_prev.setText("❮")

        self.vlay = QVBoxLayout()
        self.vlay.setSpacing(0)
        self.vlay.setObjectName(name + "_vLay")

        self.page_ttl = QLabel(self)
        font = QFont()
        font.setPointSize(14)
        self.page_ttl.setFont(font)
        self.page_ttl.setStyleSheet("color: white;")
        self.page_ttl.setAlignment(Qt.AlignCenter)
        self.page_ttl.setObjectName("smry_juiciness_part_ttl")
        self.vlay.addWidget(self.page_ttl)

        self.pages = QTabWidget(self)
        font = QFont()
        font.setPointSize(14)
        self.pages.setFont(font)
        self.pages.setStyleSheet(
            """
            TabWidget::pane {
                background: transparent;
                border: 0;
            }

            QTabBar::tab {
                height: 0px;
                background: transparent;
                color: transparent;
                margin: 0;
                border: 0;
                padding: 0;
            }

            QTabBar::tab:selected {
                height: 0px;
                background: transparent;
                color: transparent;
                margin: 0;
                border: 0;
                padding: 0;
            }
            """)
        self.pages.setDocumentMode(False)
        self.pages.setObjectName(name + "_pages")
        self.vlay.addWidget(self.pages)
        self.hlay.addLayout(self.vlay)

        self.btn_next = QPushButton(self)
        size_policy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        size_policy.setHorizontalStretch(0)
        size_policy.setVerticalStretch(0)
        size_policy.setHeightForWidth(
            self.btn_next.sizePolicy().hasHeightForWidth())
        self.btn_next.setSizePolicy(size_policy)
        self.btn_prev.setMinimumSize(QSize(0, 0))
        self.btn_next.setCursor(QCursor(Qt.PointingHandCursor))
        self.btn_next.setObjectName(name + "_btn_next")
        self.hlay.addWidget(self.btn_next)
        self.btn_next.setText("❯")

        # Link internal tab-widget to self
        self.pages.currentChanged.connect(lambda index:
                                          self.__page_chg_actions(index))
        self.addTab = self.pages.addTab
        self.count = self.pages.count
        self.currentIndex = self.pages.currentIndex
        self.currentWidget = self.pages.currentWidget
        self.indexOf = self.pages.indexOf
        self.insertTab = self.pages.insertTab
        self.removeTab = self.pages.removeTab
        self.setCurrentIndex = self.pages.setCurrentIndex
        self.setCurrentWidget = self.pages.setCurrentWidget
        self.tabText = self.pages.tabText
        self.widget = self.pages.widget

        self.pages.tabInserted = self.tab_inserted
        self.pages.tabRemoved = self.tab_removed

        # Set button actions
        self.btn_prev.clicked.connect(self.prev_page)
        self.btn_next.clicked.connect(self.next_page)

    def prev_page(self):
        """
        Display previous page (page towards the left).

        Returns
        -------
        None.

        """
        self.setCurrentIndex(self.currentIndex() - 1)

    def next_page(self):
        """
        Display next page (page towards the right).

        Returns
        -------
        None.

        """
        self.setCurrentIndex(self.currentIndex() + 1)

    def tab_inserted(self, index):
        """
        Definition of a virtual func which is called when a new page is added.

        Parameters
        ----------
        index : int
            Index at which the page was added.

        Returns
        -------
        None.

        """
        self.__page_chg_actions(self.currentIndex())

    def tab_removed(self, index):
        """
        Definition of a virtual func which is called when a page is removed.

        Parameters
        ----------
        index : int
            Index from which the page was removed.

        Returns
        -------
        None.

        """
        self.__page_chg_actions(self.currentIndex())

    def __page_chg_actions(self, index):
        """
        Configure the page title and nav buttons after a adding/removing a page, or switching to a different page.

        Parameters
        ----------
        index : int
            Index of the current page.

        Returns
        -------
        None.

        """
        self.page_ttl.setText(self.tabText(index))

        if index == 0:
            self.btn_prev.setEnabled(False)
            self.btn_prev.setStyleSheet("color: transparent;")
        else:
            self.btn_prev.setEnabled(True)
            self.btn_prev.setStyleSheet("color: white;")

        if index == self.count() - 1:
            self.btn_next.setEnabled(False)
            self.btn_next.setStyleSheet("color: transparent;")
        else:
            self.btn_next.setEnabled(True)
            self.btn_next.setStyleSheet("color: white;")

        self.currentChanged.emit(index)


class AttrValsGrid(CardsGrid):
    """
    Custom grid of cards for showing the attribute values (in %) for several fruits.

    Parameters
    ----------
    name : str
        Name of the widget object.
    start_index : int
        Index of the fruit to which the first card in the grid corresponds.
    min_val : int, optional
        Lower limit for the percentage. This is used for the colour-coding.
        The default is 0.
    max_val : int, optional
        Upper limit for the percentage. This is used for the colour-coding.
        The default is 100.
    ncards : int, optional
        Number of cards to be actually displayed in the grid. If less than zero
        of greater than the total places, all places in the grid will be
        occupied by cards. The default is -1.
    inverse_clr : bool, optional
        If False, colour coding is performed as red (lowest value) to
        green (highest value). If True, colour coding is performed as
        green (lowest value) to red (highest value). The default is False.
    parent : instance of PyQt5.QtWidgets.QWidget, optional
        Parent widget. The default is None.

    Returns
    -------
    None.

    """

    def __init__(self, name, start_index, min_val=0, max_val=100, ncards=-1,
                 inverse_clr=False, parent=None):
        nrows = 5
        ncols = 4
        super().__init__(name, nrows, ncols, 14, 16,
                         min_val, max_val, inverse_clr, parent)
        if (ncards < 0) or (ncards > (nrows * ncols)):
            ncards = nrows * ncols
        for i in range(0, ncards):
            self.set_card_ttl(i // ncols, i % ncols, str(i + start_index))

    def set_card_val(self, card_id, val):
        """
        Set the value to be displayed by the specified card in the grid.

        Parameters
        ----------
        card_id : int
            Flattened index of the card. Index begins with zero at the top left
            corned, increases along first row from left to right, and then
            continues to increase along the next rows.
        val : int
            Value (in %) to be displayed by the card.

        Returns
        -------
        None.

        """
        super().set_card_val(card_id // len(self.cards[0]),
                             card_id % len(self.cards[0]), val)


class AttrValsRow(CardsGrid):
    """
    Row of cards displaying values for Average, Min and Max.

    Parameters
    ----------
    name : str
        Name of the widget object.
    min_val : int, optional
        Lower limit for the percentage. This is used for the colour-coding.
        The default is 0.
    max_val : int, optional
        Upper limit for the percentage. This is used for the colour-coding.
        The default is 100.
    inverse_clr : bool, optional
        If False, colour coding is performed as red (lowest value) to
        green (highest value). If True, colour coding is performed as
        green (lowest value) to red (highest value). The default is False.
    parent : instance of PyQt5.QtWidgets.QWidget, optional
        Parent widget. The default is None.

    Returns
    -------
    None.

    """

    def __init__(self, name, min_val=0, max_val=100, inverse_clr=False,
                 parent=None):
        super().__init__(name, 1, 3, 16, 18, min_val, max_val,
                         inverse_clr, parent)
        self.set_card_ttl(0, 0, "AVG")
        self.set_card_ttl(0, 1, "MIN")
        self.set_card_ttl(0, 2, "MAX")

    def set_avg_val(self, val):
        """
        Set the average value (in %) in the corresponding card.

        Parameters
        ----------
        val : int
            Average value (in %).

        Returns
        -------
        None.

        """
        self.set_card_val(0, 0, val)

    def set_min_val(self, val):
        """
        Set the minimum value (in %) in the corresponding card.

        Parameters
        ----------
        val : int
            Minimum value (in %).

        Returns
        -------
        None.

        """
        self.set_card_val(0, 1, val)

    def set_max_val(self, val):
        """
        Set the maximum value (in %) in the corresponding card.

        Parameters
        ----------
        val : int
            Maximum value (in %).

        Returns
        -------
        None.

        """
        self.set_card_val(0, 2, val)


class AttrValsMulti(MultiPage):
    """
    Multipage widget for displaying cards for several fruits.

    Parameters
    ----------
    name : str
        Name of the widget object.
    nfruits : int
        The number of fruits to be displayed.
    min_val : int, optional
        Lower limit for the percentage. This is used for the colour-coding.
        The default is 0.
    max_val : int, optional
        Upper limit for the percentage. This is used for the colour-coding.
        The default is 100.
    inverse_clr : bool, optional
        If False, colour coding is performed as red (lowest value) to
        green (highest value). If True, colour coding is performed as
        green (lowest value) to red (highest value). The default is False.
    parent : instance of PyQt5.QtWidgets.QWidget, optional
        Parent widget. The default is None.

    Returns
    -------
    None.

    """

    def __init__(self, name, nfruits, min_val=0, max_val=100,
                 inverse_clr=False, parent=None):
        super().__init__(name, parent)
        fruits_pending = nfruits
        while fruits_pending > 0:
            batch_sz = min(fruits_pending, 20)
            start_id = nfruits - fruits_pending + 1

            # No need to have parent for adding widget to tabbed widget
            grid = AttrValsGrid("%s_%d" % (name, (self.count() + 1)), start_id,
                                min_val=min_val, max_val=max_val,
                                ncards=batch_sz, inverse_clr=inverse_clr,
                                parent=None)

            self.addTab(grid, "Fruits %d-%d" % (start_id,
                                                start_id + batch_sz - 1))
            fruits_pending = fruits_pending - batch_sz
        self.setCurrentIndex(0)

    def set_val(self, fruit_id, val):
        """
        Set the attribute value for a particular fruit.

        Parameters
        ----------
        fruit_id : int
            Index of the fruit.
        val : int
            Value (in %) to be displayed by the corresponding card.

        Returns
        -------
        None.

        """
        page_id = fruit_id // 20
        if page_id >= self.count():
            return
        card_id = fruit_id % 20
        self.widget(page_id).set_card_val(card_id, round(val, 2))


# Matplotlib widget
class MplWidget(QWidget):
    """
    QT Widget for matplotlib canvas.

    Parameters
    ----------
    parent : instance of PyQt5.QtWidgets.QWidget, optional
        Parent widget. The default is None.

    Returns
    -------
    None.

    """

    def __init__(self, parent=None):
        super().__init__(parent)
        self.canvas = MplCanvas()
        self.vbl = QVBoxLayout()
        self.vbl.setContentsMargins(0, 0, 0, 0)
        self.vbl.addWidget(self.canvas)
        self.setLayout(self.vbl)


class AttrTabWidget(QTabWidget):
    """
    Tabbed widget for holding multi-page card grids for various attributes.

    Parameters
    ----------
    parent : instance of PyQt5.QtWidgets.QWidget, optional
        Parent widget. The default is None.

    Returns
    -------
    None.

    """

    def __init__(self, parent=None):
        super().__init__(parent)
        self.nfruits = 0

    def set_nfruits(self, nfruits):
        """
        Set the number of fruits to be displayed.

        Warning
        -------
        If the specified number of fruits differs from the exiting value, all
        existing tabs will be removed.

        Parameters
        ----------
        nfruits : int
            Number of fruits to be displayed.

        Returns
        -------
        None.

        """
        if nfruits != self.nfruits:
            self.clear()
            self.nfruits = nfruits

    def get_nfruits(self):
        """
        Return the number of fruits displayed in an attribute tab.

        Returns
        -------
        int
            Number of fruits.

        """
        return self.nfruits

    def add_attr(self, attr_name, min_val=0, max_val=100, inverse_clr=False):
        """
        Add a tab for the specified attribute.

        Parameters
        ----------
        attr_name : str
            Name of the attribute.
        min_val : int, optional
            Lower limit for the percentage. This is used for the colour-coding.
            The default is 0.
        max_val : int, optional
            Upper limit for the percentage. This is used for the colour-coding.
            The default is 100.
        inverse_clr : bool, optional
            If False, colour coding is performed as red (lowest value) to
            green (highest value). If True, colour coding is performed as
            green (lowest value) to red (highest value). The default is False.

        Returns
        -------
        None.

        """
        base_name = self.objectName() + "_" + attr_name
        attr = QWidget()
        attr.setObjectName(base_name)
        vlay = QVBoxLayout(attr)
        vlay.setContentsMargins(0, 0, 0, 5)
        vlay.setSpacing(6)
        vlay.setObjectName(base_name + "_vLay")

        attr_row = AttrValsRow(base_name + "_row", min_val=min_val,
                               max_val=max_val, inverse_clr=inverse_clr,
                               parent=attr)
        vlay.addWidget(attr_row)

        spacer = QSpacerItem(20, 40, QSizePolicy.Minimum,
                             QSizePolicy.Expanding)
        vlay.addItem(spacer)

        attrs_multi = AttrValsMulti(base_name + "_multi", self.nfruits,
                                    min_val=min_val, max_val=max_val,
                                    inverse_clr=inverse_clr, parent=attr)
        vlay.addWidget(attrs_multi)

        spacer = QSpacerItem(20, 40, QSizePolicy.Minimum,
                             QSizePolicy.Expanding)
        vlay.addItem(spacer)

        self.addTab(attr, attr_name.capitalize())

    def get_attr_widget(self, attr_name):
        """
        Return the widget corresponding to the specified attribute.

        Parameters
        ----------
        attr_name : str
            DESCRIPTION.

        Returns
        -------
        instance of PyQt5.QtWidgets.QWidget or None
            If the attribute name is found among the tabs, the correspinding
            widget is returned. Otherwise None is returned.

        """
        for i in range(0, self.count()):
            if self.tabText(i) == attr_name:
                return self.widget(i)
        return None

    def set_val(self, attr, fruit_id, val):
        """
        Set the attribute value for the specified fruit.

        Parameters
        ----------
        attr : str
            Name of the attribute.
        fruit_id : int
            Index of the fruit.
        val : int
            Attribute value (in %) for the fruit.

        Returns
        -------
        None.

        """
        wid = self.get_attr_widget(attr)
        wid.children()[2].set_val(fruit_id, val)

    def set_vals(self, attr, vals):
        """
        Set the values of the specified attribute for all fruits.

        Parameters
        ----------
        attr : str
            Attribute name.
        vals : numpy.ndarray
            Array of values for all fruits.

        Returns
        -------
        None.

        """
        end = min(self.get_nfruits(), vals.size)
        vals = vals[:end]

        wid = self.get_attr_widget(attr)
        wid.children()[1].set_avg_val(round(vals.mean(), 2))
        wid.children()[1].set_min_val(round(vals.min(), 2))
        wid.children()[1].set_max_val(round(vals.max(), 2))
        for i in range(0, end):
            wid.children()[2].set_val(i, vals[i])

    def clear(self):
        """
        Remove all tabs and reset the number of fruits to zero.

        Returns
        -------
        None.

        """
        while self.count():
            w = self.widget(0)
            self.removeTab(0)
            w.deleteLater()
        self.nfruits = 0

    def __getitem__(self, attr_name):
        """
        Enable dict-like indexing to get widgets corresponding to the specified attribute name.

        Alias for :py:func:`cust_widgets.AttrTabWidget.get_attr_widget`.

        """
        return self.get_attr_widget(self, attr_name)


class RxFilesSelAllCB(CheckBox):
    """
    CheckBox widget for implementing select-all functionality in file-transfer.

    Parameters
    ----------
    parent : instance of PyQt5.QtWidgets.QWidget, optional
        Parent widget. The default is None.

    Returns
    -------
    None.

    """

    def __init__(self, parent):
        super().__init__("select_all", parent=parent, font_size=16, vmargin=5)
