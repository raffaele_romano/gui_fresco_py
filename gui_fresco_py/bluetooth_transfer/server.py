import socket
import os
import bluetooth

BUFFER_SIZE = 1024
SEPARATOR = '<SEPARATOR>'


def initialize_socket(mac_address, port):
    s = socket.socket(socket.AF_BLUETOOTH, socket.SOCK_STREAM, socket.BTPROTO_RFCOMM)
    s.bind((mac_address, port))
    print(f'[*] Listening as {mac_address}:{port}')
    return s


def listen_for_connections(main_socket, max_connections):
    main_socket.listen(max_connections)
    client, address = main_socket.accept()
    print(f'[+] {address} is connected.')
    return client


def receive_file_info(client):
    # receive file info
    received = client.recv(BUFFER_SIZE).decode()
    files = received.split(SEPARATOR)
    # remove absolute path
    # name = os.path.basename(name)
    # convert to integer
    # size = int(size)
    return files


def receive_file(name):
    with open(name, 'wb') as f:
        while True:
            bytes_read = client_socket.recv(BUFFER_SIZE)
            if not bytes_read:
                # file transmitting is done
                break
            f.write(bytes_read)


def end_connections(client, main_socket):
    client.close()
    main_socket.close()


'''

 InspironRaf 'D8:3B:BF:31:AD:C0' VictusRaf = '14:13:33:D8:5B:12'
 
 bluetooth.read_local_bdaddr()[0]

'''


if __name__ == "__main__":
    try:
        host_mac_address = '14:13:33:D8:5B:12'
        # host_mac_address = bluetooth.read_local_bdaddr()[0]
        server_port = 7
        server_socket = initialize_socket(host_mac_address, server_port)
        backlog = 1  # number of connection that the system will allow

        client_socket = listen_for_connections(server_socket, backlog)

        files_list = receive_file_info(client_socket)
        print(files_list)
        files_list.pop()
        print('files = ' + str(files_list))

        requested_files = ''
        for file in files_list:
            requested_files += file + SEPARATOR

        client_socket.send(requested_files.encode())

        for filename in files_list:
            received = client_socket.recv(BUFFER_SIZE).decode()
            client_socket.send('ACK'.encode())
            received.split(SEPARATOR)
            with open(filename, 'wb') as f:
                while True:
                    bytes_read = client_socket.recv(BUFFER_SIZE)
                    if not bytes_read:
                        # file transmitting is done
                        break
                    f.write(bytes_read)




    finally:
        # receive_file(filename)

        end_connections(client_socket, server_socket)
