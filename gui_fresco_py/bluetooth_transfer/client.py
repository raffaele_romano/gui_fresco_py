import socket
import os
import sys

SEPARATOR = '<SEPARATOR>'
BUFFER_SIZE = 4096  # send 4096 bytes each time step


def connect_to_server(mac_address, port):
    s = socket.socket(socket.AF_BLUETOOTH, socket.SOCK_STREAM, socket.BTPROTO_RFCOMM)
    print(f"[+] Connecting to {mac_address}:{port}")
    s.connect((mac_address, port))
    print("[+] Connected.")
    return s


def send_file(server_socket, file):
    filesize = os.path.getsize(file)
    server_socket.send(f'{file}{SEPARATOR}{filesize}'.encode())
    with open(file, 'rb') as f:
        while True:
            # read the bytes from the file
            bytes_read = f.read(BUFFER_SIZE)
            if not bytes_read:
                # file transmitting is done
                break
            # we use sendall to assure transmission in busy networks
            server_socket.sendall(bytes_read)


def disconnect_server(server_socket):
    server_socket.close()


'''

 InspironRaf 'D8:3B:BF:31:AD:C0' VictusRaf = '14:13:33:D8:5B:12'

 bluetooth.read_local_bdaddr()[0]

'''


if __name__ == '__main__':
    server_mac_address = '14:13:33:D8:5B:12'   # 'C4:23:60:F0:C1:88'
    server_port = 7

    socket = connect_to_server(server_mac_address, server_port)

    files = ['kek.txt', 'kek2.txt', 'kek3.txt']
    files_string = ''
    for f in files:
        files_string += f + SEPARATOR
    print(files_string)
    encoded_files_string = files_string.encode()

    socket.send(encoded_files_string)

    requested_files_list = socket.recv(BUFFER_SIZE).decode().split(SEPARATOR)
    requested_files_list.pop()
    
    #print(requested_files_list)
    #print(requested_files_list.split(SEPARATOR))

    for filename in requested_files_list:
        filesize = os.path.getsize(filename)
        print(f'sending {filename}{SEPARATOR}{filesize}')
        socket.send(f'{filename}{SEPARATOR}{filesize}'.encode())
        with open(filename, 'rb') as f:
            while True:
                bytes_read = f.read(BUFFER_SIZE)
                if not bytes_read:
                    break
                socket.sendall(bytes_read)
        ack = socket.recv(BUFFER_SIZE)
        print(ack.decode())
                
    socket.close()
                
    #filesize = os.path.getsize(file)
    #server_socket.send(f'{file}{SEPARATOR}{filesize}'.encode())
    #with open(file, 'rb') as f:
        #while True:
            # read the bytes from the file
         #   bytes_read = f.read(BUFFER_SIZE)
          #  if not bytes_read:
                # file transmitting is done
           #     break
            # we use sendall to assure transmission in busy networks
            #server_socket.sendall(bytes_read)
    #print(files_string)
    if False:
        input_file = None

        if len(sys.argv) < 2:
            print(f'[+] Usage: {sys.argv[0]} {{filename}}')
            exit(1)
        else:
            input_file = sys.argv[1]

        server_mac_address = 'C4:23:60:F0:C1:88'
        server_port = 5

        socket = connect_to_server(server_mac_address, server_port)

        send_file(socket, input_file)

        disconnect_server(socket)
