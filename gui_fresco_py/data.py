# -*- coding: utf-8 -*-
"""
Created on Thu Aug 19 17:06:13 2021.

@author: Nishad Mandlik
"""
import numpy as np
import json
import os
from time import sleep

from gui_fresco_py.calib import Cal
from gui_fresco_py.lic_manager import License
from matplotlib import pyplot as plt
from gui_fresco_py.measurement import Meas
from gui_fresco_py.settings import Settings
from gui_fresco_py.vna import VNA
from gui_fresco_py.gauge import Gauge
from pathlib import Path
import joblib

_CALKIT_NAME = "Rosenberger_03CK10A_Female_W5101"
_SCRIPT_DIR = Path(__file__).parent.parent
_BOUNDS_DIR = _SCRIPT_DIR / "data/User/Calibration/Bounds"
_MODELS_DIR = _SCRIPT_DIR / "data/User/Models"


class Holder():
    """
    Class for holding the data used during the application.

    Returns
    -------
    None.

    """

    def __init__(self):
        self.l = License()
        self.v = VNA()
        self.g = Gauge()
        self.c = Cal(self.v.vna_name, self.v.vna_sn, _CALKIT_NAME)
        self.s = Settings(self.v.vna_name)
        if self.c.is_complete():
            self.set_freq(self.c.freq[0], self.c.freq[-1],
                          self.c.freq.size)
        else:
            self.set_freq(self.s["start_freq"], self.s["stop_freq"],
                          self.s["n_points"])
        self.m = None
        self.models_dict = None

    def get_fruit_names(self):
        """
        Return the names of available fruits.

        Returns
        -------
        list
            List of names of fruits.

        """
        return self.l.get_collection().get_fruit_names()

    def get_breed_names(self, fruit_name):
        """
        Return the names of available breeds for the specified fruit.

        Parameters
        ----------
        fruit_name : str
            Fruit name for which the breeds need to be retrieved.

        Returns
        -------
        list
            List of breeds.

        """
        fruit = self.l.get_collection()[fruit_name]
        if fruit is None:
            return []
        return fruit.get_breed_names()

    def get_attr_names(self, fruit_name, breed_name):
        """
        Return the names of available attributes for the specified fruit and breed.

        Parameters
        ----------
        fruit_name : str
            Fruit name.
        breed_name : str
            Breed name.

        Returns
        -------
        list
            List of attributes.

        """
        fruit = self.l.get_collection()[fruit_name]
        if fruit is None:
            return []
        breed = fruit[breed_name]
        if breed is None:
            return []
        return breed.get_attr_names()

    @property
    def vna_min_freq(self):
        """
        Return the minimum compatible frequency for the attached VNA device.

        Returns
        -------
        float
            Minimum possible frequency (in Hz) for the VNA.

        """
        return self.v.get_min_freq()

    @property
    def vna_max_freq(self):
        """
        Return the maximum compatible frequency for the attached VNA device.

        Returns
        -------
        float
            Maximum possible frequency (in Hz) for the VNA.

        """
        return self.v.get_max_freq()

    def get_calib_status(self, status_type="overall"):
        """
        Return the calibration status.

        Parameters
        ----------
        status_type : str, optional
            If "individual", returns the status of individual load-, open-,
            and short- calibration. If "overall", returns the overall status
            of the calibration. The default is "overall".

        Returns
        -------
        dict or bool
            If "individual", dictionary of individual status. If "overall",
            status of the complete calibration.

        """
        if status_type == "overall":
            return self.c.is_complete()
        elif status_type == "individual":
            return {"load": self.c.is_load_done(),
                    "open": self.c.is_open_done(),
                    "short": self.c.is_short_done()}

    def calibrate(self, kind, ax=None):
        """
        Perform calibration.

        Parameters
        ----------
        kind : str
            Type of calibration ("load", "open" or "short").
        ax : matplotlib.axes.Axes, optional
            Axes object for plotting the measurement. The default is None.

        Returns
        -------
        bool
            True, if entire calibration is complete. False, otherwise.

        """
        self.set_freq(self.s.start_freq, self.s.stop_freq, self.s.n_points)
        s11_raw = self.v.meas_s11()

        if ax is not None:
            for line in ax.get_lines():
                line.remove()
            abs_s11_raw = abs(s11_raw)
            ax.plot(self.s.get_freq_vect(), abs_s11_raw, color="orange",
                    linewidth=4)
            mx = max(abs_s11_raw)
            mn = min(abs_s11_raw)
            padding = 0.05 * (mx - mn)
            ax.set_ylim(mn - padding, mx + padding)

        if kind == "load":
            return self.c.calib_load(s11_raw)
        elif kind == "open":
            return self.c.calib_open(s11_raw)
        elif kind == "short":
            return self.c.calib_short(s11_raw)

    def validate_freq_calib(self, start, stop, n_points):
        """
        Validate the supplied frequency vector parameters with the calibration.

        Parameters
        ----------
        start : float
            Lower-limit of the frequency range (in Hz).
        stop : float
            Upper-limit of the frequency range (in Hz).
        n_points : int
            Number of frequency points (including the upper and lower
                                        limits).

        Returns
        -------
        bool
            True, if the supplied frequency vector parameters matches the
            calibration. False, otherwise.

        """
        if self.c.validate_freq(np.linspace(start, stop, n_points)):
            return True
        return False

    def reset_calib(self):
        """
        Erase the existing calib file (if any), and reinitialize a blank one.

        Returns
        -------
        None.

        """
        self.c.reset_cal()

    def set_freq(self, start, stop, n_points):
        """
        Set the frequency range to be used during the application.

        Parameters
        ----------
        start : float
            Lower-limit of the frequency range (in Hz).
        stop : float
            Upper-limit of the frequency range (in Hz).
        n_points : int
            Number of frequency points (including the upper and lower
                                        limits).

        Returns
        -------
        None.

        """
        # First change the Settings object
        self.s.set_freq_range(start, stop, n_points)

        # Then apply to VNA. VNA Object might change the endpoints according
        # to its limits.
        self.v.apply_settings(self.s)

        # Finally apply to calibration.
        # Calibration will reset in case of incompatible settings
        self.c.freq = np.linspace(self.s.start_freq,
                                  self.s.stop_freq, self.s.n_points)

    def get_freq(self):
        """
        Set the frequency range parameters being used during the application.

        Returns
        -------
        tuple
            Frequency range parameters (start, stop, number of points).

        """
        return self.s.get_freq_range()

    def set_n_scans(self, n_fruits, n_meas):
        """
        Set the number of scans to be performed.

        Parameters
        ----------
        n_fruits : int
            Number of fruits to be measured.
        n_meas : int
            Number of measurements per fruit.

        Returns
        -------
        None.

        """
        self.s.set_n_scans(n_fruits, n_meas)

        # After changing settings, reinit the Measurement object because the
        # array dimensions may no longer be valid
        if self.m is not None:
            self.init_scan(self.m.name, self.ax, self.scan_info)

    def get_n_scans(self):
        """
        Return the parameters defining the number of scans to be performed.

        Returns
        -------
        int
            Number of fruits to be measured.
        int
            Number of measurements per fruit.

        """
        return self.s.get_n_scans()

    def load_models(self):
        """

        Load predictions models.

        Returns
        -----------

        models_dict: dict
            Dictionary with prediction models for the attributes selected by the user, available in scan_info

        """
        models_dict = {}
        if self.scan_info is not None and len(self.scan_info.split(',')) > 1:
            fruit, breed_patch = self.scan_info.split(',')
            breed_patch = breed_patch.lstrip()
            breed_patch = breed_patch.replace(" ", "_")
            model_folder_name = breed_patch
            input_var = 's11'

            if 'demo' not in breed_patch.lower():

                breed, probe = breed_patch.split("_", 1)

                for attribute in self.attributes:

                    observation = attribute.lower()

                    url_model_dict = _MODELS_DIR / Path(
                        fruit + "\\" + model_folder_name + \
                        "\\Model_" + probe + "_" + input_var + "To" + observation.replace(" ", "") + ".pkl")
                    try:
                        m_dict = joblib.load(url_model_dict)

                        models_dict[observation] = m_dict

                        print(f'Model {observation} loaded')

                    except FileNotFoundError:
                        # Handle the case where the file does not exist
                        print("The file does not exist.")
                        # QMessageBox.critical(None, 'Error', f"An error occurred while predicting {observation}\n")

                    except Exception as e:
                        # Handle other potential exceptions that may occur during loading
                        print("An error occurred:", e)
                        # QMessageBox.critical(None, 'Error', f"An error occurred while predicting {observation}\n")

        return models_dict

    def init_scan(self, session_name, ax=None, scan_info=None,
                  default_n_scans=False, attrs=[]):
        """
        Initialize the scan.

        Parameters
        ----------
        session_name : str
            Name of the measurement session.
        ax : matplotlib.axes.Axes, optional
             Axes object for plotting the measurement(s). The default is None.
        scan_info : str or None, optional
            Additional info to be registered with the scan.
            The default is None.
        default_n_scans : bool, optional
            If True, the scan is initialized with the number of fruits and
            meas from the default settings. The default is False.
        attrs : list of selected attributes to be predicted

        Returns
        -------
        None.

        """
        self.s11 = None
        self.s11_raw = None
        self.sensorT = None
        self.bounds = {}
        self.scan_info = scan_info
        self.attributes = attrs
        self.pred_attrs = []

        n_attrs = len(self.attributes)

        if default_n_scans:
            defaults = self.s.get_defaults()
            n_fruits = defaults["n_fruits"]
            n_meas = defaults["n_meas"]
        else:
            n_fruits = self.s["n_fruits"]
            n_meas = self.s["n_meas"]

        eterms = self.c.eterms if self.c.is_complete() else None
        self.m = Meas(session_name, n_fruits, n_meas, n_attrs, self.s.get_freq_vect(),
                      self.v.vna_name, self.v.vna_sn, self.s["power"],
                      cal_eterms=eterms, info=self.scan_info, attributes=attrs)

        self.ax = ax
        self.bounds = self.get_bounds()
        self.models_dict = self.load_models()
        self.v.scpi.write('*CLS')
        if self.ax is not None:
            for line in self.ax.get_lines():
                line.remove()
            self.ax.set_prop_cycle(None)

    @property
    def scan_started(self):
        """
        Check if the measurement has started.

        Returns
        -------
        bool
            True, if the measurement has started. False, otherwise.

        """
        if self.m is None:
            return False
        return self.m.is_started()

    def _update_ax(self):
        """
        Update the axes object to show the necessary plots and remove the rest.

        Returns
        -------
        None.

        """
        cols = [i["color"] for i in plt.rcParams['axes.prop_cycle']]
        freq_vect = self.s.get_freq_vect()

        # If only one measurement per fruit, plot all measurements together
        # Otherwise erase plot after each fruit
        if self.m.n_meas == 1:
            index = self.m.sample_count + 1
        else:
            index = (self.m.sample_count + 1) % self.m.n_meas

        for line in self.ax.get_lines()[index:]:
            line.remove()

        self.ax.plot(freq_vect, abs(self.s11),
                     color=cols[index % len(cols)], linewidth=4)

        self.ax.autoscale(enable=True, axis='y')

        # # if self.m.sample_count > -1:
        # if self.m.is_started():
        #     s_min = np.nanmin([np.nanmin(np.abs(self.m.s11)), np.nanmin(abs(self.s11))])
        #     s_max = np.nanmax([np.nanmax(np.abs(self.m.s11)), np.nanmax(abs(self.s11))])
        #     self.ax.axes.set_ylim(s_min - 0.025, s_max + 0.025)

        if self.bounds:
            try:
                # Plot the bounds
                mean_line = self.ax.plot(freq_vect, self.bounds['mean'], color='blue', linestyle='dashed')
                lower_b = self.ax.plot(freq_vect, self.bounds['lower_b'], color='green', linestyle='dashed')
                upper_b = self.ax.plot(freq_vect, self.bounds['upper_b'], color='red', linestyle='dashed')
            except Exception as e:
                print("An error occurred plotting the bounds: " + str(e))

    def perform_meas(self):
        """
        Perform the measurement using the attached VNA device and correct it using the available calibration.

        Returns
        -------
        None.

        """
        self.s11_raw = self.v.meas_s11()
        while len(self.s11_raw) < 2:
            print('No s11 raw')
            sleep(0.1)
            self.s11_raw = self.v.meas_s11()

        self.s11 = self.c.apply_correction(self.s11_raw)
        self.sensorT = self.v.get_temperature()

        # Update plot
        if self.ax is not None:
            self._update_ax()

    def commit_meas(self):
        """
        Add the preformed measurement to the measurement object.

        Returns
        -------
        None.

        """
        if self.s11_raw is not None:
            self.m.add_meas(self.s11_raw, self.s11, self.sensorT, self.pred_attrs)
            self.s11_raw = None
            self.s11 = None
            self.sensorT = None
            self.pred_attrs = []  # clean the prediction for the single measurement

    @property
    def scan_complete(self):
        """
        Check if the measurement is complete.

        Returns
        -------
        bool
            True, if the measurement is complete. False, otherwise.

        """
        if self.m is None:
            return False
        return self.m.is_complete()

    def save_scan(self):
        """
        Save the scan to file.

        Returns
        -------
        None.

        """
        if self.m is not None:
            self.m.to_pkl()

    def __getattr__(self, k):
        """
        Return None since the specified attribute is not found.

        Note
        ----
        This function is called only when the normal method __getattribute__
        fails. Hence, it can be said that if this function has been called,
        then the attribute is not available in the object.

        Parameters
        ----------
        k : str
            Attribute name.

        Returns
        -------
        None.

        """
        return None

    def get_bounds(self):

        """

        This function when available loads the bounds for the s11 magnitude according to the fruit selected.

        Returns
        -------
        Dictionary with bounds interpolated at the measurement frequency

        """
        bounds = {}

        try:
            if self.scan_info is not None:


                if 'demo' in self.scan_info.lower():
                # if 'demo' in breed_patch.lower():

                    bounds = {}

                else:
                    fruit = self.scan_info.split(',')[0]
                    bounds_path = _BOUNDS_DIR / (fruit.lower() + ".json")

                    if self.scan_info is not None and len(self.scan_info.split(',')) > 1:
                        fruit, breed_patch = self.scan_info.split(',')
                        breed_patch = breed_patch.lstrip()
                        patch = breed_patch.split(' ')[1]
                        if os.path.isfile(_BOUNDS_DIR / (fruit.lower() + "_" + patch + ".json")):
                            bounds_path = _BOUNDS_DIR / (fruit.lower() + "_" + patch + ".json")

                    # Loading the dictionary from the JSON file
                    with open(bounds_path, "r") as file:
                        bounds_dict = json.load(file)

                    bounds = {

                        'lower_b': np.interp(self.s.get_freq_vect(), bounds_dict["frequency"],
                                             bounds_dict["lower_bound"]),
                        'upper_b': np.interp(self.s.get_freq_vect(), bounds_dict["frequency"],
                                             bounds_dict["upper_bound"]),
                        'mean': np.interp(self.s.get_freq_vect(), bounds_dict["frequency"],
                                          bounds_dict["mean_spectrum"]),

                    }

        except Exception as e:
            print("An error occurred while loading plot bounds \n" + str(e))
            return bounds

        return bounds
