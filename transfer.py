# -*- coding: utf-8 -*-
"""
Created on Sun Sep 19 00:42:59 2021.

@author: Nishad Mandlik
"""
# import pydevd_pycharm

import ctypes
from PyQt5.QtWidgets import QApplication
import sys
from gui_fresco_py.ui_transfer import Window


def is_admin():
    """
    Return whether the process is being run with admin privileges or not.


    Returns
    -------
    bool
        True if process is invoked with admin privileges. False otherwise.

    """
    # return True
    try:
        return ctypes.windll.shell32.IsUserAnAdmin()
    except:
        return False


if __name__ == '__main__':
    if is_admin():
        # pydevd_pycharm.settrace('localhost', port=9929, stdoutToServer=True, stderrToServer=True)
        # For IPython Compatibility
        tmp = QApplication.instance() is None
        if tmp:
            app = QApplication(sys.argv)
        w = Window()
        if tmp:
            sys.exit(app.exec_())
    else:
        # Re-run the program with admin rights
        ctypes.windll.shell32.ShellExecuteW(None, "runas", sys.executable, " ".join(sys.argv), None, 1)


