# -*- coding: utf-8 -*-
"""
Created on Sun Aug 22 18:40:49 2021.

@author: Nishad Mandlik
"""

from PyQt5.QtWidgets import QApplication
import sys
from gui_fresco_py.ui_scanner import Window

if __name__ == '__main__':
    # For IPython Compatibility
    tmp = QApplication.instance() is None
    if tmp:
        app = QApplication(sys.argv)
    w = Window()
    if tmp:
        sys.exit(app.exec_())

#### SplashScreenImplementation

# import sys
# from pathlib import Path
# from PyQt5.QtWidgets import QApplication, QSplashScreen, QMainWindow, QLabel
# from PyQt5.QtGui import QPixmap
# from PyQt5.QtCore import Qt, QTimer
# from gui_fresco_py.ui_scanner import Window
# _SPLASHSCREENFILE = "data/System/Design/splash_400.png"
#
# def show_splash_screen():
#     app = QApplication(sys.argv)
#
#     # Load the splash screen image
#     splash_pix = QPixmap(_SPLASHSCREENFILE)
#
#     # Create the splash screen
#     splash = QSplashScreen(splash_pix, Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint)
#     splash.setWindowFlag(Qt.FramelessWindowHint)
#     splash.show()
#
#     # Create and initialize the main window
#     main_window = Window()
#
#     # Ensure the splash screen remains for a minimum duration
#     QTimer.singleShot(1000, lambda: finish_splash(splash, main_window))
#
#     sys.exit(app.exec_())
#
#
# def finish_splash(splash, main_window):
#     # Close the splash screen and show the main window
#     splash.close()
#     main_window.show()
#
# def show_main_window():
#     main_window = Window()
#     main_window.show()
#
#
# if __name__ == '__main__':
#     show_splash_screen()
