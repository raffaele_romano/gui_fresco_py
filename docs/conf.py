# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys

# sys.path.insert(0, os.path.abspath('.'))

_SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

sys.path.insert(0, os.path.join(_SCRIPT_DIR, '../'))

# -- Project information -----------------------------------------------------

project = 'fresco'
copyright = '2021, Vertigo Technologies'
author = 'Vertigo Technologies'


# -- General configuration ---------------------------------------------------

# Linking to Source Code
# branch = Repository(os.path.join(_SCRIPT_DIR, "../")).head.shorthand
branch = "master"


def linkcode_resolve(domain, info):
    if domain != 'py':
        return None
    if not info['module']:
        return None
    filename = info['module'].replace('.', '/')
    return "https://bitbucket.org/raffaele_romano/gui_fresco_py/src/" + branch + \
        "/" + filename + ".py"


# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.napoleon',
    'sphinx.ext.linkcode',
    'm2r2'
]
napoleon_google_docstring = True
napoleon_numpy_docstring = True
napoleon_include_init_with_doc = True
napoleon_include_private_with_doc = True

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store', 'source/modules.rst']

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'
html_logo = 'fresco_logo.png'
html_favicon = 'fresco_fav.png'
html_theme_options = {
    'style_nav_header_background': '#009ce3',
}

html_show_sphinx=False

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = []
