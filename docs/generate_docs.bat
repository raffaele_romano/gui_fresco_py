cd %~dp0
del /q /s .\_build
del /q /s .\source
sphinx-apidoc -f -o source ../ ../__pycache__ ../temp* ../untitled* ../examples ../main.py ../transfer.py ../resources_rc.py ../build ../dist ../docs ../System ../User
.\make.bat html