# File Transfer Mechanism

This page describes the operation of the file-transfer software.

## Modes of Operation
The software can operate in one of the two modes:
* **SEND** - To be used on the sender device.
* **RECEIVE** - To be used on the receiver device.


## Connection Types
Files can be transferred between devices in a Local Area Network (LAN). The network can be created by connecting the sender and receiver:
* to each other via Ethernet _(default option)_, or,
* to a router via Ethernet/WiFi.

The connection type can be chosen by selecting/deselecting the "**Direct Ethernet Connection**" option on the home screen.

In the direct-connection mode, both devices are configured with a static IP and gateway to ensure that a LAN is established.
* The sender IP and Gateway are configured to 172.168.0.1 and 172.168.0.2 respectively.
* The receiver IP and Gateway are configured to 172.168.0.2 and 172.168.0.1 respectively.


## Discovery
* In order to initiate a connection, at least one of the devices must know the IP address of the other. To eliminate the need for entering the IP address manually, auto-discovery is employed.
* The device operating in **RECEIVE** mode acts as a _master_ and the device operating in **SEND** mode acts as the _slave__ for discovery. The receiver broadcasts a particular query using UDP, and waits for a response from the sender(s). The sender, on the other hand listens for the query, and upon receiving it, responds to the IP address which sent it. The receiver detects the IP address of the sender, when it receives the response message.
* The query and response messages are _"fresco"_ and _"ocserf"_. The query is sent from (<i>receiver_ip : 2017</i>) to (<i>255.255.255.255 : 2018</i>) and the response is sent from (<i>sender_ip : 2018</i>) to (<i>receiver_ip : 2017</i>).


## Connection
* A TCP connection is used for transferring files, with the sender as the server and the receiver as the client.
* The connection is established from (<i>receiver_ip : 2019</i>) to (<i>sender_ip : 2020</i>).


## Transfer
The measurement files are transferred in the following stages:
1. After the connection is established, the receiver requests a list of available files from the sender, and displays them to the user for selection.
2. The selected files ae requested from the sender, and the sender sends the files sequentially.
3. After all files have been received, the connection is closed.
